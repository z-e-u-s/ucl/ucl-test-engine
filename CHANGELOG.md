## 0.6.0 (2025-02-06)

Dependencies related to Ubuntu have been updated.

## 0.3.0 (2024-09-30)

### undefined

- [SIEM-7205-Scheduled_trivy_scans_with_E-mail-reporting_in_case_of_finding](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/commit/660c60e1e4e6447a9474396429ebba2307ed2d84) ([merge request](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/merge_requests/44))
- [SIEM-6819-6976-CICD_implement_trivy_for_image_scans](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/commit/93742185826228bec7361dc733ce4a26b6c0485f) ([merge request](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/merge_requests/32))
- [[SIEM-6698] Reproducible Builds via pip-tools](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/commit/739aa62f83ca6776935c20478ad421c482cca1df) ([merge request](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/merge_requests/31))
- [improvement/SIEM-6699_django_upgrade](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/commit/2133458496e22866df4885518be6046bb0f23fb5) ([merge request](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/merge_requests/30))
- [[SIEM-6744]: Coverage config improvements](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/commit/656dfbf5fab448c8a6b2864f66bc493082dba4e0) ([merge request](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/merge_requests/29))
- [Task/siem 5662 ci cd pre commit hook](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/commit/4cb233359174854919ad0871b1c09b04933fe21d) ([merge request](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/merge_requests/22))
- [Task/siem 5750 generate api docu in pipeline](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/commit/3291790928d661e49f55c3bb3d34b5cc9376a8b3) ([merge request](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/merge_requests/19))

## 0.1.0 (2023-10-19)

### undefined

- [Feature/siem 4959 connect zeus mock](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/commit/01044557d047ee9fa2dc185cb42f444fafc05fbc) ([merge request](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/merge_requests/9))
- [Feature/siem 4828 enable list of test events](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/commit/bada7ba606936155ca3f4f38f820a5d04f972e40) ([merge request](https://gitlab.com/z-e-u-s/ucl/ucl-test-engine/-/merge_requests/5))
