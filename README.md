# UCL Test Engine

## Requirements

Python 3.12.3 is recommended.

## Getting started

### Environment variables

Copy the wanted `...-example.env`-file e.g. `local-example.env` and name it `.env`. There is no `.env`-file in the repo to prevent these settings to get into production. `django-environ` [package](https://django-environ.readthedocs.io/en/latest/) is used for configuring the app via environment variables.

### Run server

#### Bash on linux

```bash
python3 -m venv .venv # maybe you need to use "python" instead of "python3"
source ./.venv/bin/activate
pip install -r requirements.txt
pip install -r requirements_dev.txt
docker-compose -f docker/docker-compose.yaml up -d # starts local postgres
./manage.py migrate
./manage.py init_ucl_test_engine # to load all necessary data and settings
./manage.py qcluster # start cluster for queue / cronjob scheduler
./manage.py runserver 0.0.0.0:8001 # starts local dev-server on port 8001 on all interfaces
```

#### Powershell on windows

```powershell
python3 -m venv .venv # maybe you need to use "python" instead of "python3"
./.venv/bin/Activate.ps1
pip install -r requirements.txt
pip install -r requirements_dev.txt
docker-compose -f docker/docker-compose.yaml up -d # starts local postgres
./manage.py migrate
./manage.py init_ucl_test_engine # to load all necessary data and settings
./manage.py qcluster # start cluster for queue / cronjob scheduler
./manage.py runserver 0.0.0.0:8001 # starts local dev-server on port 8001 on all interfaces
```

#### Cmd on windows

```console
python3 -m venv .venv # maybe you need to use "python" instead of "python3"
.\.venv\Scripts\activate
pip install -r requirements.txt
pip install -r requirements_dev.txt
docker-compose -f docker/docker-compose.yaml up -d # starts local postgres
.\manage.py migrate
.\manage.py init_ucl_test_engine # to load all necessary data and settings
.\manage.py qcluster # start cluster for queue / cronjob scheduler
.\manage.py runserver localhost:8001 # starts local dev-server on port 8001 on all interfaces
```

#### Run server and database in docker

The following command starts and runs the UCL backend along with a database

```bash
docker-compose -f docker/docker-compose-server-with-db-start.yaml up -d
```

(windows cmd)

```console
docker-compose -f docker/docker-compose-server-with-db-start.yaml up -d
```

The following command stops the UCL backend along with the database

```bash
docker-compose -f docker/docker-compose-server-with-db-start.yaml down
```

(windows cmd)

```console
docker-compose -f docker/docker-compose-server-with-db-start.yaml down
```

### Tasks

If you want to run cronjobs or asynchronous tasks, you have to start the qcluster. This executes the tasks from the queue.

```bash
./manage.py qcluster
```

(windows cmd)

```console
.\manage.py qcluster
```

### Create admin user

```bash
./manage.py createsuperuser
```

(windows cmd)

```console
.\manage.py createsuperuser
```

### Create new migrations

If you have changed database models you need to create a new migration to apply this changes to the database when calling `migrate`.

```bash
./manage.py makemigrations
```

(windows cmd)

```console
.\manage.py makemigrations
```

### Upgrade python dependencies

Please never modifiy the `requirements*.txt` files directly. When working with `pip-tools` the `requirements*.txt` files act as dependency lock files like in the javascript ecosystem.

First find out how [pip-tools](https://pypi.org/project/pip-tools/) works.
If `pip-compile` finds an existing `requirements.txt` file that fulfils the dependencies then no changes will be made, even if updates are available. To compile from scratch, first delete the existing `requirements*.txt` files.

```bash
rm requirements.txt requirements_dev.txt
```

Make your changes (e.g. add/remove a package or update version constraints) to the files (`requirements.in` and/or `requirements_dev.in`) and then compile from scratch.

```bash
pip-compile --no-header requirements.in
pip-compile --no-header requirements_dev.in
```

Upgrade a single package.

Note: If the dependency is related to `requirements.txt` and `requirments_dev.txt` its needed to run pip-compile with both files (`requirements.in` / `requirements_dev.in`). Otherwise the will be a version mismatch.

```bash
pip-compile --no-header --upgrade-package Django requirements.in
pip-compile --no-header --upgrade-package Django requirements_dev.in
```

or with version constraints.

```bash
pip-compile --no-header --upgrade-package 'Django<5' requirements.in
pip-compile --no-header --upgrade-package 'Django<5' requirements_dev.in
```

## Cronjobs

There is the function `create_default_cronjobs()` in `CronjobService` (`ucl_test_engine\services\cronjob.py`) that starts cronjobs at service start. (`./manage.py init_ucl_test_engine` is called at service start which calls `create_default_cronjobs()`). Add new cronjobs here if they shall start with the ucl-test-engine.

## Tests

### Running tests

`pytest` is the default testrunner. Run tests with:

```bash
pytest -vv
```

You can also run the tests right from the files in vscode. You need the python extension and you need to configure the tests location. It is explained [here](https://code.visualstudio.com/docs/python/testing#_configure-tests). Additionaly `vscode` settings are provided to discover tests after setting your python interpreter in vscode to `./.venv/bin/python` (python extension has to be installed)

### Running tests with coverage

You can run the tests and show the coverage with `coverage.py`. Use the following command in the root directory of the repo.

#### Same as in the GitLab-CI pipeline

```bash
pytest --cov --cov-report=term --cov-report=xml
```

#### Also shows missing lines in coverage

```bash
coverage run -m pytest -v && coverage report -m
```

#### Use already configured pre-commit hook

```bash
pre-commit run --all-files -v pytest
```

## Code Quality

### Pylint

`pylint` is a linter, a static code analyser. Run `pylint`

```bash
pylint keycloak ucl_test_engine
```

### Black

`black` is a Python code formatter. Run `black` and format your code

```bash
black .
```

or run `black` in check-mode

```bash
black --check --diff .
```

### Semgrep

Semgrep is a static code analysis tool. Semgrep is scanning code and package dependencies for known issues, software vulnerabilities, and detected secrets. Run semgrep by using existing pre-commit hook:

```bash
pre-commit run --all-files -v semgrep
```

### Mypy

`mypy` is a static type checker for Python. Run `mypy` to scan a single file or the whole repo.

```bash
mypy <PATH_TO_FILE>
mypy ./
```

### pre-commit (unix)

These tests can also be executed automatically before a commit (pre-commit). Python related tests are executed only if Python files are going to be commited. To activate this option, type the following command:

```bash
pre-commit install
```

If the commit and thus the hooks are executed via the terminal, the status messages of the test steps are displayed in the terminal. Otherwise, if the commit is triggered via an IDE, the status messages are not necessarily displayed in the terminal. For example, VSCode hides the reports in the "OUTPUT" tab of the console.

To deactivate the pre-commit hooks, type the command:

```bash
pre-commit uninstall
```

## Documentations

### Code / Sphinx Documentation

- docs are written with sphinx
- generate with:

```bash
cd doc
make html
```

### API Documentation

The API documentation can be used when running the service with the development server and having enabled the api docu (ENABLE_API_DOCU).

- Swagger-URL: `.../api/v1/schema/swagger-ui`
- Redoc-URL: `.../api/v1/schema/redoc`

The API documentation is also built in the GitLab CI pipeline and deployed to GitLab pages. Therefore we use files from the folder `gitlab_pages`. If we want to use a new swagger version keep in mind to update it also in `gitlab_pages/swagger-ui.html` and recalculate the integrity-sha. This can be done with this site: https://www.srihash.org/.

The documentation is also available under the following url: https://z-e-u-s.gitlab.io/ucl/ucl-test-engine/

## Configuration

This list must be adjusted if variables are added or removed. Also add the variable in the `local-example.env`-file and the corresponding pipeline (currently GitLab CI/CD) with values for local development and testing!

### PostgreSQL

- POSTGRES_DB: Database name used by postgres.
- POSTGRES_HOST: Host used by postgres.
- POSTGRES_PORT: Port used by postgres.
- POSTGRES_USER: Admin name used by postgres.
- POSTGRES_PASSWORD: Admin password used by postgres.

### Django / Application

- DJANGO_SECRET_KEY: Used for cryptographic signing by django.
- DJANGO_DEBUG: Turns debug mode of django on or off. `False` automatically sets `ENABLE_API_DOCU` to `False` because static files are not served anymore.  
  Default if not set: `False`.
- ALLOWED_HOSTS: String of comma separated hosts that can serve this django site.
- CORS_ALLOWED_ORIGINS: String of comma separated origins allowed for cors.
- CORS_ORIGIN_ALLOW_ALL: Allow / prohibit all cors origins.
  Default if not set: `False`.
- SECURE_SSL_REDIRECT: If True, the SecurityMiddleware redirects all non-HTTPS requests to HTTPS (except for those URLs matching a regular expression listed in `SECURE_REDIRECT_EXEMPT`). In case of problems behind a proxy set `SECURE_PROXY_SSL_HEADER`.
  Default if not set: `True`.
- SECURE_REDIRECT_EXEMPT: If a URL path matches a regular expression in this list, the request will not be redirected to HTTPS.
  Default if not set: `[]` (Empty list).
- SECURE_PROXY_SSL_HEADER: A tuple representing an HTTP header/value combination that signifies a request is secure.
  Default if not set: `None`.
- SESSION_COOKIE_SECURE: Whether to use a secure cookie for the session cookie. If this is set to True, the cookie will be marked as “secure”, which means browsers may ensure that the cookie is only sent under an HTTPS connection.
  Default if not set: `True`.
- CSFR_COOKIE_SECURE: Whether to use a secure cookie for the CSRF cookie. If this is set to True, the cookie will be marked as “secure”, which means browsers may ensure that the cookie is only sent with an HTTPS connection.
  Default if not set: `True`.
- SECURE_HSTS_SECONDS: If set to a non-zero integer value, the SecurityMiddleware sets the HTTP Strict Transport Security header on all responses that do not already have it.
  Default if not set: `31536000` (One year).
- SECURE_HSTS_INCLUDE_SUBDOMAINS: If True, the SecurityMiddleware adds the includeSubDomains directive to the HTTP Strict Transport Security header. It has no effect unless `SECURE_HSTS_SECONDS` is set to a non-zero value.
  Default if not set: `True`.
- ENABLE_API_DOCU: Enables the api-docu url. `False` disables API-documentation (/schema), `True` enables it. `DJANGO_DEBUG=False` automatically sets `ENABLE_API_DOCU` to `False` too.
  Default if not set: `False`.
- ENABLE_DJANGO_AUTH: Enables django's /api-auth url. Probably won't work with RBAC secured routes. `False` disables django's /api-auth, `True` enables it.
  Default if not set: `False`.
- ENABLE_ADMIN_SITE: Enables the admin-site url. `False` disables admin-site (/admin), `True` enables it.
  Default if not set: `False`.
- DISABLE_AUTHENTICATION: `True` or `False`. Disabling authentication should ONLY be used locally. The `system` user will always be logged in for every request. No credentials must be passed to the API. There won't be any check against Keycloack or some other auth-provider.
  Default is `False` if key is not existing.
- SIEM_BASE_URL: Base url (Protocol, host and Port) of the SIEM service to send the log tests to.
- SIEM_VERIFY_SSL_CERTIFICATE: `False` disables SSL-Certificate check. Disabling is discouraged for security reasons.
  Default if not set: `True`.
- THROTTLE_RATE_ANON: Rate that shall be used for the throttler for an anonymous user.
  Default if not set: `100/day`.
- THROTTLE_RATE_USER: Rate that shall be used for the throttler for an authenticated user.
  Default if not set: `1000/day`.
- MAX_WAITING_TIME_TO_ABORT_EXECUTION_IN_SEC: The time the test engine tries to get a result from the siem. After the time has expired the execution will be aborted.
  Unit is seconds and default if not set is `86400` (= 1 day in seconds).
- USE_WINDOWS_SELECTOR_EVENT_LOOP_POLICY: `True` enables `WindowsSelectorEventLoopPolicy` usage for `asyncio`. This is needed if you want to run the service on windows.
  Default if not set: `False`.

### Opensearch

- OPENSEARCH_BASE_URL: Base url (Protocol, host and Port) of Opensearch to recieve the log test results from.
- OPENSEARCH_USER_NAME: User name for authentication against Opensearch.
- OPENSEARCH_USER_PASSWORD: User password for authentication against Opensearch.
- OPENSEARCH_VERIFY_SSL_CERTIFICATE: `False` disables SSL-Certificate check. Disabling is discouraged for security reasons.
  Default if not set: `True`.
- OPENSEARCH_PAGINATION_LIMIT: Pagination limit for responses from Opensearch.
  Default if not set: `10000`.

### Keycloak

- KEYCLOAK_CLIENT_ID: The client created in keycloak.
- KEYCLOAK_CLIENT_SECRET: Secret of the used client.
- KEYCLOAK_SERVER_URL: The authentication url of the used keycloak instance.
- KEYCLOAK_REALM: The name of the used realm (the one in which the client is created).
- KEYCLOAK_AUDIENCE: The audience that must be present in JWT `aud` field.
- KEYCLOAK_CLIENT_ID_FOR_AUTHZ_ROLES: Path in JWT where users' roles are set: TOKEN. resource_access"."<KEYCLOAK_CLIENT_ID_FOR_AUTHZ_ROLES>"."roles".["<the_list_of_roles>"]
- KEYCLOAK_VERIFY_CERTIFICATE: Boolean value to turn keycloak token certificate validation on or off.  
  Default if not set: `True`.
- KEYCLOAK_ISSUER: String value to set keycloak validate jwt issuer content.  
  Default if not set: `{KEYCLOAK_SERVER_URL}/realms/{KEYCLOAK_REALM}"`.
- KEYCLOAK_VALIDATE_ISSUER: Boolean value to turn keycloak validate jwt issuer validation on or off.  
  Default if not set: `True`.

### QCluster

- Q_CLUSTER_NAME: Name of the cluster used by django-q.
  Default if not set: `UclCluster`.
- Q_CLUSTER_WORKERS: Number of workers used by django-q.
  Default if not set: `4`.
- Q_CLUSTER_TIMEOUT: Timeout for a task before it is marked as failed.
  Default if not set: `90`.
- Q_CLUSTER_RETRY: Number of retries for a task before it is marked as failed.
  Default if not set: `120`.
- Q_CLUSTER_QUEUE_LIMIT: Maximum number of tasks in a queue.
  Default if not set: `50`.
- Q_CLUSTER_SYNC: Needs to be set to `True` on [Windows](https://django-q.readthedocs.io/en/latest/install.html#windows).
- Q_CLUSTER_CATCH_UP: Schedules that didn’t run while a cluster was down, will play catch up and execute all the missed time slots until things are back on schedule.
  Default if not set: `False`.
- Q_CLUSTER_MAX_ATTEMPTS: Maximum number of attempts to successfully execute a task.
  Default if not set: `1`.

### Gunicorn

- WEB_CONCURRENCY: The number of worker processes for handling requests. This is a gunicorn config setting. Further [information](https://docs.gunicorn.org/en/latest/settings.html#workers).
  Default if not set: `1`.

## Use other SIEM

By default the system uses `ucl_test_engine\services\zeus_mock\zeus_connection_service` for communication with the SIEM. To use an other SIEM you have to create your own service with the communication logic. Inherit from `ucl_test_engine\services\siem_connection_interface.py` and then assigne your service to `SiemConnectionService` variable in `ucl_test_engine\services\test_engine.py`.

## Troubleshooting

Here are some issues that might occur when working with this repo.

### Docker container is not up to date

When the `Dockerfile` is updated you need to recreate the image. Therefore delete the currently used docker container and docker image. You can do this via the `Docker for Desktop`-client or the CLI. Afterwards start the docker-compose again, it will creat them newly and use the new `Dockerfile` version.

## Authentication and Authorization

Keycloak is used for user management.

### Roles for ucl-test-engine client (tecnical user / service account)

- ucl-test-engine (needed for communication with ucl-backend)
