Services
=========


.. toctree::
   :maxdepth: 2
   :caption: Siem connection implementations:

   ucl.test.engine.services.siem_connection_interface


Submodules
----------

CronjobService Module
---------------------

.. automodule:: ucl_test_engine.services.cronjob
   :members:
   :undoc-members:
   :show-inheritance:

Error Handling Module
---------------------

.. automodule:: ucl_test_engine.services.error_handling
   :members:
   :undoc-members:
   :show-inheritance:

KeycloakServiceAccount Module
-----------------------------

.. automodule:: ucl_test_engine.services.service_account
   :members:
   :undoc-members:
   :show-inheritance:

SiemConnectionInterface Module
------------------------------

.. automodule:: ucl_test_engine.services.siem_connection_interface
   :members:
   :undoc-members:
   :show-inheritance:

TestEngineService Module
------------------------

.. automodule:: ucl_test_engine.services.test_engine
   :members:
   :undoc-members:
   :show-inheritance: