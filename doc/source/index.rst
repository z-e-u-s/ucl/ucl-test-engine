.. Use-Case Library documentation master file, created by
   sphinx-quickstart on Fri Jul 22 15:00:39 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Use-Case Library's documentation!
========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   development/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
