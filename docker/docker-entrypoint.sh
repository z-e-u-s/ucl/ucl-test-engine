#!/bin/sh

# Apply database migrations
echo "Apply database migrations"
python3 manage.py migrate

echo "set up UCL_TEST_ENGINE"
python3 manage.py init_ucl_test_engine

# Start Django Q cluster
echo "Starting Django Q cluster"
python3 manage.py qcluster &

# Start server
echo "Starting gunicorn server"
exec gunicorn -b 0.0.0.0:8000 wsgi
