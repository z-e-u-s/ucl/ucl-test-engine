#!/usr/bin/env bash

# This script checks if the database connection is successful

# Load the environment variables
source ./.env

# Set the environment variables for the database connection
export DB_USER=$POSTGRES_USER
export DB_PASS=$POSTGRES_PASSWORD
export DB_NAME=$POSTGRES_DB
export DB_CONTAINER="ucl-test-engine-db"

# Connect to the database and leave
docker exec $DB_CONTAINER psql -h localhost -U $DB_USER -d $DB_NAME -c '\q'

# Set the exit code based on the result of the connection
if [ $? -eq 0 ]; then
    echo "Database connection successful."
    exit 0
else
    echo "Database connection failed."
    exit 1
fi