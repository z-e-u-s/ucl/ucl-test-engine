#!/usr/bin/env bash

# This script checks if the SSL redirection is set up correctly and runs a health check if the redirection is enabled.

# Load the environment variables
source ./.env

# Set the environment variables for the database connection
export SECURE_SSL_REDIRECT=$SECURE_SSL_REDIRECT
export SIEM_BASE_URL=$SIEM_BASE_URL

# Check if the SSL redirection is enabled
if [ "$SECURE_SSL_REDIRECT" = "True" ]; then
    echo "SSL redirection is enabled."
else
    echo "SSL redirection is disabled."
    exit 0
fi

# Extract the port from the SIEM_BASE_URL
PORT=$(echo $SIEM_BASE_URL | awk -F ":" '{print $NF}')

# Check if the Django server is already running
if lsof -Pi :$PORT -sTCP:LISTEN -t >/dev/null; then
    export DJANGO_ALREADY_STARTED=true
else
    export DJANGO_ALREADY_STARTED=false
fi

# Check if the port is open
check_port() {
    local port=$1
    if lsof -Pi :$port -sTCP:LISTEN -t >/dev/null; then
        return 0
    else
        return 1
    fi
}

# Terminate the Django server
kill_port() {
    if [ "$DJANGO_ALREADY_STARTED" = false ]; then
        echo "Stopping Django server..."
        kill $(lsof -ti :$PORT)
    fi
}


# Start the Django server if it is not running
if ! check_port $PORT; then
    echo "Starting Django server on port $PORT..."
    python manage.py runserver 0.0.0.0:$PORT &
fi

# Wait until the server is running
while ! check_port $PORT; do
    sleep 1
done

# Run curl to get the headers of the URL
OUTPUT=$(curl -I $SIEM_BASE_URL)

# Check if the response code is 301
if echo $OUTPUT | grep "301 Moved Permanently" > /dev/null; then
    echo "301 redirect is set up correctly."
else
    echo "301 redirect is not set up correctly."
    kill_port
    exit 1
fi

# Check if the location header is set to https
if echo $OUTPUT | grep "Location: https://" > /dev/null; then
    echo "SSL redirection is set up correctly."
    kill_port
    exit 0
else
    echo "SSL redirection is not set up correctly."
    kill_port
    exit 1
fi

