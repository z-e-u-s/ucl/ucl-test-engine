#!/usr/bin/env bash

function run_check_versions_format() {
    export UCL_VERSION=$(grep -o "version = \"[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+\"" pyproject.toml | grep -o "[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+")
    echo "$UCL_VERSION"
    if [[ $UCL_VERSION =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]
      then
        return 0
      fi
    echo "ucl version from pyproject.toml version does not match pattern '^\d+\.\d+\.\d+$'"
    return 1
}

run_check_versions_format