#!/usr/bin/env bash

echo "Running Trufflehog"

REPO_PATH=$PWD
DOCKER_WORKDIR="/repo"

# This command works on all terminals to successfully run trufflehog for uncommitted changes
# MSYS_NO_PATHCONV=1 is necessary to make it work for Git Bash (See: https://github.com/docker-archive/toolbox/issues/673 or https://github.com/moby/moby/issues/24029)
MSYS_NO_PATHCONV=1 docker run --rm -v "$REPO_PATH":"$DOCKER_WORKDIR" trufflesecurity/trufflehog:latest filesystem $DOCKER_WORKDIR --exclude-paths=$DOCKER_WORKDIR/hooks/trufflehog-excluded-paths.txt --fail

# If trufflehog finds a secret, abort the commit
if [ $? -ne 0 ]; then
    echo "TruffleHog found secrets! Commit aborted."
    exit 1
fi