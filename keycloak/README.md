# Keycloak Authentication for ucl_test_engine Backend

For ucl_test_engine authentication, we follow the Authorization Code Flow.
For us, this means that the frontend takes care of the registration with Keycloak.
The backend only validates the jwt header token.
As a result, there are only a few functionalities that have been implemented.

### Environment variables

```
KEYCLOAK_CONFIG = {
    "CLIENT_ID": os.environ["KEYCLOAK_CLIENT_ID"],
    "CLIENT_SECRET": os.environ["KEYCLOAK_CLIENT_SECRET"],
    "SERVER_URL": os.environ["KEYCLOAK_SERVER_URL"],
    "REALM": os.environ["KEYCLOAK_REALM"],
}
```

You can find a selection of variables in `keycloak.settings.py`, just overwrite them in the django settings.

By setting the variable

```
KEYCLOAK_CONFIG = {
    ...
    "VERIFY_TOKENS_WITH_KEYCLOAK": True
    ...
}
```

This means that the token is validated with the Keycloak API and locally.
By default, the setting is based on `DEBUG` variable.

### Enable

Add `keycloak` to INSTALLED_APPS.

```
INSTALLED_APPS = [
    "django.contrib.auth",
    ...
    "keycloak"
]
```

Add `keycloak.authentication.KeycloakAuthBackend` to DRF settings

```
REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        ...
        "keycloak.authentication.KeycloakAuthBackend",
        ...
    ],
```

### Permissions

To create permissions for your API follow the example in `HasViewProfilePermission` in `keycloak.permissions.py`.

use it as usual...

```
class UserApi(generics.RetrieveAPIView):
    permission_classes = [HasViewProfilePermission]
```

### Middleware

For security reasons, use the middleware in `keycloak.middleware.UclTestEngineHeaderMiddleware` at the top of the settings.

```
MIDDLEWARE = [
    "keycloak.middleware.UclTestEngineHeaderMiddleware",
    ....

]
```

You should also take a look at the [django-csp](https://github.com/mozilla/django-csp) package from Mozilla.

### Requirements

```
requests==2.28.1
PyJWT==2.6.0
```

Thanks to [`django-rest-framework-simplejwt`](https://github.com/jazzband/djangorestframework-simplejwt), the code was inspirational for this package.
