"""AppConfig Keycloak"""

from django.apps import AppConfig


class KeycloakConfig(AppConfig):
    """app config"""

    default_auto_field = "django.db.models.BigAutoField"
    name = "keycloak"

    def ready(self):
        # pylint: disable=unused-import, import-outside-toplevel
        # import above schema.py
        import keycloak.schema  # noqa: E402
