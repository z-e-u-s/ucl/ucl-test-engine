"""Module for Keycloak exception"""

from rest_framework.exceptions import APIException
from rest_framework import status


class SignatureExpired(APIException):
    """
    API Exception for SignatureExpiredError
    """

    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Signature has expired."
    default_code = "signature_expired"


class InvalidSignature(APIException):
    """
    API Exception for InvalidSignatureError
    """

    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Signature is invalid."
    default_code = "invalid_signature"


class InvalidAudience(APIException):
    """
    API Exception for InvalidAudienceError
    """

    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Audience is invalid."
    default_code = "invalid_audience"


class InvalidIssuer(APIException):
    """
    API Exception for InvalidIssuerError
    """

    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Issuer is invalid."
    default_code = "invalid_issuer"


class InvalidIssuedAt(APIException):
    """
    API Exception for InvalidIssuedAtError
    """

    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "IssuedAt is invalid."
    default_code = "invalid_issuer_at"


class MissingRequiredClaim(APIException):
    """
    API Exception for MissingRequiredClaimError
    """

    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Missing required claim."
    default_code = "missing_required_claim"


class InvalidAlgorithm(APIException):
    """
    API Exception for InvalidAlgorithmError
    """

    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Algorithm is invalid."
    default_code = "invalid_algorithm"


class InvalidToken(APIException):
    """
    API Exception for token errors where we do not want to return the detailed reason.
    """

    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Token is invalid or expired"
    default_code = "token_not_valid"


class ImmatureSignature(APIException):
    """
    API Exception for ImmatureSignatureError
    """

    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "The token is not yet valid (iat)"
    default_code = "token_not_yet_valid"
