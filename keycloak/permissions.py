""" Get User permissions from Keycloak token  """

from rest_framework.permissions import BasePermission
from rest_framework.permissions import IsAuthenticated

from django.conf import settings
from keycloak.settings import keycloak_settings


class HasPermission(BasePermission):
    """
    Allows the User access if they have the expected permission
    """

    def __init__(self, permission=None):
        if permission:
            self.permission = permission

    def has_permission(self, request, view) -> bool:
        """
        Return `True` if permission is granted, `False` otherwise.
        """

        if settings.DISABLE_AUTHENTICATION is True:
            return IsAuthenticated().has_permission(request, view)

        jwt_permission = []
        try:
            if request.auth is not None:
                jwt_permission = request.auth["resource_access"][
                    keycloak_settings.CLIENT_ID_FOR_AUTHZ_ROLES
                ]["roles"]
        except KeyError:
            jwt_permission = []
        return self.permission in jwt_permission

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        return self.has_permission(request, view)


class HasViewProfilePermission(HasPermission):
    """
    this is a default keycloak permission
    it is an example
    """

    permission = "view-profile"
