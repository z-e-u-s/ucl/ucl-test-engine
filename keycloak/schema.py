""" keycloak schema """

from drf_spectacular.extensions import OpenApiAuthenticationExtension
from drf_spectacular.plumbing import build_bearer_security_scheme_object


class TokenScheme(OpenApiAuthenticationExtension):
    """OpenAPI 3.0 security definition for Token authentication."""

    target_class = "keycloak.authentication.KeycloakAuthBackend"
    name = "KeycloakAuth"
    match_subclasses = True
    priority = -1

    def get_security_definition(self, auto_schema):
        """Return the security definition for the given view."""
        return build_bearer_security_scheme_object(
            header_name="AUTHORIZATION", token_prefix="Bearer", bearer_format="JWT"
        )
