""" test keycloak auth """

from unittest import mock
from datetime import datetime, timedelta
import jwt

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.test.utils import override_settings
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.test import APIRequestFactory

from keycloak.authentication import KeycloakAuthBackend
from keycloak.settings import keycloak_settings
from keycloak.exceptions import (
    SignatureExpired,
    InvalidSignature,
    InvalidAudience,
    InvalidIssuer,
    InvalidIssuedAt,
    MissingRequiredClaim,
    InvalidAlgorithm,
    InvalidToken,
    ImmatureSignature,
)
from keycloak.tests.helpers import create_user, USERNAME, EMAIL, FIRST_NAME, LAST_NAME


User = get_user_model()
keycloak_mapping = keycloak_settings.CLAIM_MAPPING
USER_CLAIMS = {
    keycloak_mapping["username"]: USERNAME,
    keycloak_mapping["first_name"]: FIRST_NAME,
    keycloak_mapping["last_name"]: LAST_NAME,
    keycloak_mapping["email"]: EMAIL,
}


class TestKeycloakAuthentication(TestCase):
    backend = KeycloakAuthBackend()
    private_key = (
        "-----BEGIN RSA PRIVATE KEY-----\n"
        + "MIICWgIBAAKBgH/mES/eqRUDdMrXwGAES3LAuLglbYZOccdQn5m575CpKcZTLEUe"
        + "1FL66o/X3kgWIi3vN2p4HOynpmZge68XRl1LtBdazEp1fXSlPE8XTia67zXGGduQ"
        + "VWvZSF+BybwxTdg/vTOr7Kmp2Kh7+wGrflbaDAiFtTsBrM1AZKJmQB7/AgMBAAEC"
        + "gYBAHj+phu+vCC+J7UWVHTIR0koIj0LsHPFvYZzRfWR7WkMGGPZNpJn/C6mB+U+H"
        + "RglWi+F1L8gYYCfMlTwMWBe+wZ3S+R0zD0G2xapFSfcJEziv9DwWV4CTS+Ep3xbV"
        + "hulq/HF9dbPoSC6MNbnn0L1KsALgzbfMutwxa/BkKFzgwQJBAMZhSCJtaajQu6yo"
        + "kdbE/bHO6TEwY/IbbMIC47QG6w2HzTsJPBfVSM/qIC0Sd7SyOKGp5E9ivIHSEtbc"
        + "vqaPBl8CQQClDBYcT5rVUU0NK6gWcx3wJ78rLz8JgT8Q4OipRDX1wW5bdgvb69J8"
        + "hF2vVMVRKns6j4e7fMbdWVXLp5IFu2thAkBG1zh2KtIfWaKbLnsd0ayykSvOx/P4"
        + "4fg9c+CySgIoC8Wq/ko6TIy6ymWBEuYyXa5BdLS18JFWXpkhpyRhwBNLAkB+vC0u"
        + "SCJIs0lLV76WnobapmqxKnJpRgm7WDjP4UTeFU48hQTONuNGIg4eWCW8EbbWXOiJ"
        + "G2NJDRRIccA7DpOBAkBjxMLUWJMezEKkx3BQXB6ImJjVZqSJDw4Ksk8AOnOL501q"
        + "0OcRrQjQbWSL98C0lwnpaHiBHR8FaIAbzii1/y7R\n"
        + "-----END RSA PRIVATE KEY-----"
    )
    public_key = (
        "-----BEGIN PUBLIC KEY-----\n"
        + "MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgH/mES/eqRUDdMrXwGAES3LAuLgl"
        + "bYZOccdQn5m575CpKcZTLEUe1FL66o/X3kgWIi3vN2p4HOynpmZge68XRl1LtBda"
        + "zEp1fXSlPE8XTia67zXGGduQVWvZSF+BybwxTdg/vTOr7Kmp2Kh7+wGrflbaDAiF"
        + "tTsBrM1AZKJmQB7/AgMBAAE=\n"
        + "-----END PUBLIC KEY-----"
    )

    @override_settings(KEYCLOAK_CONFIG={"VERIFY_TOKENS_WITH_KEYCLOAK": False})
    def test_backend_get_user(self):
        user = self.backend.get_user_or_create(USER_CLAIMS)
        self.assertEqual(user.email, EMAIL)

    def test_backend_user_raise(self):
        claims = {"foo": "bar"}
        with self.assertRaises(InvalidToken):
            self.backend.get_user_or_create(claims)

    def test_user_is_inactive(self):
        user = create_user()
        user.is_active = False  # nosemgrep
        user.save()
        self.assertIsNone(self.backend.get_user_or_create(USER_CLAIMS))
        user.is_active = True  # nosemgrep
        user.save()
        self.assertEqual(self.backend.get_user_or_create(USER_CLAIMS).id, user.id)

    @override_settings(KEYCLOAK_CONFIG={"VERIFY_TOKENS_WITH_KEYCLOAK": False})
    def test_update_user(self):
        USER_CLAIMS["given_name"] = "foo"
        self.assertEqual(self.backend.get_user_or_create(USER_CLAIMS).first_name, "foo")

    @override_settings(KEYCLOAK_CONFIG={"VERIFY_TOKENS_WITH_KEYCLOAK": False})
    def test_create_user_from_claims(self):
        claims = {
            keycloak_mapping["username"]: "FooBar",
            keycloak_mapping["first_name"]: "Foo",
            keycloak_mapping["last_name"]: "Bar",
            keycloak_mapping["email"]: "foo@bar.io",
        }
        self.assertFalse(User.objects.filter(username__iexact="FooBar").exists())
        user = self.backend.get_user_or_create(validated_token=claims)
        self.assertEqual(user.username, "FooBar")
        self.assertEqual(user.email, "foo@bar.io")
        self.assertTrue(user.is_active)  # nosemgrep

    @override_settings(KEYCLOAK_CONFIG={"VERIFY_TOKENS_WITH_KEYCLOAK": True})
    @mock.patch("keycloak.api.KeycloakApi.get_userinfo")
    def test_create_user_from_keycloak_mock(self, userinfo):
        token_payload = {
            keycloak_mapping["username"]: "ZOIDBERG",
            keycloak_mapping["first_name"]: "John A.",
            keycloak_mapping["last_name"]: "Zoidberg",
            keycloak_mapping["email"]: "zoidberg@bar.io",
        }
        mock_claims = token_payload.copy()
        mock_claims[keycloak_mapping["first_name"]] = "first_name_is_different"
        userinfo.return_value = mock_claims
        self.assertFalse(User.objects.filter(username__iexact="ZOIDBERG").exists())
        self.backend.raw_token = b"test_token"
        user = self.backend.get_user_or_create(validated_token=token_payload)
        # now user will be updated with keycloak api,
        self.assertEqual(user.username, "ZOIDBERG")
        self.assertTrue(user.is_active)  # nosemgrep
        self.assertEqual(user.first_name, "first_name_is_different")

    def test_authenticate_header(self):
        factory = APIRequestFactory()
        self.assertEqual(self.backend.authenticate_header(factory.request()), "Bearer realm='api'")

    def test_get_raw_token(self):
        fake_token = "CoolToken"
        fake_header = f"Bearer {fake_token}"
        self.assertIsNone(self.backend.get_raw_token(""))
        self.assertIsNone(self.backend.get_raw_token("is_not_bearer"))
        self.assertEqual(self.backend.get_raw_token(fake_header), fake_token)
        with self.assertRaises(AuthenticationFailed):
            self.backend.get_raw_token("Bearer one two")
        with self.assertRaises(AuthenticationFailed):
            self.backend.get_raw_token("Bearer")

    def test_authenticate_return_none(self):
        request = APIRequestFactory().get("/")
        self.assertIsNone(self.backend.authenticate(request))
        request.META["HTTP_AUTHORIZATION"] = "test"
        self.assertIsNone(self.backend.authenticate(request))

    @override_settings(KEYCLOAK_CONFIG={"VERIFY_TOKENS_WITH_KEYCLOAK": False})
    @mock.patch("keycloak.authentication.KeycloakAuthBackend.get_raw_token")
    @mock.patch("keycloak.token.JWToken.decode")
    def test_authenticate_get_user(self, mock_token, mock_decode_token):
        request = APIRequestFactory().get("/")
        request.META["HTTP_AUTHORIZATION"] = "test"
        claims = {
            keycloak_mapping["username"]: "ZOIDBERG",
            keycloak_mapping["first_name"]: "John A.",
            keycloak_mapping["last_name"]: "Zoidberg",
            keycloak_mapping["email"]: "zoidberg@bar.io",
        }
        token_value = {"preferred_username": "ZOIDBERG"}
        mock_token.return_value = token_value
        mock_decode_token.return_value = claims
        user, validated_token = self.backend.authenticate(request)
        self.assertEqual(user, User.objects.get(username="ZOIDBERG"))
        self.assertEqual(validated_token, token_value)

    def test_authenticate_get_raw_token_is_none(self):
        request = APIRequestFactory().get("/")
        request.META["HTTP_AUTHORIZATION"] = "test"
        self.assertIsNone(self.backend.authenticate(request))

    @override_settings(KEYCLOAK_CONFIG={"VERIFY_TOKENS_WITH_KEYCLOAK": False})
    @mock.patch("keycloak.api.KeycloakApi.get_public_key")
    @mock.patch("keycloak.api.KeycloakApi.get_jwks")
    def test_authenticate_raise_exception_signature_expired(
        self, mock_get_jwt_key, mock_public_key
    ):
        request = APIRequestFactory().get("/")

        mock_public_key.return_value = self.public_key
        mock_get_jwt_key.return_value = self.public_key
        token = jwt.encode(
            headers={
                "alg": "RS256",
                "typ": "JWT",
            },
            payload={
                "iss": "test",
                "exp": 0,
                "resource_access": {keycloak_settings.CLIENT_ID_FOR_AUTHZ_ROLES: "something"},
            },
            key=self.private_key,
        )
        request.META["HTTP_AUTHORIZATION"] = "Bearer " + token
        with self.assertRaises(SignatureExpired):
            self.backend.authenticate(request)

    @override_settings(KEYCLOAK_CONFIG={"VERIFY_TOKENS_WITH_KEYCLOAK": False})
    @mock.patch("keycloak.api.KeycloakApi.get_public_key")
    @mock.patch("keycloak.api.KeycloakApi.get_jwks")
    def test_authenticate_raise_exception_invalid_signature(
        self, mock_get_jwt_key, mock_public_key
    ):
        request = APIRequestFactory().get("/")

        mock_public_key.return_value = self.public_key
        mock_get_jwt_key.return_value = self.public_key
        token = jwt.encode(
            headers={
                "alg": "RS256",
                "typ": "JWT",
            },
            payload={
                "iss": "test",
                "resource_access": {keycloak_settings.CLIENT_ID_FOR_AUTHZ_ROLES: "something"},
            },
            key=self.private_key,
        )
        request.META["HTTP_AUTHORIZATION"] = "Bearer " + token + "fff"
        with self.assertRaises(InvalidSignature):
            self.backend.authenticate(request)

    @override_settings(
        KEYCLOAK_CONFIG={
            "VERIFY_TOKENS_WITH_KEYCLOAK": False,
            "AUDIENCE": "we",
            "VALIDATE_ISSUER": False,
        }
    )
    @mock.patch("keycloak.api.KeycloakApi.get_public_key")
    @mock.patch("keycloak.api.KeycloakApi.get_jwks")
    def test_authenticate_raise_exception_invalid_audiance(self, mock_get_jwt_key, mock_public_key):
        request = APIRequestFactory().get("/")

        mock_public_key.return_value = self.public_key
        mock_get_jwt_key.return_value = self.public_key
        token = jwt.encode(
            headers={
                "alg": "RS256",
                "typ": "JWT",
            },
            payload={
                "iss": "test",
                "aud": "something",
                "resource_access": {keycloak_settings.CLIENT_ID_FOR_AUTHZ_ROLES: "something"},
            },
            key=self.private_key,
        )
        request.META["HTTP_AUTHORIZATION"] = "Bearer " + token
        with self.assertRaises(InvalidAudience):
            self.backend.authenticate(request)

    @override_settings(
        KEYCLOAK_CONFIG={"VERIFY_TOKENS_WITH_KEYCLOAK": False, "VALIDATE_ISSUER": True}
    )
    @mock.patch("keycloak.api.KeycloakApi.get_public_key")
    @mock.patch("keycloak.api.KeycloakApi.get_jwks")
    def test_authenticate_raise_exception_invalid_issuer(self, mock_get_jwt_key, mock_public_key):
        request = APIRequestFactory().get("/")

        mock_public_key.return_value = self.public_key
        mock_get_jwt_key.return_value = self.public_key
        token = jwt.encode(
            headers={
                "alg": "RS256",
                "typ": "JWT",
            },
            payload={
                "iss": "test",
                "resource_access": {keycloak_settings.CLIENT_ID_FOR_AUTHZ_ROLES: "something"},
            },
            key=self.private_key,
        )
        request.META["HTTP_AUTHORIZATION"] = "Bearer " + token
        with self.assertRaises(InvalidIssuer):
            self.backend.authenticate(request)

    @override_settings(
        KEYCLOAK_CONFIG={
            "VERIFY_TOKENS_WITH_KEYCLOAK": False,
            "VALIDATE_ISSUER": False,
        }
    )
    @mock.patch("keycloak.api.KeycloakApi.get_public_key")
    @mock.patch("keycloak.api.KeycloakApi.get_jwks")
    def test_authenticate_raise_exception_invalid_issued_at(
        self, mock_get_jwt_key, mock_public_key
    ):
        request = APIRequestFactory().get("/")

        mock_public_key.return_value = self.public_key
        mock_get_jwt_key.return_value = self.public_key
        token = jwt.encode(
            headers={
                "alg": "RS256",
                "typ": "JWT",
            },
            payload={
                "iat": "not a datetime",
                "preferred_username": "my name",
                "resource_access": {keycloak_settings.CLIENT_ID_FOR_AUTHZ_ROLES: "something"},
            },
            key=self.private_key,
        )

        request.META["HTTP_AUTHORIZATION"] = "Bearer " + token
        with self.assertRaises(InvalidIssuedAt):
            self.backend.authenticate(request)

    @override_settings(
        KEYCLOAK_CONFIG={
            "VERIFY_TOKENS_WITH_KEYCLOAK": False,
            "VALIDATE_ISSUER": False,
        }
    )
    @mock.patch("keycloak.api.KeycloakApi.get_public_key")
    @mock.patch("keycloak.api.KeycloakApi.get_jwks")
    def test_authenticate_raise_exception_immature_signature(
        self, mock_get_jwt_key, mock_public_key
    ):
        request = APIRequestFactory().get("/")

        mock_public_key.return_value = self.public_key
        mock_get_jwt_key.return_value = self.public_key
        now_plus_one_hour: datetime = datetime.now() + timedelta(hours=1)
        token = jwt.encode(
            headers={
                "alg": "RS256",
                "typ": "JWT",
            },
            payload={
                "iat": datetime.timestamp(now_plus_one_hour),
                "preferred_username": "my name",
                "resource_access": {keycloak_settings.CLIENT_ID_FOR_AUTHZ_ROLES: "something"},
            },
            key=self.private_key,
        )

        request.META["HTTP_AUTHORIZATION"] = "Bearer " + token
        with self.assertRaises(ImmatureSignature):
            self.backend.authenticate(request)

    @override_settings(KEYCLOAK_CONFIG={"VERIFY_TOKENS_WITH_KEYCLOAK": False})
    @mock.patch("keycloak.api.KeycloakApi.get_public_key")
    @mock.patch("keycloak.api.KeycloakApi.get_jwks")
    def test_authenticate_raise_exception_invalid_missing_required_claim(
        self, mock_get_jwt_key, mock_public_key
    ):
        request = APIRequestFactory().get("/")

        mock_public_key.return_value = self.public_key
        mock_get_jwt_key.return_value = self.public_key
        token = jwt.encode(
            headers={
                "alg": "RS256",
                "typ": "JWT",
            },
            payload={"iss": "test"},
            key=self.private_key,
        )
        request.META["HTTP_AUTHORIZATION"] = "Bearer " + token
        with self.assertRaises(MissingRequiredClaim):
            self.backend.authenticate(request)

    @override_settings(KEYCLOAK_CONFIG={"VERIFY_TOKENS_WITH_KEYCLOAK": False})
    @mock.patch("keycloak.api.KeycloakApi.get_public_key")
    @mock.patch("keycloak.api.KeycloakApi.get_jwks")
    def test_authenticate_raise_exception_invalid_algorithm(
        self, mock_get_jwt_key, mock_public_key
    ):
        request = APIRequestFactory().get("/")

        mock_public_key.return_value = self.public_key
        mock_get_jwt_key.return_value = self.public_key
        token = jwt.encode(
            headers={
                "alg": "HS256",
                "typ": "JWT",
            },
            payload={
                "iss": "test",
                "resource_access": {keycloak_settings.CLIENT_ID_FOR_AUTHZ_ROLES: "something"},
            },
            key="very_key_very_secret",
        )
        request.META["HTTP_AUTHORIZATION"] = "Bearer " + token
        with self.assertRaises(InvalidAlgorithm):
            self.backend.authenticate(request)

    @override_settings(
        KEYCLOAK_CONFIG={
            "VERIFY_TOKENS_WITH_KEYCLOAK": False,
            "VALIDATE_ISSUER": False,
        }
    )
    def test_authenticate_raise_exception_token_invalid(self):
        request = APIRequestFactory().get("/")

        token = jwt.encode(
            headers={
                "alg": "RS256",
                "typ": "JWT",
            },
            payload={
                "iss": "test",
                "resource_access": {keycloak_settings.CLIENT_ID_FOR_AUTHZ_ROLES: "something"},
            },
            key=self.private_key,
        )
        request.META["HTTP_AUTHORIZATION"] = "Bearer " + token
        with self.assertRaises(InvalidToken):
            self.backend.authenticate(request)
