"""Module for django admin site tests"""

from django.test import TestCase
from django.test.client import Client


class AdminTestCase(TestCase):
    """Class for django admin site tests"""

    def setUp(self) -> None:
        super().setUp()
        self.client = Client()

    def test_admin_site_is_disabled(self):
        """Check admin site is disabled by default."""
        response = self.client.get("/admin/", follow=True)
        self.assertEqual(response.status_code, 404)
