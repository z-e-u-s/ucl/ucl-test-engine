"""Admin"""

from django.contrib import admin

from .models.scheduled_task import ScheduledTask
from .models.test_engine_test_execution import TestEngineTestExecution

# Register your models here.
admin.site.register(ScheduledTask)
admin.site.register(TestEngineTestExecution)
