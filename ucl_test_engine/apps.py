from django.apps import AppConfig


class uclTestEngineConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ucl_test_engine"

    def ready(self):
        # pylint: disable=unused-import, import-outside-toplevel
        import ucl_test_engine.signals  # noqa
