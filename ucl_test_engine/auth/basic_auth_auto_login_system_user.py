""" AUTHENTICATION_CLASS that automatically logs system user in """

from rest_framework.authentication import BaseAuthentication

from django.contrib.auth import get_user_model

USER_MODEL = get_user_model()


class BasicAuthAutoLoginSystemUser(BaseAuthentication):
    """
    AUTHENTICATION_CLASS that automatically logs system user in
    """

    def authenticate(self, request):
        """
        Authenticate the request by returning a two-tuple of (user, token).
        The user is always the system user.
        """
        user = USER_MODEL.objects.get(username="system")
        return (user, None)
