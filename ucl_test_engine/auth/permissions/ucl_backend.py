"""Permission for UclBackend"""

from keycloak.permissions import HasPermission
from ucl_test_engine.zeus_roles import ZeusRoles


class UclBackendPermission(HasPermission):
    """
    Global permission check if user has role 'ucl-backend" assigned.
    """

    permission = ZeusRoles.UCL_BACKEND.value
