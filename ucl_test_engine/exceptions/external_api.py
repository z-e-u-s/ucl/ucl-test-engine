"""Module for exception from external api calls"""

from rest_framework.exceptions import APIException


class ServiceUnavailable(APIException):
    """Exception to throw if a third party service is not reachable"""

    status_code = 503
    default_detail = "Service temporarily unavailable, try again later."
    default_code = "service_unavailable"


class InvalidSslCertificate(APIException):
    """Exception to throw if the external API returns an invalid SSL certificate"""

    status_code = 503
    default_detail = "Service has an invalid SSL certificate."
    default_code = "invalid_ssl_certificate"
