""" Command to set up UCL_TEST_ENGINE """

import logging
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from ucl_test_engine.services.cronjob import CronjobService

UserModel = get_user_model()
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """Command to load development data"""

    def handle(self, *args, **options):
        """Handle the command"""
        logger.info("Initializing UCL_TEST_ENGINE")
        self.create_scheduled_cronjobs()

    def create_scheduled_cronjobs(self):
        """Create default cronjobs / scheduled tasks"""
        CronjobService.create_default_cronjobs()
