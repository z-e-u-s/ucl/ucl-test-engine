""" Header middleware """

from django.conf import settings
from django.db import connection
from rest_framework import status

from rest_framework.response import Response
from ucl_test_engine.renderers.json_response_renderer import JSONResponseRenderer


class HealthCheckMiddleware:
    """
    Security setting for the header
    """

    def __init__(self, get_response):
        """
        :param request:
        :return:
        """
        self.get_response = get_response

    def __call__(self, request):
        # modify header
        if request.method == "GET":
            if request.path.endswith("/readinesscheck"):
                return self.readiness(request)
            if request.path.endswith("/healthcheck"):
                return self.healthiness(request)

        response = self.get_response(request)

        return response

    def __check_db_connection(self) -> bool:
        """Check if a database connection can be established."""
        try:
            cursor = connection.cursor()
            cursor.execute("SELECT 1")
            return True
        except Exception:  # pylint: disable=broad-except
            return False

    def healthiness(self, request):
        """
        Returns that the server is alive.
        """
        response = Response(status=status.HTTP_200_OK)
        response.accepted_renderer = JSONResponseRenderer()
        response.accepted_media_type = "application/json"
        response.renderer_context = {}
        response.render()
        return response

    def readiness(self, request):
        """Check authentication is turned on and databse is available"""
        if settings.DISABLE_AUTHENTICATION:
            response = Response(
                data={"ready": False, "details": "Authentication is disabled!"},
                status=status.HTTP_503_SERVICE_UNAVAILABLE,
            )
            response.accepted_renderer = JSONResponseRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            response.render()
            return response
        if not self.__check_db_connection():
            response = Response(
                data={"ready": False, "details": "Database connection failed!"},
                status=status.HTTP_503_SERVICE_UNAVAILABLE,
            )
            response.accepted_renderer = JSONResponseRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            response.render()
            return response

        response = Response(status=status.HTTP_200_OK)
        response.accepted_renderer = JSONResponseRenderer()
        response.accepted_media_type = "application/json"
        response.renderer_context = {}
        response.render()
        return response
