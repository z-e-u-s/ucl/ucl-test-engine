from django.conf import settings
from django.db import migrations


def create_system_user(apps, schema_editor):
    """create base user"""

    user = apps.get_model(settings.AUTH_USER_MODEL)
    user.objects.create_user(
        username="system",
        email="system@example.de",
        password="lirumlarum",
        is_active=0,
    )


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.RunPython(create_system_user),
    ]
