"""Abstract Base Entity Model."""

# Holds timestamps and user references of object creation and modification.
import uuid

from django.db import models


class Base(models.Model):
    """Abstract Base Entity class.

    Provides created and updated Timestamp field.
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    """:class:`UUID`"""
    created = models.DateTimeField(auto_now_add=True)
    """:class:`DateTimeField`: Timestamp of object creation."""
    updated = models.DateTimeField(null=True, blank=True)
    """:class:`DateTimeField`: Timestamp of last modification."""

    class Meta:
        abstract = True
