""" ScheduledTask model """

# pylint: disable=line-too-long
from croniter import croniter
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from ucl_test_engine.models.base import Base


def validate_cron_string(value):
    """Validate that the cron_string is a valid cron string."""

    if not croniter.is_valid(value):
        raise ValidationError("cron_string must be a valid cron string")


class ScheduledTask(Base):
    """
    The ScheduledTask model is used for managing tasks that are to be executed at specific times.

    These tasks are executed using the Django-Q scheduler.

    Attributes:

    * function_name: The fully qualified name of the function to be executed.
        This should include the path to the module and the function name.
        For example, this could be "ucl_test_engine.services.test_engine.TestEngineService.get_new_results_and_return_results_sync" if the function "get_new_results_and_return_results_sync"
        is defined in the "services.test_engine.py" file in the Django app "ucl_test_engine".
    * cron_string: A cron format string that determines when the task should be executed.
        The cron format consists of five fields representing minutes, hours, day of the month,
        month, and day of the week. For instance, the cron string "0 0 * * *" would mean that
        the task is executed every day at midnight.
    """

    class FunctionChoices(models.TextChoices):
        """Enum for all UCL_TEST_ENGINE cronjobs"""

        CHECK_FOR_NEW_TEST_RESULTS = (
            "ucl_test_engine.services.test_engine.TestEngineService.get_new_results_and_return_results_sync",
            _("get new results and return results"),
        )

    function_name = models.CharField(max_length=128, choices=FunctionChoices.choices, unique=True)
    """:class:`CharField`: describes the name of the function to be executed."""

    cron_string = models.CharField(max_length=64, validators=[validate_cron_string])
    """:class:`CharField`: describes the cron string for the task."""

    def __str__(self):
        return f"{self.get_function_name_display()} ({self.cron_string})"

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.full_clean()
        return super().save(force_insert, force_update, using, update_fields)
