"""Model for Rule test execution."""

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from ucl_test_engine.models.base import Base


class TestEngineTestExecution(Base):
    __test__ = False
    """Execution of a RuleTest."""

    execution_id = models.UUIDField(unique=True)
    """:class:`ForeignKey(RuleTest)`: Id of the execution object."""

    rule_id = models.UUIDField()
    """:class:`ForeignKey(RuleTest)`: Id of the rule that is tested."""

    response_url = models.TextField(max_length=500)
    """:class:`TextField`: url to send the result to."""

    test_events = models.JSONField()
    """:class:`JSONField`: List of Test event jsons"""

    result = models.IntegerField(
        null=True, validators=[MinValueValidator(-1), MaxValueValidator(2147483647)]
    )
    """
    :class:`PositiveIntegerField`: number of times the rule triggered.
    '-1' means the execution was stopped by an error.
    """

    def __str__(self):
        return str(self.execution_id)
