"""JsonResponseRenderer"""

import json

from djangorestframework_camel_case.util import camelize
from rest_framework.renderers import JSONRenderer
from rest_framework.utils.encoders import JSONEncoder


class JSONResponseRenderer(JSONRenderer):
    """Custom Renderer for Json Responses"""

    media_type = "application/json"
    charset = "utf-8"

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """return response dict as json string

        response contains an array field 'data' with the given data
        key names are converted to camelCase
        """

        data_dict = data
        status_code = str(renderer_context["response"].status_code)[0]

        # TODO remove or change message key
        message_as_dict = None

        if isinstance(data_dict, dict) and "message" in data_dict.keys():
            # custom response with message and data fields
            message = data_dict["message"]
            data = data_dict.get("data", [])
        elif isinstance(data_dict, dict) and "detail" in data_dict.keys():
            # default response with detail field
            message = data_dict["detail"]
            data = []
        elif status_code == "2":  # data
            message = ""
            data = data_dict
        else:  # error message
            message_as_dict = data_dict
            message = f"{data_dict}"
            data = []

        response_dict = {
            "data": camelize(data),
            "message": message,
        }
        if message_as_dict is not None and "errors" in message_as_dict:
            response_dict["errors"] = camelize(message_as_dict["errors"])

        return json.dumps(response_dict, cls=JSONEncoder)
