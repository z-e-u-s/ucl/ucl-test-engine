"""Serializer for HTTP response 207 body"""

# pylint: disable=R0801:duplicate-code

from rest_framework_dataclasses.serializers import DataclassSerializer

from ucl_test_engine.typing.http_response_207_body import (
    UclResponseWrapperBody,
)


class UclResponseWrapperBodySerializer(DataclassSerializer):
    """Serializer for UclResponseWrapperBody"""

    class Meta:
        dataclass = UclResponseWrapperBody

    def create(self, validated_data):
        raise NotImplementedError

    def update(self, instance, validated_data):
        raise NotImplementedError
