"""Serializer for Rule Test Executions"""

# pylint: disable=R0801:duplicate-code

from collections import OrderedDict

from rest_framework import serializers

from ucl_test_engine.models.test_engine_test_execution import TestEngineTestExecution


class RuleTestExecutionCreateValidationSerializer(serializers.ModelSerializer):
    """Serializer for RuleTestExecution creations"""

    test_events = serializers.ListField(child=serializers.DictField(), min_length=1)

    class Meta:
        model = TestEngineTestExecution
        fields = [
            "id",
            "execution_id",
            "rule_id",
            "response_url",
            "test_events",
            "created",
            "updated",
        ]
        read_only_fields = [
            "id",
            "created",
            "updated",
        ]

    def update(self, instance, validated_data):
        raise NotImplementedError


class RuleTestExecutionCreateValidationListSerializer(serializers.ListSerializer):
    """Serializer for RuleTestExecution creations"""

    child = RuleTestExecutionCreateValidationSerializer()
    min_length = 1
    allow_empty = False

    def update(self, instance, validated_data):
        raise NotImplementedError


class RuleTestExecutionForSiemExecutionSerializer(serializers.ModelSerializer):
    """Serializer for RuleTestExecution to be sent to siem"""

    # TODO remove if real zeus is used instead of mock
    mock = serializers.SerializerMethodField(
        "get_mock_object",
        read_only=True,
        required=False,
        allow_null=False,
    )

    # TODO remove if real zeus is used instead of mock
    def get_mock_object(self, execution: TestEngineTestExecution) -> dict | None:
        """add mock object if wanted"""
        mock_obj = None
        if "sre_to_create" in execution.test_events[0] or "delay" in execution.test_events[0]:
            mock_obj = {}
            if "sre_to_create" in execution.test_events[0]:
                mock_obj["sre_to_create"] = execution.test_events[0]["sre_to_create"]
            if "delay" in execution.test_events[0]:
                mock_obj["delay"] = execution.test_events[0]["delay"]
        return mock_obj

    class Meta:
        model = TestEngineTestExecution
        fields = ["execution_id", "rule_id", "test_events", "mock"]
        read_only_fields = [
            "execution_id",
            "rule_id",
            "test_events",
        ]

    def create(self, validated_data):
        raise NotImplementedError

    def update(self, instance, validated_data):
        raise NotImplementedError

    def to_representation(self, instance):
        result = super().to_representation(instance)
        return OrderedDict([(key, result[key]) for key in result if result[key] is not None])


class RuleTestExecutionUclResponseSerializer(serializers.ModelSerializer):
    """Serializer for RuleTestExecution to be sent back to ucl"""

    result_timestamp = serializers.DateTimeField(
        source="updated",
        read_only=True,
    )

    class Meta:
        model = TestEngineTestExecution
        fields = ["execution_id", "result", "result_timestamp"]
        read_only_fields = ["execution_id", "result", "result_timestamp"]

    def create(self, validated_data):
        raise NotImplementedError

    def update(self, instance, validated_data):
        raise NotImplementedError
