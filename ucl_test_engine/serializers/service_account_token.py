"""Serializer for ServiceAccountToken"""

# pylint: disable=R0801:duplicate-code

from rest_framework_dataclasses.serializers import DataclassSerializer

from ucl_test_engine.typing.service_account_token import (
    ServiceAccountToken,
)


class ServiceAccountTokenSerializer(DataclassSerializer):
    """Serializer for ServiceAccountToken"""

    class Meta:
        dataclass = ServiceAccountToken

    def create(self, validated_data):
        raise NotImplementedError

    def update(self, instance, validated_data):
        raise NotImplementedError
