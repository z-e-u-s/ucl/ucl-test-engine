"""service module for cronjobs"""

from django.contrib.auth import get_user_model

from ucl_test_engine.models.scheduled_task import ScheduledTask

UserModel = get_user_model()


class CronjobService:  # pylint:disable=R0903:too-few-public-methods
    """service for Cronjob logic"""

    @staticmethod
    def create_default_cronjobs() -> None:
        """Create default scheduled Tasks"""

        ScheduledTask.objects.get_or_create(
            function_name="ucl_test_engine.services.test_engine.TestEngineService."
            + "get_new_results_and_return_results_sync",
            defaults={"cron_string": "*/1 * * * *"},
        )
