""" Error handling for UclTestEngineTestEngine API."""

import re

# pylint: disable=unused-variable
# pylint: disable=unused-argument
# pylint: disable=E0401:import-error
from dataclasses import dataclass

from django.http import Http404
from rest_framework.exceptions import APIException
from rest_framework.views import exception_handler as drf_exception_handler
from rest_framework_dataclasses.serializers import DataclassSerializer


@dataclass
class UclTestEngineTestEngineError:
    """
    Represents an error encountered while processing an UclTestEngineTestEngine submission.
    """

    type: str
    code: str
    description: str
    help_text: str = None
    path: str = None
    field_description: str = None


def build_errors(
    detail: list | dict, exception_type: str, path: list, help_text: str
) -> list[UclTestEngineTestEngineError]:
    """
    Builds a list of `UclTestEngineTestEngineError` objects from a dictionary or a list.

    Args:
        detail (dict or list): The dict or list (list and detail view) to extract errors from.
        exception_type (str): The type of the exception, e.g. 'ValidationError'.
        path (list): A list of keys representing the path to the field where the error occurred.
        help_text (str, optional): Help text to include in the error. Defaults to None.

    Returns:
        list: A list of `UclTestEngineTestEngineError` objects representing the errors.
    """
    if isinstance(detail, list):
        return [
            UclTestEngineTestEngineError(
                type=exception_type,
                code=item["code"],
                description=clean_error_message(str(item["message"])),
                help_text=help_text,
                path=snake_to_camel(".".join(map(str, path))) or None,
                field_description=create_field_description(path),
            )
            for item in detail
        ]

    if isinstance(detail, dict):
        if "message" in detail and "code" in detail:
            return [
                UclTestEngineTestEngineError(
                    type=exception_type,
                    description=clean_error_message(str(detail["message"])),
                    code=detail["code"],
                    help_text=help_text,
                    path=snake_to_camel(".".join(map(str, path))) or None,
                    field_description=create_field_description(path),
                )
            ]

    errors = []
    for key, value in detail.items():
        errors += build_errors(value, exception_type, [*path, key], help_text)
    return errors


def create_field_description(path: list | str) -> str | None:
    """
    Generates a human-readable field description from a list of
    keys representing the path to the field.
    Args:
        path (list or str): The path to the field, represented as a list of keys or a string.
    Returns:
        str or None: A human-readable description of the field, or None if the path is empty.
    """
    if not path:
        return None
    if not isinstance(path, list):
        return snake_to_camel(path)
    field_description = ""
    for item in path:
        if isinstance(item, int):
            field_description += f"{item + 1}. Item: "
        else:
            field_description += f"{snake_to_camel(item)} "
    return field_description.strip()


def clean_error_message(error: str) -> str:
    """clean error message"""
    cleaned_error = re.sub(r"[^\w+><.\/_\-:\(\)]", " ", str(error)).replace(" > ", "> ")
    return re.sub(r"\s+", " ", cleaned_error).strip()


def normalize_exception(exc) -> list[UclTestEngineTestEngineError]:
    """
    Normalizes an exception into a list of `UclTestEngineTestEngineError` objects.
    """
    exception_type = exc.__class__.__name__
    help_text = getattr(exc, "help_text", None)
    if isinstance(exc, APIException):
        data = exc.get_full_details()
        return build_errors(data, exception_type, [], help_text)

    if isinstance(exc, Http404):
        return [
            UclTestEngineTestEngineError(
                type=exception_type,
                description="Resource or one of related resources not found",
                code="not_found",
                help_text=help_text,
            )
        ]
    return [
        UclTestEngineTestEngineError(
            type=exception_type,
            description="Unknown validation error",
            code="unknown",
            help_text=help_text,
        )
    ]


def exception_handler(exc, context):
    """Custom exception handler for UclTestEngineTestEngine API."""
    response = drf_exception_handler(exc, context)
    if response is not None:
        errors = normalize_exception(exc)
        response.data = {
            "errors": [UclTestEngineTestEngineErrorSerializer(error).data for error in errors]
        }
    return response


error_status_codes = ["400", "401", "403", "404"]

ERROR = {
    "content": {
        "application/json": {"schema": {"$ref": "#/components/schemas/ErrorResponseObject4xx"}}
    },
    "description": "",
}

ErrorResponseObject4xx = {
    "type": "object",
    "description": "Error response for 4xx",
    "properties": {
        "data": {"type": "array", "default": []},
        "message": {"type": "object", "default": {}},
        "errors": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "code": {"type": "string"},
                    "description": {"type": "string"},
                    "helpText": {"type": "string"},
                    "path": {"type": "string"},
                    "fieldDescription": {"type": "string"},
                },
                "default": {},
            },
        },
    },
}


def inject_errors_to_listing(result, generator, request, public):
    """Injects error responses to the listing view."""
    for path, config in result["paths"].items():
        for method, schema in config.items():
            for code in error_status_codes:
                schema["responses"][code] = schema["responses"].get(code) or ERROR

    result["components"]["schemas"]["ErrorResponseObject4xx"] = ErrorResponseObject4xx

    return result


def snake_to_camel(underscore: str) -> str:
    """Convert a string from snake_case to camelCase."""
    if "_" in underscore:
        list_of_strings = underscore.split("_")
        to_camel = list_of_strings[0]
        for item in list_of_strings[1:]:
            to_camel += item.title()
        return to_camel
    return underscore


class UclTestEngineTestEngineErrorSerializer(DataclassSerializer):
    """
    A serializer for `UclTestEngineTestEngineError` objects.
    """

    class Meta:
        dataclass = UclTestEngineTestEngineError
        extra_kwargs = {
            "type": {"help_text": "The exception type of the error"},
            "code": {
                "help_text": "Code of the error as text, "
                "equivalent to Backend Framework validation code"
            },
            "description": {"help_text": "Textual description of the error. "},
            "path": {
                "help_text": "Path to the field inside the object. "
                "Null means there is no specific field the error is associated with."
            },
            "help_text": {"help_text": "Shows the field help text if not empty"},
            "field_description": {
                "help_text": "Presents the error location to the user "
                "in a clear and user-friendly way"
            },
        }
