"""service module for KeycloakServiceAccount logic"""

import datetime
from threading import Lock

import requests
from django.conf import settings
from django.utils import timezone

from ucl_test_engine.exceptions.external_api import ServiceUnavailable
from ucl_test_engine.serializers.service_account_token import (
    ServiceAccountTokenSerializer,
)
from ucl_test_engine.typing.service_account_token import ServiceAccountToken

TIMEOUT_FOR_REQUESTS_IN_MS = 5000
MIN_VALID_TOKEN_TIME_BEFORE_REFRESH_IN_SECONDS = 30

SERVICE_ACCOUNT_TOKEN: ServiceAccountToken | None = None

LOCK = Lock()


class KeycloakServiceAccountService:  # pylint:disable=R0903:too-few-public-methods
    """service for KeycloakServiceAccount logic"""

    @staticmethod
    def get_service_account_access_token() -> str | None:
        """get a service account token from cache or a new one."""

        # if authentication is disabled there is also no way to get a service account token.
        # Therefore return None

        if settings.DISABLE_AUTHENTICATION is True:
            return None

        now: datetime.datetime = timezone.now()

        if (
            SERVICE_ACCOUNT_TOKEN is not None
            and SERVICE_ACCOUNT_TOKEN.expires_at is not None
            and (
                (SERVICE_ACCOUNT_TOKEN.expires_at - now)
                > datetime.timedelta(seconds=MIN_VALID_TOKEN_TIME_BEFORE_REFRESH_IN_SECONDS)
            )
        ):
            return SERVICE_ACCOUNT_TOKEN.access_token

        with LOCK:
            KeycloakServiceAccountService.__request_service_account_access_token()

        if SERVICE_ACCOUNT_TOKEN is None:
            return None

        return SERVICE_ACCOUNT_TOKEN.access_token

    @staticmethod
    def __set_service_account_access_token(service_account_token: ServiceAccountToken) -> None:
        """set a service account token in cache"""
        global SERVICE_ACCOUNT_TOKEN  # pylint: disable=global-statement
        SERVICE_ACCOUNT_TOKEN = service_account_token

    @staticmethod
    def __request_service_account_access_token() -> None:
        """get a service account token from cache or request a new one."""
        now: datetime.datetime = timezone.now()
        if (
            SERVICE_ACCOUNT_TOKEN is not None
            and SERVICE_ACCOUNT_TOKEN.expires_at is not None
            and (
                (SERVICE_ACCOUNT_TOKEN.expires_at - now)
                > datetime.timedelta(seconds=MIN_VALID_TOKEN_TIME_BEFORE_REFRESH_IN_SECONDS)
            )
        ):
            return

        try:
            res = requests.post(
                f"{settings.KEYCLOAK_CONFIG['SERVER_URL']}/realms/{settings.KEYCLOAK_CONFIG['REALM']}"  # pylint: disable=C0301:line-too-long
                + "/protocol/openid-connect/token",
                timeout=TIMEOUT_FOR_REQUESTS_IN_MS,
                data={
                    "grant_type": "client_credentials",
                    "client_id": settings.KEYCLOAK_CONFIG["CLIENT_ID"],
                    "client_secret": settings.KEYCLOAK_CONFIG["CLIENT_SECRET"],
                },
            )
            res.raise_for_status()

            # convert the json result to python object
            token = res.json()
            serializer = ServiceAccountTokenSerializer(data=token)
            serializer.is_valid(raise_exception=True)
            sat: ServiceAccountToken = serializer.validated_data
            # this must be adjusted if "expires_in" in token is changed from seconds to other unit
            sat.expires_at = now + datetime.timedelta(seconds=sat.expires_in)
            KeycloakServiceAccountService.__set_service_account_access_token(sat)
        except requests.exceptions.RequestException as exc:
            raise ServiceUnavailable(
                detail="Service cannot get service account token.",
                code="get_service_token_failed",
            ) from exc
