"""service module for siem communication"""

from aiohttp import ClientSession

from ucl_test_engine.serializers.rule_test_execution import (
    RuleTestExecutionForSiemExecutionSerializer,
)


class SiemConnectionInterface:
    """interface for communication with the siem"""

    @staticmethod
    def execute_tests(
        rule_test_executions: list[RuleTestExecutionForSiemExecutionSerializer],
    ) -> None:
        """Send rule_test_executions to siem to be tested / processed"""

    @staticmethod
    async def get_rule_test_result(
        rule_test_execution_id: list[RuleTestExecutionForSiemExecutionSerializer],
        session: ClientSession,
    ) -> None:
        """Check if rule_test_executions is finished testing and a result exists"""
