"""service module for test engine TEMP logic"""

import asyncio
import logging
import uuid
from collections.abc import Coroutine
from datetime import timedelta
from threading import Lock
from typing import Any

import aiohttp
from aiohttp import ClientSession, ClientTimeout
from asgiref.sync import async_to_sync, sync_to_async
from django.conf import settings
from django.utils import timezone
from djangorestframework_camel_case import util as CamelCaseUtil
from rest_framework import status
from rest_framework.exceptions import AuthenticationFailed, NotFound

from ucl_test_engine.exceptions.external_api import ServiceUnavailable
from ucl_test_engine.models.test_engine_test_execution import TestEngineTestExecution
from ucl_test_engine.serializers.http_response_207_body import (
    UclResponseWrapperBodySerializer,
)
from ucl_test_engine.serializers.rule_test_execution import (
    RuleTestExecutionUclResponseSerializer,
)
from ucl_test_engine.services.service_account import KeycloakServiceAccountService
from ucl_test_engine.services.zeus_mock.zeus_connection_service import (
    ZeusConnectionService,
)
from ucl_test_engine.typing.http_response_207_body import (
    HttpResponse207BodyData,
    HttpResponse207DataElement,
    UclResponseWrapperBody,
)
from ucl_test_engine.typing.service_account_token import ServiceAccountToken

TIMEOUT_FOR_REQUESTS_IN_MS = 5000

LOCK_GET_AND_RETURN_RESULTS = Lock()

SERVICE_ACCOUNT_TOKEN: ServiceAccountToken = None
logger = logging.getLogger(__name__)


# change assignment if other siem_connection_service implementation is used
SiemConnectionService = ZeusConnectionService


class TestEngineService:
    __test__ = False
    """service for TestEngine logic"""

    @staticmethod
    def __abort_test_execution(execution: TestEngineTestExecution) -> None:
        """
        Abort the test_execution by setting result to -1 and save in DB.
        This should be done on error executing it or waiting to long for a result.
        """

        execution.result = -1
        execution.updated = timezone.now().isoformat(sep="T").replace("+00:00", "Z")
        execution.save()

    @staticmethod
    def test_rule_test_executions(
        test_executions: list[TestEngineTestExecution],
    ) -> None:
        """start executions of rule_test_executions"""
        try:
            SiemConnectionService.execute_tests(test_executions)
        except Exception as exc:
            # if SiemConnectionService returns an error,
            # we set result = -1 to indicate that it was not executed.
            for rule_test_execution in test_executions:
                TestEngineService.__abort_test_execution(rule_test_execution)
            raise exc

    @staticmethod
    async def __send_result_of_one_url(
        test_executions_queue_objects: list[TestEngineTestExecution],
        url: str,
        session: ClientSession,
    ) -> HttpResponse207BodyData:
        """send the results back to the given response_url"""

        serializer_results = RuleTestExecutionUclResponseSerializer(
            test_executions_queue_objects, many=True
        )

        try:
            timeout = ClientTimeout(total=5)

            ucl_test_engine_service_token: str = (
                KeycloakServiceAccountService.get_service_account_access_token()
            )

            async with session.patch(
                url,
                json=serializer_results.data,
                timeout=timeout,
                headers={"Authorization": f"Bearer {ucl_test_engine_service_token}"},
            ) as resp:
                resp.raise_for_status()

                ucl_response = await resp.json()
                ucl_response = CamelCaseUtil.underscoreize(ucl_response)
                serializer = UclResponseWrapperBodySerializer(data=ucl_response)
                serializer.is_valid(raise_exception=True)
                ucl_response_validated: UclResponseWrapperBody = serializer.validated_data
                return ucl_response_validated.data

        except aiohttp.client_exceptions.ClientResponseError as exc:
            # nosemgrep: logging-error-without-handling
            logger.error(msg="__send_result_of_one_url failed", exc_info=exc)
            if exc.status is not None and exc.status == status.HTTP_404_NOT_FOUND:
                raise NotFound(
                    detail="Response service not found.", code="response_service_not_found"
                ) from exc

            if exc.status is not None and exc.status == status.HTTP_401_UNAUTHORIZED:
                raise AuthenticationFailed(
                    detail="Response service denied access.",
                    code="response_service_authentication_failed",
                ) from exc

            raise ServiceUnavailable(
                detail="Response service temporarily unavailable try again later.",
                code="response_service_unavailable",
            ) from exc

        except Exception as exc:
            # nosemgrep: logging-error-without-handling
            logger.error(msg="__send_result_of_one_url failed", exc_info=exc)
            raise ServiceUnavailable(
                detail="Response service temporarily unavailable try again later.",
                code="response_service_unavailable",
            ) from exc

    @staticmethod
    async def __return_results_of_one_url(
        test_executions_queue_objects: list[TestEngineTestExecution],
        url: str,
        session: ClientSession,
    ) -> None:
        """handle the returning and cleanup for the finished results of one url"""

        ucl_response_validated: HttpResponse207BodyData = (
            await TestEngineService.__send_result_of_one_url(
                test_executions_queue_objects, url, session
            )
        )

        # filter failed executions
        failed_executions: list[HttpResponse207DataElement] = list(
            filter(
                lambda execution: execution.status == 400,
                ucl_response_validated.data,
            )
        )
        # map failed execution_id
        failed_execution_ids: list[str] = list(
            map(
                lambda failed_execution: str(failed_execution.resource.execution_id),
                failed_executions,
            )
        )

        for fei in failed_execution_ids:
            logger.warning("Send and update result for execution with id: '%s' failed.", fei)

        test_execution_ids: list[uuid.UUID] = list(
            map(lambda test_execution: test_execution.id, test_executions_queue_objects)
        )
        await TestEngineTestExecution.objects.filter(id__in=test_execution_ids).adelete()

    @staticmethod
    async def __return_results(session: ClientSession) -> None:
        """handle the returning and cleanup for all finished results"""

        executions_with_result = TestEngineTestExecution.objects.filter(result__isnull=False)

        mapping: dict = {}
        async for execution in executions_with_result:
            if (execution.response_url in mapping) is True:
                mapping[execution.response_url].append(execution)
            else:
                mapping[execution.response_url] = [execution]

        promises: list[Coroutine[Any, Any, None]] = []

        for key, value in mapping.items():
            res = TestEngineService.__return_results_of_one_url(value, key, session)
            promises.append(res)
        await asyncio.gather(*promises)

    @staticmethod
    def __save_result_in_db(execution: TestEngineTestExecution, result: int | None) -> None:
        """
        Save result for an execution in the db
        """

        if result is not None:
            execution.result = result
            execution.updated = timezone.now().isoformat(sep="T").replace("+00:00", "Z")
            execution.save()

    @staticmethod
    async def __get_results_and_save_in_db(
        execution: TestEngineTestExecution, session: ClientSession
    ) -> None:
        """
        get the result for an execution.
        If a result exists save it in the db
        """

        result: int | None = await SiemConnectionService.get_rule_test_result(
            execution.execution_id, session
        )

        await sync_to_async(TestEngineService.__save_result_in_db)(execution, result)

    @staticmethod
    async def get_new_results_and_return_results() -> None:
        """
        Executions without result will be checked,
         if they have finished and the result can be fetched.
        Executions that are waiting too long for a result will be aborted
         (limit is set via MAX_WAITING_TIME_TO_ABORT_EXECUTION_IN_SEC).
        LOCK_GET_AND_RETURN_RESULTS prevents concurrently executing this method.
         Otherwise it might do things twice (double work and send results two times.).
        "__get_results_and_save_in_db" is executed concurrently to speed it up.
        At the end '__return_results' sends the found results back to the wanted responseURL.
        """

        with LOCK_GET_AND_RETURN_RESULTS:
            # TODO this session should be created somewhere else
            #  where that outlives this method call, that it is not created very time
            session = ClientSession()
            currently_running_valid_executions = TestEngineTestExecution.objects.filter(
                result__isnull=True,
                created__gte=timezone.now()
                - timedelta(seconds=settings.MAX_WAITING_TIME_TO_ABORT_EXECUTION_IN_SEC),
            )
            currently_running_too_long_waiting_executions = TestEngineTestExecution.objects.filter(
                result__isnull=True,
                created__lt=timezone.now()
                - timedelta(seconds=settings.MAX_WAITING_TIME_TO_ABORT_EXECUTION_IN_SEC),
            )

            async for execution in currently_running_too_long_waiting_executions:
                await sync_to_async(TestEngineService.__abort_test_execution)(execution)

            # execute __get_results_and_save_in_db in parallel and await all responses
            try:
                promises: list[Coroutine[Any, Any, None]] = []

                async for execution in currently_running_valid_executions:
                    promises.append(
                        TestEngineService.__get_results_and_save_in_db(execution, session)
                    )

                await asyncio.gather(*promises)
            except Exception as exc:
                logger.exception(  # nosemgrep: python.lang.best-practice.logging-error-without-handling.logging-error-without-handling # pylint: disable=C0301:line-too-long
                    # do nothing
                    "Error during get_new_results_and_return_results"
                )
                raise exc

            await TestEngineService.__return_results(session)

            await session.close()

    @staticmethod
    def get_new_results_and_return_results_sync() -> None:
        """
        Synchronous method that call the async version of this method.
        This method is called by the scheduler/cronjob.
        """

        async_to_sync(TestEngineService.get_new_results_and_return_results)()
