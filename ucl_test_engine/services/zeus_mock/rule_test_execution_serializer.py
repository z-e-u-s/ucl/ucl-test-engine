"""Serializer for Rule Test Executions"""

# pylint: disable=R0801:duplicate-code

from rest_framework_dataclasses.serializers import DataclassSerializer

from ucl_test_engine.services.zeus_mock.test_execution_dataclass import (
    OpenSearchGetIndexSearchResponse,
)


class OpenSearchGetIndexSearchResponseSerializer(DataclassSerializer):
    """
    Serializer for OpenSearch GET Request response on an index with '/_search'
    """

    class Meta:
        dataclass = OpenSearchGetIndexSearchResponse

    def create(self, validated_data):
        raise NotImplementedError

    def update(self, instance, validated_data):
        raise NotImplementedError
