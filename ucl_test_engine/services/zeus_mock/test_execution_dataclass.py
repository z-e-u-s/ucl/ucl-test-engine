"""module for opensearch dataclasses"""

from dataclasses import dataclass


@dataclass
class OpenSearchGetIndexSearchResponseShards:
    """
    Nest part of class for Opensearch response,
     when making a GET request to an index with '/_search'.
    This is the value of the '_shards' field.
    """

    __test__ = False

    total: int
    successful: int
    skipped: int
    failed: int


@dataclass
class OpenSearchGetIndexSearchResponseHitsTotal:
    """
    Nest part of class for Opensearch response,
     when making a GET request to an index with '/_search'.
    This is the value of the 'hits/total' field.
    """

    __test__ = False

    value: int
    relation: str


@dataclass
class OpenSearchGetIndexSearchResponseHitsHitsArrayDocumentSource:
    """
    Nest part of class for Opensearch response,
     when making a GET request to an index with '/_search'.
    This is a subset of the value of the 'hits/hits[]/_source' field.
    """

    __test__ = False

    pre_detection_id: str
    execution_id: str | None = None


@dataclass
class OpenSearchGetIndexSearchResponseHitsHitsArrayDocument:
    """
    Nest part of class for Opensearch response,
     when making a GET request to an index with '/_search'.
    This is the value of the 'hits/hits' field-array.
    """

    __test__ = False

    _index: str
    _id: str
    _score: float
    _source: OpenSearchGetIndexSearchResponseHitsHitsArrayDocumentSource


@dataclass
class OpenSearchGetIndexSearchResponseHits:
    """
    Nest part of class for Opensearch response,
     when making a GET request to an index with '/_search'.
    This is the value of the 'hits' field.
    """

    __test__ = False

    total: OpenSearchGetIndexSearchResponseHitsTotal
    max_score: float | None
    hits: list[OpenSearchGetIndexSearchResponseHitsHitsArrayDocument]


@dataclass
class OpenSearchGetIndexSearchResponse:
    """
    Base class for Opensearch response when making a GET request to an index with '/_search'
    """

    __test__ = False

    took: int
    timed_out: bool
    _shards: OpenSearchGetIndexSearchResponseShards
    hits: OpenSearchGetIndexSearchResponseHits
