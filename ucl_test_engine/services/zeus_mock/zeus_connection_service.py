"""service module for zeus communication"""

import uuid

import aiohttp
import requests
from aiohttp import BasicAuth, ClientSession, ClientTimeout
from django.conf import settings
from rest_framework import status
from rest_framework.exceptions import AuthenticationFailed, NotFound

from ucl_test_engine.exceptions.external_api import (
    InvalidSslCertificate,
    ServiceUnavailable,
)
from ucl_test_engine.models.test_engine_test_execution import TestEngineTestExecution
from ucl_test_engine.serializers.rule_test_execution import (
    RuleTestExecutionForSiemExecutionSerializer,
)
from ucl_test_engine.services.siem_connection_interface import SiemConnectionInterface
from ucl_test_engine.services.zeus_mock.rule_test_execution_serializer import (
    OpenSearchGetIndexSearchResponseSerializer,
)
from ucl_test_engine.services.zeus_mock.test_execution_dataclass import (
    OpenSearchGetIndexSearchResponse,
)

# Pagination limit for opensearch that shall be used in queries.
# Currently maximum is 10.000. Do not set above.
OPENSEARCH_PAGINATION_LIMIT: int = settings.OPENSEARCH_PAGINATION_LIMIT

TIMEOUT_FOR_REQUESTS_IN_MS = 10000


class ZeusConnectionService(SiemConnectionInterface):
    """service for communication with zeus"""

    @staticmethod
    def __get_open_serach_pagination_limit() -> int:
        """get OPENSEARCH_PAGINATION_LIMIT"""

        return OPENSEARCH_PAGINATION_LIMIT

    @staticmethod
    def execute_tests(
        rule_test_executions: list[TestEngineTestExecution],
    ) -> None:
        """Send rule_test_executions to siem to be tested / processed"""
        serializer = RuleTestExecutionForSiemExecutionSerializer(rule_test_executions, many=True)
        send_data = serializer.data

        try:
            res = requests.post(
                f"{settings.SIEM_BASE_URL}",
                # TODO add Auth when existing in siem
                timeout=TIMEOUT_FOR_REQUESTS_IN_MS,
                json=send_data,
                verify=settings.SIEM_VERIFY_SSL_CERTIFICATE,
            )
            res.raise_for_status()
        except requests.exceptions.RequestException as exc:
            if exc.response is not None and exc.response.status_code == status.HTTP_404_NOT_FOUND:
                raise NotFound(
                    detail="Zeus mock service not found.", code="zeus_mock_service_not_found"
                ) from exc
            if (
                exc.response is not None
                and exc.response.status_code == status.HTTP_401_UNAUTHORIZED
            ):
                raise AuthenticationFailed(
                    detail="Zeus mock service denied access.",
                    code="zeus_mock_service_authentication_failed",
                ) from exc
            raise ServiceUnavailable(
                detail="Zeus mock service temporarily unavailable try again later.",
                code="zeus_mock_service_unavailable",
            ) from exc

    @staticmethod
    async def __get_predetection_ids_of_execution_from_events_index(
        rule_test_execution_id: uuid.UUID, session: ClientSession, start_index: int = 0
    ) -> list[str]:
        """Check if rule_test_execution is finished testing by checking if results exist"""
        try:
            timeout = ClientTimeout(total=10)
            basic_auth = BasicAuth(
                settings.OPENSEARCH_USER_NAME,
                password=settings.OPENSEARCH_USER_PASSWORD,
                encoding="latin1",
            )

            async with session.get(
                f"{settings.OPENSEARCH_BASE_URL}/event/_search",
                json={
                    "query": {"term": {"execution_id": str(rule_test_execution_id)}},
                    "from": start_index,
                    "size": ZeusConnectionService.__get_open_serach_pagination_limit(),
                },
                timeout=timeout,
                auth=basic_auth,
                ssl=settings.OPENSEARCH_VERIFY_SSL_CERTIFICATE,
            ) as resp:
                resp.raise_for_status()

                open_search_response = await resp.json()
                serializer = OpenSearchGetIndexSearchResponseSerializer(data=open_search_response)
                serializer.is_valid(raise_exception=True)
                open_search_response_validated: OpenSearchGetIndexSearchResponse = (
                    serializer.validated_data
                )

                # check if ruleTestExecution was found and therefore processed.
                if open_search_response_validated.hits.total.value > 0:
                    pre_detection_ids: list[str] = list(
                        map(
                            lambda hit: hit._source.pre_detection_id,
                            open_search_response_validated.hits.hits,
                        )
                    )
                    return pre_detection_ids

                return []
        except aiohttp.client_exceptions.ClientResponseError as exc:
            if exc.status is not None and exc.status == status.HTTP_404_NOT_FOUND:
                raise NotFound(
                    detail="Opensearch service not found.", code="opensearch_service_not_found"
                ) from exc
            if exc.status is not None and exc.status == status.HTTP_401_UNAUTHORIZED:
                raise AuthenticationFailed(
                    detail="Opensearch service denied access.",
                    code="opensearch_service_authentication_failed",
                ) from exc
            raise ServiceUnavailable(
                detail="Opensearch service temporarily unavailable try again later.",
                code="opensearch_service_unavailable",
            ) from exc
        except aiohttp.client_exceptions.ClientConnectorCertificateError as exc:
            raise InvalidSslCertificate(  # pylint:disable=E0705:bad-exception-cause
                detail="Opensearch service temporarily unavailable try again later.",
                code="opensearch_service_unavailable",
            ) from exc

    @staticmethod
    async def __get_records_count_by_predetection_ids_from_sre_index(
        pre_detection_ids: list[str], session: ClientSession
    ) -> int:
        """Check if rule_test_execution is finished testing by checking if results exist"""

        try:
            timeout = ClientTimeout(total=10)
            basic_auth = BasicAuth(
                settings.OPENSEARCH_USER_NAME,
                password=settings.OPENSEARCH_USER_PASSWORD,
                encoding="latin1",
            )
            async with session.get(
                f"{settings.OPENSEARCH_BASE_URL}/sre/_search",
                json={"query": {"terms": {"pre_detection_id": pre_detection_ids}}},
                timeout=timeout,
                auth=basic_auth,
                ssl=settings.OPENSEARCH_VERIFY_SSL_CERTIFICATE,
            ) as resp:
                resp.raise_for_status()

                open_search_response = await resp.json()
                serializer = OpenSearchGetIndexSearchResponseSerializer(data=open_search_response)
                serializer.is_valid(raise_exception=True)
                open_search_response_validated: OpenSearchGetIndexSearchResponse = (
                    serializer.validated_data
                )

                # check if ruleTestExecution was found and therefore processed.
                return open_search_response_validated.hits.total.value
        except aiohttp.client_exceptions.ClientResponseError as exc:
            if exc.status is not None and exc.status == status.HTTP_404_NOT_FOUND:
                raise NotFound(
                    detail="Opensearch service not found.", code="opensearch_service_not_found"
                ) from exc
            if exc.status is not None and exc.status == status.HTTP_401_UNAUTHORIZED:
                raise AuthenticationFailed(
                    detail="Opensearch service denied access.",
                    code="opensearch_service_authentication_failed",
                ) from exc
            raise ServiceUnavailable(
                detail="Opensearch service temporarily unavailable try again later.",
                code="opensearch_service_unavailable",
            ) from exc
        except aiohttp.client_exceptions.ClientConnectorCertificateError as exc:
            raise InvalidSslCertificate(  # pylint:disable=E0705:bad-exception-cause
                detail="Opensearch service temporarily unavailable try again later.",
                code="opensearch_service_unavailable",
            ) from exc

    @staticmethod
    async def get_rule_test_result(
        rule_test_execution_id: uuid.UUID, session: ClientSession
    ) -> int | None:
        """Check if rule_test_executions is finished testing and a result exists"""

        pre_detection_ids: list[str] = []

        # max retruned items per query are 10.000 for opensearch.
        # Get more items if request returned 10.000 items
        event_request_index: int = 0
        while True:
            new_predetection_ids: list[str] = (
                await ZeusConnectionService.__get_predetection_ids_of_execution_from_events_index(
                    rule_test_execution_id, session, event_request_index
                )
            )
            pre_detection_ids.extend(new_predetection_ids)
            event_request_index += ZeusConnectionService.__get_open_serach_pagination_limit()
            if (
                len(new_predetection_ids)
                < ZeusConnectionService.__get_open_serach_pagination_limit()
            ):
                break

        # check if rule_test_execution is processed. If not do nothing and return.
        if len(pre_detection_ids) == 0:
            return

        # if rule_test_executions is processed get the results count.
        sre_records_count: int = (
            await ZeusConnectionService.__get_records_count_by_predetection_ids_from_sre_index(
                pre_detection_ids, session
            )
        )

        return sre_records_count
