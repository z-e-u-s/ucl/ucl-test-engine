""" Signals for the UCL_TEST_ENGINE app. """

from datetime import datetime
from django.utils import timezone
from django.dispatch import receiver
from django.db.models.signals import post_save
from django_q.models import Schedule
from croniter import croniter
from .models.scheduled_task import ScheduledTask


@receiver(post_save, sender=ScheduledTask)
def update_schedule(sender, instance, **kwargs):  # pylint: disable=unused-argument
    """Update the schedule for a ScheduledTask."""

    now: datetime = timezone.now()
    next_run: datetime = croniter(expr_format=str(instance.cron_string), start_time=now).get_next(
        datetime
    )
    next_run_iso: str = next_run.isoformat()

    schedule, created = Schedule.objects.get_or_create(
        name=instance.id,
        defaults={
            "func": instance.function_name,
            "cron": instance.cron_string,
            "next_run": next_run_iso,
            "schedule_type": Schedule.CRON,
        },
    )
    if not created:
        schedule.func = instance.function_name
        schedule.cron = instance.cron_string
        schedule.next_run = next_run_iso
        schedule.save()
