""" Test ScheduledTask Model """

import pytest

from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone
from django_q.models import Schedule

from ucl_test_engine.models.scheduled_task import ScheduledTask
from ucl_test_engine.tests.set_up import CreateUclTestEngineData


class ScheduledTaskModelTest(CreateUclTestEngineData, TestCase):
    """Test ScheduledTask"""

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def setUp(self):
        self.init_test_user()
        self.base_data = {
            "function_name": (ScheduledTask.FunctionChoices.CHECK_FOR_NEW_TEST_RESULTS.value),
            "updated": timezone.now(),
            "cron_string": "59 23 29 1 *",
        }

    def test_cron_string_validation(self):
        """Test that the cron_string field is validated correctly."""
        valid_cron_strings = [
            "@annually",
            "@yearly",
            "@monthly",
            "@weekly",
            "@daily",
            "@hourly",
            "* * * * *",
            "0 0 1 * *",
            "*/2 * * * *",
            "59 23 29 1 *",
        ]

        for idx, cron_string in enumerate(  # pylint:disable=W0612:unused-variable
            valid_cron_strings
        ):
            self.base_data["cron_string"] = cron_string
            task = ScheduledTask.objects.create(**self.base_data)
            task.full_clean()
            self.assertEqual(str(task), f"{task.get_function_name_display()} ({cron_string})")
            task.delete()

        invalid_cron_strings = [
            "not a valid cron string",
            "@every 5m",
            "0 0 0",
        ]

        for cron_string in invalid_cron_strings:
            self.base_data["cron_string"] = cron_string
            # task = ScheduledTask(**self.base_data)
            with self.assertRaises(ValidationError) as error:
                ScheduledTask.objects.create(**self.base_data)

            self.assertDictEqual(
                error.exception.message_dict,
                {"cron_string": ["cron_string must be a valid cron string"]},
            )

    def test_schedule_creation_signal(self):
        """Test if the Schedule is correctly created when a ScheduledTask is created."""
        scheduled_task = ScheduledTask.objects.create(
            **self.base_data,
        )
        schedule = Schedule.objects.get(name=scheduled_task.id)
        self.assertEqual(schedule.func, scheduled_task.function_name)
        self.assertEqual(schedule.cron, scheduled_task.cron_string)

    def test_schedule_update_signal(self):
        """Test if the Schedule is correctly updated when a ScheduledTask is updated."""
        scheduled_task = ScheduledTask.objects.create(
            **self.base_data,
        )
        scheduled_task.cron_string = "*/5 * * * *"
        scheduled_task.save()
        schedule = Schedule.objects.get(name=scheduled_task.id)
        self.assertEqual(schedule.cron, scheduled_task.cron_string)

    def test_schedule_creation_0_1(self):
        scheduled_task: ScheduledTask = ScheduledTask.objects.create(
            function_name=(ScheduledTask.FunctionChoices.CHECK_FOR_NEW_TEST_RESULTS.value),
            cron_string="5 1 * * *",
        )
        self.assertIsInstance(scheduled_task, ScheduledTask)

    def test_schedule_creation_0_2(self):
        with pytest.raises(ValidationError) as error:
            ScheduledTask.objects.create(
                function_name="ucl_test_engine.services.test_engine.TestEngineService."
                + "test_not_accepted",
                cron_string="5 1 * * *",
            )

        self.assertEqual(
            str(error.value),
            "{'function_name': [\"Value 'ucl_test_engine.services.test_engine.TestEngineService."
            + "test_not_accepted' is not a valid choice.\"]}",
        )
