""" Test Environment Model """

from django.test.testcases import TestCase
from ucl_test_engine.tests.set_up import CreateUclTestEngineData
from ucl_test_engine.models.test_engine_test_execution import TestEngineTestExecution


class UclModelTestsTestEngineTestExecution(CreateUclTestEngineData, TestCase):
    """Test TestEngineTestExecution"""

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def test_test_execution(self):
        self.init_test_execution_1_without_result()
        test_executions = TestEngineTestExecution.objects.all()
        self.assertEqual(len(test_executions), 1)
        self.assertEqual(
            str(test_executions[0]), str(self.test_execution_1_without_result.execution_id)
        )
