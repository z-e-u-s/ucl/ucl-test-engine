""" Test Permissions """

from django.test.testcases import SimpleTestCase

from ucl_test_engine.auth.permissions.ucl_backend import UclBackendPermission

from ucl_test_engine.zeus_roles import ZeusRoles


class ZeusPermissionTests(SimpleTestCase):
    """Test Permissions"""

    @classmethod
    def setUpClass(cls):
        super()

    def test_ucl_backend_permission_set_correctly(self):
        """check if permission is correctly set"""

        self.assertEqual(UclBackendPermission.permission, ZeusRoles.UCL_BACKEND.value)
