""" Test Serializer for UclResponseWrapperBodySerializer"""

import uuid
import pytest

from django.test.testcases import SimpleTestCase

from ucl_test_engine.serializers.http_response_207_body import UclResponseWrapperBodySerializer
from ucl_test_engine.typing.http_response_207_body import (
    UclResponseWrapperBody,
    HttpResponse207Metadata,
    HttpResponse207DataElement,
    HttpResponse207BodyData,
)
from ucl_test_engine.typing.test_execution_dataclass import (
    RuleTestExecutionResult,
)


class SerializerTestsUclResponseWrapperBody(SimpleTestCase):
    """Test Serializer UclResponseWrapperBody"""

    @classmethod
    def setUpClass(cls):
        super()

    def test_UclResponseWrapperBodySerializer_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """
        is_valid shall succeed.
        Readonly and unknown fields are not in validated_data
        """

        data = {
            "data": {
                "data": [
                    {
                        "message": "some message success",
                        "resource": {
                            "execution_id": "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                            "result": 1,
                            "result_timestamp": "2021-11-11T11:11:11Z",
                        },
                        "status": 200,
                    },
                    {
                        "message": "some message fail",
                        "resource": {
                            "execution_id": "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                            "result": 1,
                            "result_timestamp": "2021-11-11T11:11:11Z",
                        },
                        "status": 400,
                    },
                ],
                "metadata": {"failure": 1, "success": 1, "total": 2},
            },
            "message": "some message",
        }

        serializer = UclResponseWrapperBodySerializer(data=data)
        self.assertTrue(serializer.is_valid())

        self.assertEqual(
            serializer.validated_data,
            UclResponseWrapperBody(
                data=HttpResponse207BodyData(
                    data=[
                        HttpResponse207DataElement(
                            message="some message success",
                            resource=RuleTestExecutionResult(
                                **{
                                    "execution_id": uuid.UUID(
                                        "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4"
                                    ),
                                    "result": 1,
                                    "result_timestamp": "2021-11-11T11:11:11Z",
                                }
                            ),
                            status=200,
                        ),
                        HttpResponse207DataElement(
                            message="some message fail",
                            resource=RuleTestExecutionResult(
                                **{
                                    "execution_id": uuid.UUID(
                                        "33236e07-2ef1-4f3b-9497-7ece4d7ff587"
                                    ),
                                    "result": 1,
                                    "result_timestamp": "2021-11-11T11:11:11Z",
                                }
                            ),
                            status=400,
                        ),
                    ],
                    metadata=HttpResponse207Metadata(failure=1, success=1, total=2),
                ),
                message="some message",
            ),
        )

    def test_UclResponseWrapperBodySerializer_fail_missing_fields(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall fail, missing 'data' and 'metadata'"""

        data = {}

        serializer = UclResponseWrapperBodySerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(set(serializer.errors), {"data", "message"})

    def test_UclResponseWrapperBodySerializer_fail_missing_fields_in_wrapper_data(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall fail, missing 'data' and 'metadata'"""

        data = {"data": {}, "message": ""}

        serializer = UclResponseWrapperBodySerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(set(serializer.errors["data"]), {"data", "metadata"})

    def test_UclResponseWrapperBodySerializer_fail_missing_fields_in_test_attribute_data(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall fail, missing fields in 'data' attribute"""

        data = {
            "data": {
                "data": [
                    {},
                    {
                        "message": "some message fail",
                        "resource": {
                            "execution_id": "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                            "result": 1,
                            "result_timestamp": "2021-11-11T11:11:11Z",
                        },
                        "status": 400,
                    },
                ],
                "metadata": {"failure": 1, "success": 1, "total": 2},
            },
            "message": "",
        }

        serializer = UclResponseWrapperBodySerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(
            set(serializer.errors["data"]["data"][0]), {"message", "resource", "status"}
        )

    def test_UclResponseWrapperBodySerializer_fail_missing_fields_in_test_attribute_resource(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall fail, missing fields in 'data' attribute"""

        data = {
            "data": {
                "data": [
                    {
                        "message": "some message fail",
                        "resource": {},
                        "status": 400,
                    },
                ],
                "metadata": {"failure": 1, "success": 1, "total": 2},
            },
            "message": "",
        }

        serializer = UclResponseWrapperBodySerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(
            set(serializer.errors["data"]["data"][0]["resource"]),
            {"execution_id", "result", "result_timestamp"},
        )

    def test_UclResponseWrapperBodySerializer_fail_missing_fields_in_test_attribute_metadata(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall fail, missing fields in 'metadata' attribute"""

        data = {
            "data": {
                "data": [
                    {
                        "message": "some message fail",
                        "resource": {
                            "execution_id": "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                            "result": 1,
                            "result_timestamp": "2021-11-11T11:11:11Z",
                        },
                        "status": 400,
                    },
                ],
                "metadata": {},
            },
            "message": "",
        }

        serializer = UclResponseWrapperBodySerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(
            set(serializer.errors["data"]["metadata"]), {"failure", "success", "total"}
        )

    def test_UclResponseWrapperBodySerializer_to_representation_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """to_representation shall succeed and return correct data"""
        serializer = UclResponseWrapperBodySerializer(
            UclResponseWrapperBody(
                data=HttpResponse207BodyData(
                    data=[
                        HttpResponse207DataElement(
                            message="some message success",
                            resource=RuleTestExecutionResult(
                                **{
                                    "execution_id": uuid.UUID(
                                        "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4"
                                    ),
                                    "result": 1,
                                    "result_timestamp": "2021-11-11T11:11:11Z",
                                }
                            ),
                            status=200,
                        ),
                        HttpResponse207DataElement(
                            message="some message fail",
                            resource=RuleTestExecutionResult(
                                **{
                                    "execution_id": uuid.UUID(
                                        "33236e07-2ef1-4f3b-9497-7ece4d7ff587"
                                    ),
                                    "result": 1,
                                    "result_timestamp": "2021-11-11T11:11:11Z",
                                }
                            ),
                            status=400,
                        ),
                    ],
                    metadata=HttpResponse207Metadata(failure=1, success=1, total=2),
                ),
                message="some message",
            )
        )

        self.assertEqual(
            serializer.data,
            {
                "data": {
                    "data": [
                        {
                            "message": "some message success",
                            "resource": {
                                "execution_id": "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                                "result": 1,
                                "result_timestamp": "2021-11-11T11:11:11Z",
                            },
                            "status": 200,
                        },
                        {
                            "message": "some message fail",
                            "resource": {
                                "execution_id": "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                                "result": 1,
                                "result_timestamp": "2021-11-11T11:11:11Z",
                            },
                            "status": 400,
                        },
                    ],
                    "metadata": {"failure": 1, "success": 1, "total": 2},
                },
                "message": "some message",
            },
        )

    def test_UclResponseWrapperBodySerializer_create(self):  # pylint: disable=C0103:invalid-name
        """test UclResponseWrapperBodySerializer create not implemented"""

        with pytest.raises(NotImplementedError):
            UclResponseWrapperBodySerializer().create(validated_data={})

    def test_UclResponseWrapperBodySerializer_update(self):  # pylint: disable=C0103:invalid-name
        """test UclResponseWrapperBodySerializer update not implemented"""

        with pytest.raises(NotImplementedError):
            UclResponseWrapperBodySerializer().update(instance={}, validated_data={})
