""" Test Serializer for RuleTestExecution"""

import uuid

from django.test.testcases import TestCase

from freezegun import freeze_time


from ucl_test_engine.serializers.rule_test_execution import (
    RuleTestExecutionCreateValidationSerializer,
    RuleTestExecutionForSiemExecutionSerializer,
    RuleTestExecutionUclResponseSerializer,
)

from ucl_test_engine.tests.set_up import CreateUclTestEngineData


class SerializerTestsForRuleTestExecution(CreateUclTestEngineData, TestCase):
    """Test Serializers for RuleTestExecution"""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    @freeze_time("11-11-2021 11:11:11")
    def setUp(self) -> None:
        """add test data before each test"""
        self.init_user()
        self.init_test_execution_1_without_result()
        self.init_test_execution_2_with_result()

    def test_RuleTestExecutionCreateValidationSerializer_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall succeed, readonly and unknown fields are not in validated_data"""
        data = {
            "id": str(self.test_execution_1_without_result.id),
            "updated": "something",
            "execution_id": "cf136325-3333-42b0-a49c-b650e5014785",
            "rule_id": "1113ead1-f329-42b0-a49c-741961528514",
            "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
            "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            "something": 1,
        }
        serializer = RuleTestExecutionCreateValidationSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertIsInstance(serializer.validated_data, dict)
        self.assertEqual(
            serializer.validated_data,
            {
                "execution_id": uuid.UUID("cf136325-3333-42b0-a49c-b650e5014785"),
                "rule_id": uuid.UUID("1113ead1-f329-42b0-a49c-741961528514"),
                "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
                "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            },
        )

    def test_RuleTestExecutionCreateValidationSerializer_to_representation_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """
        to_representation shall succeed and return correct data
        """
        serializer = RuleTestExecutionCreateValidationSerializer(
            self.test_execution_1_without_result
        )

        self.assertEqual(
            serializer.data,
            {
                "id": str(self.test_execution_1_without_result.id),
                "execution_id": self.test_execution_1_without_result.execution_id,
                "rule_id": self.test_execution_1_without_result.rule_id,
                "response_url": self.test_execution_1_without_result.response_url,
                "test_events": self.test_execution_1_without_result.test_events,
                "created": "2021-11-11T11:11:11Z",
                "updated": None,
            },
        )

    def test_RuleTestExecutionForSiemExecutionSerializer_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall succeed, readonly and unknown fields are not in validated_data"""
        data = {
            "id": str(self.test_execution_1_without_result.id),
            "updated": "something",
            "execution_id": "cf136325-3333-42b0-a49c-b650e5014785",
            "rule_id": "1113ead1-f329-42b0-a49c-741961528514",
            "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
            "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            "something": 1,
        }
        serializer = RuleTestExecutionForSiemExecutionSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertIsInstance(serializer.validated_data, dict)
        self.assertEqual(
            serializer.validated_data,
            {},
        )

    def test_RuleTestExecutionForSiemExecutionSerializer_to_representation_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """
        to_representation shall succeed and return correct data
        """
        serializer = RuleTestExecutionForSiemExecutionSerializer(
            self.test_execution_1_without_result
        )
        self.assertEqual(
            serializer.data,
            {
                "execution_id": self.test_execution_1_without_result.execution_id,
                "rule_id": self.test_execution_1_without_result.rule_id,
                "test_events": self.test_execution_1_without_result.test_events,
            },
        )

    def test_RuleTestExecutionForSiemExecutionSerializer_to_representation_succeed_with_mock(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """
        to_representation shall succeed and return correct data.
        TODO Remove if real zeus is used. No mocking needed anymore.
        """
        self.test_execution_1_without_result.test_events[0]["delay"] = 10
        self.test_execution_1_without_result.test_events[0]["sre_to_create"] = 2
        serializer = RuleTestExecutionForSiemExecutionSerializer(
            self.test_execution_1_without_result
        )
        self.assertEqual(
            serializer.data,
            {
                "execution_id": self.test_execution_1_without_result.execution_id,
                "rule_id": self.test_execution_1_without_result.rule_id,
                "test_events": self.test_execution_1_without_result.test_events,
                "mock": {"sre_to_create": 2, "delay": 10},
            },
        )

    def test_RuleTestExecutionUclResponseSerializer_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall succeed, readonly and unknown fields are not in validated_data"""
        data = {
            "id": str(self.test_execution_1_without_result.id),
            "updated": "something",
            "execution_id": "cf136325-3333-42b0-a49c-b650e5014785",
            "rule_id": "1113ead1-f329-42b0-a49c-741961528514",
            "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
            "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            "something": 1,
        }
        serializer = RuleTestExecutionUclResponseSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertIsInstance(serializer.validated_data, dict)
        self.assertEqual(
            serializer.validated_data,
            {},
        )

    def test_RuleTestExecutionUclResponseSerializer_to_representation_succeed_with_result(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """
        to_representation shall succeed and return correct data
        """
        serializer = RuleTestExecutionUclResponseSerializer(self.test_execution_2_with_result)

        self.assertEqual(
            serializer.data,
            {
                "execution_id": self.test_execution_2_with_result.execution_id,
                "result": 1,
                "result_timestamp": "2023-11-11T11:11:11Z",
            },
        )

    def test_RuleTestExecutionUclResponseSerializer_to_representation_succeed_without_result(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """
        to_representation shall succeed and return correct data
        """
        serializer = RuleTestExecutionUclResponseSerializer(self.test_execution_1_without_result)

        self.assertEqual(
            serializer.data,
            {
                "execution_id": self.test_execution_1_without_result.execution_id,
                "result": None,
                "result_timestamp": None,
            },
        )
