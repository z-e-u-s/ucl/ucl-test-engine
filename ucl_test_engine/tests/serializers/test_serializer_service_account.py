""" Test Serializer """

# pylint: disable=R0801:duplicate-code
import pytest
from django.test.testcases import SimpleTestCase
from ucl_test_engine.serializers.service_account_token import ServiceAccountTokenSerializer

from ucl_test_engine.typing.service_account_token import (
    ServiceAccountToken,
)


class ServiceAccountTokenSerializerTest(SimpleTestCase):
    """Test Serializer ServiceAccountToken"""

    @classmethod
    def setUpClass(cls):
        super()

    def test_ServiceAccountTokenSerializer_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall succeed, readonly and unknown fields are not in validated_data"""

        data = {
            "access_token": "some great token",
            "expires_in": 600,
            "refresh_expires_in": 0,
            "token_type": "Bearer",
            "not-before-policy": 0,
            "scope": "someScope",
        }

        serializer = ServiceAccountTokenSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        self.assertEqual(
            serializer.validated_data,
            ServiceAccountToken(
                access_token="some great token",
                expires_in=600,
                refresh_expires_in=0,
                token_type="Bearer",
                scope="someScope",
            ),
        )

    def test_ServiceAccountTokenSerializer_fail_missing_fields(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """
        is_valid shall fail. Required fields are not set.
        """

        data = {}

        serializer = ServiceAccountTokenSerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(
            set(serializer.errors),
            {"access_token", "expires_in", "refresh_expires_in", "token_type", "scope"},
        )

    def test_ServiceAccountTokenSerializer_to_representation_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """to_representation shall succeed and return correct data"""
        serializer = ServiceAccountTokenSerializer(
            ServiceAccountToken(
                access_token="some great token",
                expires_in=600,
                refresh_expires_in=0,
                token_type="Bearer",
                scope="someScope",
            )
        )

        self.assertEqual(
            serializer.data,
            {
                "access_token": "some great token",
                "expires_in": 600,
                "refresh_expires_in": 0,
                "token_type": "Bearer",
                "scope": "someScope",
                "expires_at": None,
            },
        )

    def test_ServiceAccountTokenSerializer_create(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """test ServiceAccountTokenSerializer create not implemented"""

        with pytest.raises(NotImplementedError):
            ServiceAccountTokenSerializer().create(validated_data={})

    def test_ServiceAccountTokenSerializer_update(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """test ServiceAccountTokenSerializer update not implemented"""

        with pytest.raises(NotImplementedError):
            ServiceAccountTokenSerializer().update(instance={}, validated_data={})
