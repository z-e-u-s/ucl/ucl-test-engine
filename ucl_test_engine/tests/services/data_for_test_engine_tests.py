"""create test data for test_test_engine """

# pylint: disable=R0801:duplicate-code

from asgiref.sync import sync_to_async
from ucl_test_engine.models.test_engine_test_execution import TestEngineTestExecution


async def initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_1(
    test_service_test_engine,
    open_serach_pagination_limit: int,
):
    """create test data for test
    test_get_new_results_and_return_results_async_succeed_no_results_to_get_no_mocks
    """
    await sync_to_async(test_service_test_engine.init_test_execution_1_without_result)()
    test_service_test_engine.test_execution_1_without_result.created = "2021-11-11T05:11:11Z"
    await test_service_test_engine.test_execution_1_without_result.asave()

    response_data_event_1 = {
        "took": 51,
        "timed_out": False,
        "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
        "hits": {
            "total": {"value": 2, "relation": "eq"},
            "max_score": 1.0,
            "hits": [
                {
                    "_index": "event",
                    "_id": "ILlX_YkB9SJJheve1111",
                    "_score": 1.0,
                    "_source": {
                        "test_key1": "test_value1",
                        "execution_id": str(
                            test_service_test_engine.test_execution_1_without_result.id
                        ),
                        "pre_detection_id": "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                    },
                },
                {
                    "_index": "event",
                    "_id": "ILlX_YkB9SJJheve1122",
                    "_score": 1.0,
                    "_source": {
                        "test_key2": "test_value2",
                        "execution_id": str(
                            test_service_test_engine.test_execution_1_without_result.id
                        ),
                        "pre_detection_id": "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                    },
                },
            ],
        },
    }

    request_body_data_event_1 = {
        "query": {
            "term": {
                "execution_id": str(
                    test_service_test_engine.test_execution_1_without_result.execution_id
                )
            }
        },
        "from": 0,
        "size": open_serach_pagination_limit,
    }

    response_data_sre_1 = {
        "took": 51,
        "timed_out": False,
        "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
        "hits": {
            "total": {"value": 2, "relation": "eq"},
            "max_score": 1.0,
            "hits": [
                {
                    "_index": "sre",
                    "_id": "ILlX_YkB9SJJhsre2211",
                    "_score": 1.0,
                    "_source": {
                        "test_key1": "test_value1",
                        "execution_id": str(
                            test_service_test_engine.test_execution_1_without_result.id
                        ),
                        "pre_detection_id": "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                    },
                },
                {
                    "_index": "sre",
                    "_id": "ILlX_YkB9SJJhsre2222",
                    "_score": 1.0,
                    "_source": {
                        "test_key2": "test_value2",
                        "execution_id": str(
                            test_service_test_engine.test_execution_1_without_result.id
                        ),
                        "pre_detection_id": "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                    },
                },
            ],
        },
    }

    request_body_data_sre_1 = {
        "query": {
            "terms": {
                "pre_detection_id": [
                    "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                    "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                ]
            }
        }
    }

    return (
        response_data_event_1,
        request_body_data_event_1,
        response_data_sre_1,
        request_body_data_sre_1,
    )


async def initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_5(
    test_service_test_engine,
    open_serach_pagination_limit: int,
):
    """create test data for test
    test_get_new_results_and_return_results_async_succeed_no_results_to_get_no_mocks
    """
    await sync_to_async(test_service_test_engine.init_test_execution_5_without_result)()
    test_service_test_engine.test_execution_5_without_result.created = "2021-11-11T05:11:11Z"
    await test_service_test_engine.test_execution_5_without_result.asave()

    response_data_event_5 = {
        "took": 51,
        "timed_out": False,
        "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
        "hits": {
            "total": {"value": 2, "relation": "eq"},
            "max_score": 1.0,
            "hits": [
                {
                    "_index": "event",
                    "_id": "ILlX_YkB9SJJheve5511",
                    "_score": 1.0,
                    "_source": {
                        "test_key1": "test_value1",
                        "execution_id": str(
                            test_service_test_engine.test_execution_5_without_result.id
                        ),
                        "pre_detection_id": "b0a0cbd8-48dd-43fb-1111-555555555555",
                    },
                },
                {
                    "_index": "event",
                    "_id": "ILlX_YkB9SJJheve5522",
                    "_score": 1.0,
                    "_source": {
                        "test_key2": "test_value2",
                        "execution_id": str(
                            test_service_test_engine.test_execution_5_without_result.id
                        ),
                        "pre_detection_id": "33236e07-2ef1-4f3b-2222-555555555555",
                    },
                },
            ],
        },
    }
    request_body_data_event_5 = {
        "query": {
            "term": {
                "execution_id": str(
                    test_service_test_engine.test_execution_5_without_result.execution_id
                )
            }
        },
        "from": 0,
        "size": open_serach_pagination_limit,
    }

    response_data_sre_5 = {
        "took": 51,
        "timed_out": False,
        "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
        "hits": {
            "total": {"value": 2, "relation": "eq"},
            "max_score": 1.0,
            "hits": [
                {
                    "_index": "sre",
                    "_id": "ILlX_YkB9SJJhsre5511",
                    "_score": 1.0,
                    "_source": {
                        "test_key1": "test_value1",
                        "execution_id": str(
                            test_service_test_engine.test_execution_5_without_result.id
                        ),
                        "pre_detection_id": "b0a0cbd8-48dd-43fb-1111-555555555555",
                    },
                },
                {
                    "_index": "sre",
                    "_id": "ILlX_YkB9SJJhsre522",
                    "_score": 1.0,
                    "_source": {
                        "test_key2": "test_value2",
                        "execution_id": str(
                            test_service_test_engine.test_execution_5_without_result.id
                        ),
                        "pre_detection_id": "33236e07-2ef1-4f3b-2222-555555555555",
                    },
                },
            ],
        },
    }

    request_body_data_sre_5 = {
        "query": {
            "terms": {
                "pre_detection_id": [
                    "b0a0cbd8-48dd-43fb-1111-555555555555",
                    "33236e07-2ef1-4f3b-2222-555555555555",
                ]
            }
        }
    }

    return (
        response_data_event_5,
        request_body_data_event_5,
        response_data_sre_5,
        request_body_data_sre_5,
    )


async def initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_6(
    open_serach_pagination_limit: int,
):
    """create test data for test
    test_get_new_results_and_return_results_async_succeed_no_results_to_get_no_mocks
    """
    test_execution_raw_data_6 = {
        "execution_id": "cf136325-3333-42b0-a49c-666666666666",
        "rule_id": "1113ead1-f329-42b0-a49c-666666666666",
        "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
        "test_events": [{"firstKey": "firstValue", "fifthKey": 6}],
    }
    test_execution_6_without_result = await TestEngineTestExecution.objects.acreate(
        **test_execution_raw_data_6
    )
    test_execution_6_without_result.created = "2021-11-11T05:11:11Z"
    await test_execution_6_without_result.asave()

    response_data_event_6 = {
        "took": 51,
        "timed_out": False,
        "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
        "hits": {
            "total": {"value": 2, "relation": "eq"},
            "max_score": 1.0,
            "hits": [
                {
                    "_index": "event",
                    "_id": "ILlX_YkB9SJJheve6611",
                    "_score": 1.0,
                    "_source": {
                        "test_key1": "test_value1",
                        "execution_id": str(test_execution_6_without_result.id),
                        "pre_detection_id": "b0a0cbd8-48dd-43fb-1111-666666666666",
                    },
                },
            ],
        },
    }

    request_body_data_event_6 = {
        "query": {"term": {"execution_id": str(test_execution_6_without_result.execution_id)}},
        "from": 0,
        "size": open_serach_pagination_limit,
    }

    response_data_sre_6 = {
        "took": 51,
        "timed_out": False,
        "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
        "hits": {
            "total": {"value": 1, "relation": "eq"},
            "max_score": 1.0,
            "hits": [
                {
                    "_index": "sre",
                    "_id": "ILlX_YkB9SJJhsre6611",
                    "_score": 1.0,
                    "_source": {
                        "test_key1": "test_value1",
                        "execution_id": str(test_execution_6_without_result.id),
                        "pre_detection_id": "b0a0cbd8-48dd-43fb-1111-666666666666",
                    },
                },
            ],
        },
    }

    request_body_data_sre_6 = {
        "query": {
            "terms": {
                "pre_detection_id": [
                    "b0a0cbd8-48dd-43fb-1111-666666666666",
                ]
            }
        }
    }

    return (
        test_execution_6_without_result,
        response_data_event_6,
        request_body_data_event_6,
        response_data_sre_6,
        request_body_data_sre_6,
    )


async def initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_7(
    open_serach_pagination_limit: int,
):
    """create test data for test
    test_get_new_results_and_return_results_async_succeed_no_results_to_get_no_mocks
    """
    test_execution_raw_data_7 = {
        "execution_id": "cf136325-3333-42b0-a49c-777777777777",
        "rule_id": "1113ead1-f329-42b0-a49c-777777777777",
        "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
        "test_events": [{"firstKey": "firstValue", "fifthKey": 7}],
    }
    test_execution_7_without_result = await TestEngineTestExecution.objects.acreate(
        **test_execution_raw_data_7
    )
    test_execution_7_without_result.created = "2021-11-11T05:11:11Z"
    await test_execution_7_without_result.asave()

    response_data_event_7 = {
        "took": 51,
        "timed_out": False,
        "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
        "hits": {
            "total": {"value": 2, "relation": "eq"},
            "max_score": 1.0,
            "hits": [
                {
                    "_index": "event",
                    "_id": "ILlX_YkB9SJJheve7711",
                    "_score": 1.0,
                    "_source": {
                        "test_key1": "test_value1",
                        "execution_id": str(test_execution_7_without_result.id),
                        "pre_detection_id": "b0a0cbd8-48dd-43fb-1111-777777777777",
                    },
                },
            ],
        },
    }

    request_body_data_event_7 = {
        "query": {"term": {"execution_id": str(test_execution_7_without_result.execution_id)}},
        "from": 0,
        "size": open_serach_pagination_limit,
    }

    response_data_sre_7 = {
        "took": 51,
        "timed_out": False,
        "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
        "hits": {
            "total": {"value": 1, "relation": "eq"},
            "max_score": 1.0,
            "hits": [
                {
                    "_index": "sre",
                    "_id": "ILlX_YkB9SJJhsre7711",
                    "_score": 1.0,
                    "_source": {
                        "test_key1": "test_value1",
                        "execution_id": str(test_execution_7_without_result.id),
                        "pre_detection_id": "b0a0cbd8-48dd-43fb-1111-777777777777",
                    },
                },
            ],
        },
    }
    request_body_data_sre_7 = {
        "query": {
            "terms": {
                "pre_detection_id": [
                    "b0a0cbd8-48dd-43fb-1111-777777777777",
                ]
            }
        }
    }

    return (
        test_execution_7_without_result,
        response_data_event_7,
        request_body_data_event_7,
        response_data_sre_7,
        request_body_data_sre_7,
    )


async def initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_8(
    open_serach_pagination_limit: int,
):
    """create test data for test
    test_get_new_results_and_return_results_async_succeed_no_results_to_get_no_mocks
    """
    test_execution_raw_data_8 = {
        "execution_id": "cf136325-3333-42b0-a49c-888888888888",
        "rule_id": "1113ead1-f329-42b0-a49c-888888888888",
        "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
        "test_events": [{"firstKey": "firstValue", "fifthKey": 8}],
    }

    test_execution_8_without_result = await TestEngineTestExecution.objects.acreate(
        **test_execution_raw_data_8
    )
    test_execution_8_without_result.created = "2021-11-11T05:11:11Z"
    await test_execution_8_without_result.asave()
    response_data_event_8 = {
        "took": 51,
        "timed_out": False,
        "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
        "hits": {
            "total": {"value": 2, "relation": "eq"},
            "max_score": 1.0,
            "hits": [
                {
                    "_index": "event",
                    "_id": "ILlX_YkB9SJJheve8811",
                    "_score": 1.0,
                    "_source": {
                        "test_key1": "test_value1",
                        "execution_id": str(test_execution_8_without_result.id),
                        "pre_detection_id": "b0a0cbd8-48dd-43fb-1111-888888888888",
                    },
                },
            ],
        },
    }
    request_body_data_event_8 = {
        "query": {"term": {"execution_id": str(test_execution_8_without_result.execution_id)}},
        "from": 0,
        "size": open_serach_pagination_limit,
    }

    response_data_sre_8 = {
        "took": 51,
        "timed_out": False,
        "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
        "hits": {
            "total": {"value": 1, "relation": "eq"},
            "max_score": 1.0,
            "hits": [
                {
                    "_index": "sre",
                    "_id": "ILlX_YkB9SJJhsre8811",
                    "_score": 1.0,
                    "_source": {
                        "test_key1": "test_value1",
                        "execution_id": str(test_execution_8_without_result.id),
                        "pre_detection_id": "b0a0cbd8-48dd-43fb-1111-888888888888",
                    },
                },
            ],
        },
    }

    request_body_data_sre_8 = {
        "query": {
            "terms": {
                "pre_detection_id": [
                    "b0a0cbd8-48dd-43fb-1111-888888888888",
                ]
            }
        }
    }

    return (
        test_execution_8_without_result,
        response_data_event_8,
        request_body_data_event_8,
        response_data_sre_8,
        request_body_data_sre_8,
    )
