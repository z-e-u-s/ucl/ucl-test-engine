"""service module for error handling logic"""

# pylint: disable=R0801:duplicate-code

from django.test import SimpleTestCase
from rest_framework.exceptions import (
    APIException,
    ValidationError,
    NotAuthenticated,
    PermissionDenied,
    MethodNotAllowed,
)
from rest_framework.views import Http404

from ucl_test_engine.services.error_handling import (
    UclTestEngineTestEngineErrorSerializer,
    UclTestEngineTestEngineError,
)

from ucl_test_engine.services import error_handling


class ErrorHandlingTest(SimpleTestCase):
    """service for error handling logic"""

    def test_create_field_description(self):
        # Test with a string path
        path = "my_field"
        expected_output = "myField"
        self.assertEqual(error_handling.create_field_description(path), expected_output)

        # Test with a list path
        path = ["my_list", 0, "my_field"]
        expected_output = "myList 1. Item: myField"
        self.assertEqual(error_handling.create_field_description(path), expected_output)

        # Test with a nested list path
        path = ["my_list", 0, "my_dict", "my_field"]
        expected_output = "myList 1. Item: myDict myField"
        self.assertEqual(error_handling.create_field_description(path), expected_output)

    def test_normalize_api_exception(self):
        exc = APIException(detail={"foo": ["This field is required."]})
        result = error_handling.normalize_exception(exc)
        expected = [
            UclTestEngineTestEngineError(
                type="APIException",
                code="error",
                description="This field is required.",
                path="foo",
                field_description="foo",
            ),
        ]
        self.assertEqual(
            UclTestEngineTestEngineErrorSerializer(result[0]).data,
            UclTestEngineTestEngineErrorSerializer(expected[0]).data,
        )

    def test_normalize_404(self):
        exc = Http404()
        result = error_handling.normalize_exception(exc)
        expected = [
            UclTestEngineTestEngineError(
                type="Http404",
                description="Resource or one of related resources not found",
                code="not_found",
            )
        ]
        self.assertEqual(result, expected)

    def test_normalize_validation_error(self):
        exc = ValidationError("Validation error occurred")
        result = error_handling.normalize_exception(exc)
        expected = [
            UclTestEngineTestEngineError(
                type="ValidationError",
                description="Validation error occurred",
                code="invalid",
            )
        ]
        self.assertEqual(result, expected)

    def test_normalize_not_authenticated(self):
        exc = NotAuthenticated()
        result = error_handling.normalize_exception(exc)
        expected = [
            UclTestEngineTestEngineError(
                type="NotAuthenticated",
                description="Authentication credentials were not provided.",
                code="not_authenticated",
            )
        ]
        self.assertEqual(result, expected)

    def test_normalize_permission_denied(self):
        exc = PermissionDenied()
        result = error_handling.normalize_exception(exc)
        expected = [
            UclTestEngineTestEngineError(
                type="PermissionDenied",
                description="You do not have permission to perform this action.",
                code="permission_denied",
            )
        ]
        self.assertEqual(result, expected)

    def test_normalize_method_not_allowed(self):
        exc = MethodNotAllowed("GET", "http://example.com")
        result = error_handling.normalize_exception(exc)

        expected = [
            UclTestEngineTestEngineError(
                type="MethodNotAllowed",
                description="http://example.com",
                code="method_not_allowed",
            )
        ]
        self.assertEqual(result, expected)

    def test_normalize_multiple_errors(self):
        class CustomException(APIException):
            """test_normalize_multiple_errors"""

        exc = CustomException(
            detail={
                "field_1": ["required field"],
                "field_2": ["invalid field"],
            }
        )
        result = error_handling.normalize_exception(exc)

        expected = [
            UclTestEngineTestEngineError(
                type="CustomException",
                description="required field",
                code="error",
                path="field1",
                field_description="field1",
            ),
            UclTestEngineTestEngineError(
                type="CustomException",
                description="invalid field",
                code="error",
                path="field2",
                field_description="field2",
            ),
        ]
        self.assertEqual(result, expected)

    def test_normalize_unknown_error(self):
        exc = ValueError("Some error occurred")
        result = error_handling.normalize_exception(exc)

        expected = [
            UclTestEngineTestEngineError(
                type="ValueError",
                description="Unknown validation error",
                code="unknown",
            )
        ]
        self.assertEqual(result, expected)

    def test_inject_errors_to_listing(self):
        result = {
            "paths": {
                "/foo": {
                    "get": {
                        "responses": {
                            "200": {
                                "description": "Success",
                            },
                        },
                    },
                },
            },
            "components": {"schemas": {}},
        }
        generator = None
        request = None
        public = False
        expected_route = {
            "401": {
                "content": {
                    "application/json": {
                        "schema": {"$ref": "#/components/schemas/ErrorResponseObject4xx"}
                    },
                },
                "description": "",
            },
        }
        expected_error_schema = {
            "type": "object",
            "description": "Error response for 4xx",
            "properties": {
                "data": {"type": "array", "default": []},
                "message": {"type": "object", "default": {}},
                "errors": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "code": {"type": "string"},
                            "description": {"type": "string"},
                            "helpText": {"type": "string"},
                            "path": {"type": "string"},
                            "fieldDescription": {"type": "string"},
                        },
                        "default": {},
                    },
                },
            },
        }

        result = error_handling.inject_errors_to_listing(result, generator, request, public)
        self.assertEqual(result["paths"]["/foo"]["get"]["responses"]["401"], expected_route["401"])
        self.assertEqual(
            result["components"]["schemas"]["ErrorResponseObject4xx"], expected_error_schema
        )
