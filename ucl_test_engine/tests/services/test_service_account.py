""" Test Service ServiceAccount """

# pylint: disable=W0212:protected-access

from unittest.mock import patch

# from unittest import mock
# import os
import datetime
import requests
import pytest
import responses
from freezegun import freeze_time

from rest_framework import status
from rest_framework.exceptions import ValidationError
from django.test.testcases import SimpleTestCase
from django.conf import settings

from django.test import override_settings
from ucl_test_engine.typing.service_account_token import (
    ServiceAccountToken,
)


from ucl_test_engine.services.service_account import KeycloakServiceAccountService
from ucl_test_engine.exceptions.external_api import ServiceUnavailable


class TestServiceAccountService(SimpleTestCase):
    """Test Service ServiceAccount"""

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    @patch(
        "ucl_test_engine.services.service_account.KeycloakServiceAccountService."
        + "_KeycloakServiceAccountService__request_service_account_access_token",
        return_value=None,
    )
    @freeze_time("11-11-2021 11:11:11")
    def test_get_service_account_access_token_succeed_from_external(
        self, mock__request_service_account_access_token
    ):
        """test correct token is returned from external"""
        sat: ServiceAccountToken = ServiceAccountToken(
            access_token="some great new token",
            expires_in=600,
            refresh_expires_in=0,
            token_type="Bearer",
            scope="someScope",
            expires_at=datetime.datetime.strptime(
                "11-11-2021 11:11:21+00:00", "%d-%m-%Y %H:%M:%S%z"
            ),
        )
        KeycloakServiceAccountService._KeycloakServiceAccountService__set_service_account_access_token(  # pylint: disable=C0301:line-too-long
            sat
        )

        access_token = KeycloakServiceAccountService.get_service_account_access_token()

        self.assertEqual(access_token, "some great new token")

        self.assertTrue(mock__request_service_account_access_token.called)

    @patch(
        "ucl_test_engine.services.service_account.KeycloakServiceAccountService."
        + "_KeycloakServiceAccountService__request_service_account_access_token",
        return_value=None,
    )
    @freeze_time("11-11-2021 11:11:11")
    def test_get_service_account_access_token_succeed_from_cache(
        self, mock__request_service_account_access_token
    ):
        """test correct token is returned from cache"""
        sat: ServiceAccountToken = ServiceAccountToken(
            access_token="some great cached token",
            expires_in=600,
            refresh_expires_in=0,
            token_type="Bearer",
            scope="someScope",
            expires_at=datetime.datetime.strptime(
                "11-11-2021 11:13:11+00:00", "%d-%m-%Y %H:%M:%S%z"
            ),
        )
        KeycloakServiceAccountService._KeycloakServiceAccountService__set_service_account_access_token(  # pylint: disable=C0301:line-too-long
            sat
        )
        access_token = KeycloakServiceAccountService.get_service_account_access_token()

        self.assertEqual(access_token, "some great cached token")
        self.assertFalse(mock__request_service_account_access_token.called)

    @override_settings(DISABLE_AUTHENTICATION=True)
    def test_get_service_account_access_token_succeed_auth_disabled(self):
        """test none is returned if auth is disabled via environment variable"""
        access_token = KeycloakServiceAccountService.get_service_account_access_token()
        self.assertIsNone(access_token)

    @responses.activate
    @freeze_time("11-11-2021 11:11:11")
    def test__request_service_account_access_token_succeed_from_external(self):
        """test correct token is returned from external"""

        response_data = {
            "access_token": "some great token from external",
            "expires_in": 600,
            "refresh_expires_in": 0,
            "token_type": "Bearer",
            "not-before-policy": 0,
            "scope": "someScope",
        }

        responses.add(
            responses.POST,
            f"{settings.KEYCLOAK_CONFIG['SERVER_URL']}/realms/{settings.KEYCLOAK_CONFIG['REALM']}"
            + "/protocol/openid-connect/token",
            status=200,
            json=response_data,
        )

        sat: ServiceAccountToken = ServiceAccountToken(
            access_token="some great cached token",
            expires_in=600,
            refresh_expires_in=0,
            token_type="Bearer",
            scope="someScope",
            expires_at=datetime.datetime.strptime(
                "11-11-2021 11:11:21+00:00", "%d-%m-%Y %H:%M:%S%z"
            ),
        )
        KeycloakServiceAccountService._KeycloakServiceAccountService__set_service_account_access_token(  # pylint: disable=C0301:line-too-long
            sat
        )
        access_token = KeycloakServiceAccountService.get_service_account_access_token()

        self.assertEqual(access_token, "some great token from external")
        self.assertEqual(len(responses.calls), 1)
        self.assertEqual(
            responses.calls[0].request.body,
            f"grant_type=client_credentials&client_id={settings.KEYCLOAK_CONFIG['CLIENT_ID']}&"
            + f"client_secret={settings.KEYCLOAK_CONFIG['CLIENT_SECRET']}",
        )

    @responses.activate
    @freeze_time("11-11-2021 11:11:11")
    def test__request_service_account_access_token_succeed_return_cached_token(self):
        """test correct token is returned from cache"""

        responses.add(
            responses.POST,
            f"{settings.KEYCLOAK_CONFIG['SERVER_URL']}/realms/{settings.KEYCLOAK_CONFIG['REALM']}"
            + "/protocol/openid-connect/token",
            status=200,
        )

        sat: ServiceAccountToken = ServiceAccountToken(
            access_token="some great cached token",
            expires_in=600,
            refresh_expires_in=0,
            token_type="Bearer",
            scope="someScope",
            expires_at=datetime.datetime.strptime(
                "11-11-2021 11:13:11+00:00", "%d-%m-%Y %H:%M:%S%z"
            ),
        )
        KeycloakServiceAccountService._KeycloakServiceAccountService__set_service_account_access_token(  # pylint: disable=C0301:line-too-long
            sat
        )
        KeycloakServiceAccountService._KeycloakServiceAccountService__request_service_account_access_token()  # pylint: disable=C0301:line-too-long

        self.assertEqual(len(responses.calls), 0)

    @responses.activate
    @freeze_time("11-11-2021 11:11:11")
    def test__request_service_account_access_token_fails_from_external_on_400(self):
        """test failure external api returns 400"""

        responses.add(
            responses.POST,
            f"{settings.KEYCLOAK_CONFIG['SERVER_URL']}/realms/{settings.KEYCLOAK_CONFIG['REALM']}"
            + "/protocol/openid-connect/token",
            status=400,
        )

        sat: ServiceAccountToken = ServiceAccountToken(
            access_token="some great cached token",
            expires_in=600,
            refresh_expires_in=0,
            token_type="Bearer",
            scope="someScope",
            expires_at=datetime.datetime.strptime(
                "11-11-2021 11:11:21+00:00", "%d-%m-%Y %H:%M:%S%z"
            ),
        )
        KeycloakServiceAccountService._KeycloakServiceAccountService__set_service_account_access_token(  # pylint: disable=C0301:line-too-long
            sat
        )
        with pytest.raises(ServiceUnavailable) as exc:
            KeycloakServiceAccountService.get_service_account_access_token()

        self.assertEqual(
            exc.value.detail,
            "Service cannot get service account token.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "get_service_token_failed",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
        self.assertEqual(len(responses.calls), 1)

    @responses.activate
    @freeze_time("11-11-2021 11:11:11")
    def test__request_service_account_access_token_fails_from_external_on_invalid_token_schema(
        self,
    ):
        """test failure external api returns invalid token schema"""

        response_data = {
            "access_token": "some great token from external",
            "refresh_expires_in": 0,
            "token_type": "Bearer",
            "not-before-policy": 0,
            "scope": "someScope",
        }

        responses.add(
            responses.POST,
            f"{settings.KEYCLOAK_CONFIG['SERVER_URL']}/realms/{settings.KEYCLOAK_CONFIG['REALM']}"
            + "/protocol/openid-connect/token",
            status=200,
            json=response_data,
        )

        sat: ServiceAccountToken = ServiceAccountToken(
            access_token="some great cached token",
            expires_in=600,
            refresh_expires_in=0,
            token_type="Bearer",
            scope="someScope",
            expires_at=datetime.datetime.strptime(
                "11-11-2021 11:11:21+00:00", "%d-%m-%Y %H:%M:%S%z"
            ),
        )
        KeycloakServiceAccountService._KeycloakServiceAccountService__set_service_account_access_token(  # pylint: disable=C0301:line-too-long
            sat
        )

        with pytest.raises(ValidationError) as exc:
            KeycloakServiceAccountService.get_service_account_access_token()

        self.assertEqual(
            "{'expires_in': [ErrorDetail(string='This field is required.', code='required')]}",
            str(exc.value),
        )
        self.assertEqual(len(responses.calls), 1)

    @responses.activate
    @freeze_time("11-11-2021 11:11:11")
    def test__request_service_account_access_token_fails_from_external_on_connection_error(self):
        """test failure on connection error"""

        rsp = responses.Response(
            responses.POST,
            f"{settings.KEYCLOAK_CONFIG['SERVER_URL']}/realms/{settings.KEYCLOAK_CONFIG['REALM']}"
            + "/protocol/openid-connect/token",
            status=400,
        )

        my_error = requests.ConnectionError("custom error")
        my_error.response = rsp

        sat: ServiceAccountToken = ServiceAccountToken(
            access_token="some great cached token",
            expires_in=600,
            refresh_expires_in=0,
            token_type="Bearer",
            scope="someScope",
            expires_at=datetime.datetime.strptime(
                "11-11-2021 11:11:21+00:00", "%d-%m-%Y %H:%M:%S%z"
            ),
        )
        KeycloakServiceAccountService._KeycloakServiceAccountService__set_service_account_access_token(  # pylint: disable=C0301:line-too-long
            sat
        )
        with pytest.raises(ServiceUnavailable) as exc:
            KeycloakServiceAccountService.get_service_account_access_token()

        self.assertEqual(
            exc.value.detail,
            "Service cannot get service account token.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "get_service_token_failed",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
        self.assertEqual(len(responses.calls), 1)
