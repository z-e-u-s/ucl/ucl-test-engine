""" Test TestEngine service"""

# pylint: disable=C0302:too-many-lines
# pylint: disable=W0212:protected-access
# pylint: disable=R0801:duplicate-code

import logging
from unittest.mock import ANY, AsyncMock, call, patch

import pytest
import requests
from aiohttp import ClientSession, ClientTimeout
from aioresponses import CallbackResult, aioresponses
from asgiref.sync import async_to_sync, sync_to_async
from django.conf import settings
from django.test.testcases import TestCase
from freezegun import freeze_time
from rest_framework import status
from rest_framework.exceptions import AuthenticationFailed, NotFound

from ucl_test_engine.exceptions.external_api import ServiceUnavailable
from ucl_test_engine.models.test_engine_test_execution import TestEngineTestExecution
from ucl_test_engine.services.test_engine import TestEngineService
from ucl_test_engine.services.zeus_mock.zeus_connection_service import (
    ZeusConnectionService,
)
from ucl_test_engine.tests.services.data_for_test_engine_tests import (
    initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_1,
    initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_5,
    initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_6,
    initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_7,
    initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_8,
)
from ucl_test_engine.tests.set_up import CreateUclTestEngineData
from ucl_test_engine.typing.http_response_207_body import (
    HttpResponse207BodyData,
    HttpResponse207DataElement,
    HttpResponse207Metadata,
)
from ucl_test_engine.typing.test_execution_dataclass import RuleTestExecutionResult


class TestTestEngineService(
    CreateUclTestEngineData, TestCase
):  # pylint: disable=R0904:too-many-public-methods
    """Test Service TestEngine"""

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def setUp(self) -> None:
        """clear force_authenticate before each test"""
        self.init_user()
        self.test_engine_test_executions_input = [
            {
                "execution_id": "cf136325-3333-42b0-a49c-000000000000",
                "rule_id": "1113ead1-f329-42b0-a49c-741961528514",
                "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
                "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            },
            {
                "execution_id": "cf136325-3333-42b0-a49c-222222222222",
                "rule_id": "1113ead1-f329-42b0-a49c-222222222222",
                "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
                "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            },
        ]

    @freeze_time("11-11-2021 11:11:11")
    def test__abort_test_execution_succeed(self):
        """
        check that when aborting a test_execution result is set to -1 and updated is set correctly.
        """
        self.init_test_execution_1_without_result()
        self.init_test_execution_5_without_result()

        res: None = TestEngineService._TestEngineService__abort_test_execution(
            self.test_execution_1_without_result,
        )

        self.assertIsNone(res)
        self.assertEqual(
            TestEngineTestExecution.objects.get(id=self.test_execution_1_without_result.id).result,
            -1,
        )
        self.assertEqual(
            TestEngineTestExecution.objects.get(id=self.test_execution_1_without_result.id)
            .updated.isoformat(sep="T")
            .replace("+00:00", "Z"),
            "2021-11-11T11:11:11Z",
        )
        self.assertIsNone(
            TestEngineTestExecution.objects.get(id=self.test_execution_5_without_result.id).result
        )
        self.assertIsNone(
            TestEngineTestExecution.objects.get(id=self.test_execution_5_without_result.id).updated
        )

    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "execute_tests",
        return_value=None,
    )
    def test_test_rule_test_executions_succeed(self, mock_execute_tests):
        """test sending rule test execution object to zeus mock correctly"""
        self.init_test_execution_1_without_result()
        self.init_test_execution_5_without_result()

        res: None = TestEngineService.test_rule_test_executions(
            [self.test_execution_5_without_result]
        )

        self.assertIsNone(res)
        mock_execute_tests.assert_called_once()
        mock_execute_tests.assert_called_with([self.test_execution_5_without_result])

    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "execute_tests",
        side_effect=ServiceUnavailable(
            detail="Zeus mock service temporarily unavailable try again later.",
            code="zeus_mock_service_unavailable",
        ),
    )
    @freeze_time("11-11-2021 11:11:11")
    def test_test_rule_test_executions_fail_service_unavailable(self, mock_execute_tests):
        """
        test test_rule_test_executions aborts rule_test_execution correctly
         on ServiceUnavailable Exception
        """
        self.init_test_execution_1_without_result()
        self.init_test_execution_5_without_result()

        with pytest.raises(ServiceUnavailable):
            TestEngineService.test_rule_test_executions([self.test_execution_5_without_result])

        test_engine_test_executions_queryset = TestEngineTestExecution.objects.all()
        new_test_engine_test_executions: list[TestEngineTestExecution] = list(
            test_engine_test_executions_queryset.filter(id=self.test_execution_5_without_result.id)
        )

        self.assertEqual(test_engine_test_executions_queryset.count(), 2)
        self.assertIsInstance(new_test_engine_test_executions, list)
        self.assertEqual(len(new_test_engine_test_executions), 1)
        self.assertIsInstance(new_test_engine_test_executions[0], TestEngineTestExecution)
        self.assertEqual(new_test_engine_test_executions[0].result, -1)
        self.assertEqual(
            new_test_engine_test_executions[0].updated.isoformat(sep="T").replace("+00:00", "Z"),
            "2021-11-11T11:11:11Z",
        )
        mock_execute_tests.assert_called_once()

        old_test_engine_test_execution: TestEngineTestExecution = (
            test_engine_test_executions_queryset.get(id=self.test_execution_1_without_result.id)
        )
        self.assertIsNone(old_test_engine_test_execution.result)
        self.assertIsNone(old_test_engine_test_execution.updated)

    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "execute_tests",
        side_effect=NotFound(
            detail="Zeus mock service not found",
            code="zeus_mock_service_not_found",
        ),
    )
    @freeze_time("11-11-2021 11:11:11")
    def test_test_rule_test_executions_fail_not_found(self, mock_execute_tests):
        """
        test test_rule_test_executions aborts rule_test_execution correctly
         on NotFound Exception
        """
        self.init_test_execution_1_without_result()
        self.init_test_execution_5_without_result()

        with pytest.raises(NotFound):
            TestEngineService.test_rule_test_executions([self.test_execution_5_without_result])

        test_engine_test_executions_queryset = TestEngineTestExecution.objects.all()
        new_test_engine_test_executions: list[TestEngineTestExecution] = list(
            test_engine_test_executions_queryset.filter(id=self.test_execution_5_without_result.id)
        )

        self.assertEqual(test_engine_test_executions_queryset.count(), 2)
        self.assertIsInstance(new_test_engine_test_executions, list)
        self.assertEqual(len(new_test_engine_test_executions), 1)
        self.assertIsInstance(new_test_engine_test_executions[0], TestEngineTestExecution)
        self.assertEqual(new_test_engine_test_executions[0].result, -1)
        self.assertEqual(
            new_test_engine_test_executions[0].updated.isoformat(sep="T").replace("+00:00", "Z"),
            "2021-11-11T11:11:11Z",
        )
        mock_execute_tests.assert_called_once()

        old_test_engine_test_execution: TestEngineTestExecution = (
            test_engine_test_executions_queryset.get(id=self.test_execution_1_without_result.id)
        )
        self.assertIsNone(old_test_engine_test_execution.result)
        self.assertIsNone(old_test_engine_test_execution.updated)

    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "execute_tests",
        side_effect=AuthenticationFailed(
            detail="Zeus mock service denied access.",
            code="zeus_mock_service_authentication_failed",
        ),
    )
    @freeze_time("11-11-2021 11:11:11")
    def test_test_rule_test_executions_fail_authentication_failed(self, mock_execute_tests):
        """
        test test_rule_test_executions aborts rule_test_execution correctly
         on AuthenticationFailed Exception
        """
        self.init_test_execution_1_without_result()
        self.init_test_execution_5_without_result()

        with pytest.raises(AuthenticationFailed):
            TestEngineService.test_rule_test_executions([self.test_execution_5_without_result])

        test_engine_test_executions_queryset = TestEngineTestExecution.objects.all()
        new_test_engine_test_executions: list[TestEngineTestExecution] = list(
            test_engine_test_executions_queryset.filter(id=self.test_execution_5_without_result.id)
        )

        self.assertEqual(test_engine_test_executions_queryset.count(), 2)
        self.assertIsInstance(new_test_engine_test_executions, list)
        self.assertEqual(len(new_test_engine_test_executions), 1)
        self.assertIsInstance(new_test_engine_test_executions[0], TestEngineTestExecution)
        self.assertEqual(new_test_engine_test_executions[0].result, -1)
        self.assertEqual(
            new_test_engine_test_executions[0].updated.isoformat(sep="T").replace("+00:00", "Z"),
            "2021-11-11T11:11:11Z",
        )
        mock_execute_tests.assert_called_once()

        old_test_engine_test_execution: TestEngineTestExecution = (
            test_engine_test_executions_queryset.get(id=self.test_execution_1_without_result.id)
        )
        self.assertIsNone(old_test_engine_test_execution.result)
        self.assertIsNone(old_test_engine_test_execution.updated)

    @patch(
        "ucl_test_engine.services.service_account.KeycloakServiceAccountService."
        + "get_service_account_access_token",
        return_value="wonderful token",
    )
    @aioresponses()
    @pytest.mark.asyncio
    async def test__send_result_of_one_url_succeed(
        self, mock_get_service_account_access_token, mock_responses
    ):
        """test sending rule test execution object to zeus mock correctly"""
        await sync_to_async(self.init_test_execution_2_with_result)()

        response_data = {
            "data": {
                "data": [
                    {
                        "message": "some message success",
                        "resource": {
                            "execution_id": str(self.test_execution_2_with_result.execution_id),
                            "result": self.test_execution_2_with_result.result,
                            "result_timestamp": self.test_execution_2_with_result.updated,
                        },
                        "status": 200,
                    },
                ],
                "metadata": {"failure": 0, "success": 1, "total": 1},
            },
            "message": "some message",
        }

        mock_responses.patch(
            self.test_execution_2_with_result.response_url, status=207, payload=response_data
        )

        session = ClientSession()
        res: HttpResponse207BodyData = (
            await TestEngineService._TestEngineService__send_result_of_one_url(
                [self.test_execution_2_with_result],
                self.test_execution_2_with_result.response_url,
                session,
            )
        )
        await session.close()

        mock_responses.assert_called_once()
        mock_responses.assert_called_once_with(
            self.test_execution_2_with_result.response_url,
            method="PATCH",
            data=None,
            json=[
                {
                    "execution_id": str(self.test_execution_2_with_result.execution_id),
                    "result": self.test_execution_2_with_result.result,
                    "result_timestamp": self.test_execution_2_with_result.updated,
                }
            ],
            timeout=ClientTimeout(total=5, connect=None, sock_read=None, sock_connect=None),
            headers={"Authorization": "Bearer wonderful token"},
        )

        self.assertIsNotNone(res)
        self.assertIsInstance(res, HttpResponse207BodyData)
        self.assertTrue(mock_get_service_account_access_token.called)

    @patch(
        "ucl_test_engine.services.service_account.KeycloakServiceAccountService."
        + "get_service_account_access_token",
        return_value="wonderful token",
    )
    @pytest.mark.asyncio
    @aioresponses()
    async def test_execute_tests_fails_on_400(
        self, mock_get_service_account_access_token, mock_responses
    ):
        """test failure external api returns 400"""
        await sync_to_async(self.init_test_execution_2_with_result)()

        response_data = {}

        mock_responses.patch(
            self.test_execution_2_with_result.response_url, status=400, payload=response_data
        )

        session = ClientSession()
        with pytest.raises(ServiceUnavailable) as exc:
            await TestEngineService._TestEngineService__send_result_of_one_url(
                [self.test_execution_2_with_result],
                self.test_execution_2_with_result.response_url,
                session,
            )
        await session.close()

        self.assertEqual(
            exc.value.detail,
            "Response service temporarily unavailable try again later.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "response_service_unavailable",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
        self.assertTrue(mock_get_service_account_access_token.called)

    @patch(
        "ucl_test_engine.services.service_account.KeycloakServiceAccountService."
        + "get_service_account_access_token",
        return_value="wonderful token",
    )
    @pytest.mark.asyncio
    @aioresponses()
    async def test_execute_tests_fails_on_connection_error(
        self, mock_get_service_account_access_token, mock_responses
    ):
        """test failure on connection error"""
        await sync_to_async(self.init_test_execution_2_with_result)()

        mock_responses.patch(self.test_execution_2_with_result.response_url, status=400)

        my_error = requests.ConnectionError("custom error")
        my_error.response = mock_responses

        session = ClientSession()
        with pytest.raises(ServiceUnavailable) as exc:
            await TestEngineService._TestEngineService__send_result_of_one_url(
                [self.test_execution_2_with_result],
                self.test_execution_2_with_result.response_url,
                session,
            )
        await session.close()

        self.assertEqual(
            exc.value.detail,
            "Response service temporarily unavailable try again later.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "response_service_unavailable",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
        self.assertTrue(mock_get_service_account_access_token.called)

    @patch(
        "ucl_test_engine.services.service_account.KeycloakServiceAccountService."
        + "get_service_account_access_token",
        return_value="wonderful token",
    )
    @pytest.mark.asyncio
    @aioresponses()
    async def test_execute_tests_fails_on_404(
        self, mock_get_service_account_access_token, mock_responses
    ):
        """test failure external api returns 404"""
        await sync_to_async(self.init_test_execution_2_with_result)()

        response_data = {}

        mock_responses.patch(
            self.test_execution_2_with_result.response_url, status=404, payload=response_data
        )

        session = ClientSession()
        with pytest.raises(NotFound) as exc:
            await TestEngineService._TestEngineService__send_result_of_one_url(
                [self.test_execution_2_with_result],
                self.test_execution_2_with_result.response_url,
                session,
            )
        await session.close()

        self.assertEqual(
            exc.value.detail,
            "Response service not found.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "response_service_not_found",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_404_NOT_FOUND)
        self.assertTrue(mock_get_service_account_access_token.called)

    @patch(
        "ucl_test_engine.services.service_account.KeycloakServiceAccountService."
        + "get_service_account_access_token",
        return_value="wonderful token",
    )
    @pytest.mark.asyncio
    @aioresponses()
    async def test_execute_tests_fails_on_401(
        self, mock_get_service_account_access_token, mock_responses
    ):
        """test failure external api returns 401"""
        await sync_to_async(self.init_test_execution_2_with_result)()

        response_data = {}

        mock_responses.patch(
            self.test_execution_2_with_result.response_url, status=401, payload=response_data
        )

        session = ClientSession()
        with pytest.raises(AuthenticationFailed) as exc:
            await TestEngineService._TestEngineService__send_result_of_one_url(
                [self.test_execution_2_with_result],
                self.test_execution_2_with_result.response_url,
                session,
            )
        await session.close()
        self.assertEqual(
            exc.value.detail,
            "Response service denied access.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "response_service_authentication_failed",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_401_UNAUTHORIZED)
        mock_get_service_account_access_token.assert_called_once()

    @patch(
        "ucl_test_engine.services.service_account.KeycloakServiceAccountService."
        + "get_service_account_access_token",
        side_effect=ServiceUnavailable(
            detail="Service cannot get service account token.",
            code="get_service_token_failed",
        ),
    )
    @aioresponses()
    @pytest.mark.asyncio
    async def test_execute_tests_fails_on_get_service_account_error(
        self, mock_get_service_account_access_token, mock_responses
    ):
        """test failure on getting service account token error"""
        await sync_to_async(self.init_test_execution_2_with_result)()

        mock_responses.patch(self.test_execution_2_with_result.response_url, status=400)

        my_error = requests.ConnectionError("custom error")
        my_error.response = mock_responses

        session = ClientSession()
        with pytest.raises(ServiceUnavailable) as exc:
            await TestEngineService._TestEngineService__send_result_of_one_url(
                [self.test_execution_2_with_result],
                self.test_execution_2_with_result.response_url,
                session,
            )
        await session.close()

        self.assertEqual(
            exc.value.detail,
            "Response service temporarily unavailable try again later.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "response_service_unavailable",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
        mock_get_service_account_access_token.assert_called_once()

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__send_result_of_one_url",
        return_value=HttpResponse207BodyData(
            **{
                "data": [
                    HttpResponse207DataElement(
                        **{
                            "message": "some message success",
                            "resource": RuleTestExecutionResult(
                                **{
                                    "execution_id": "cf136325-3333-42b0-a49c-222222222222",
                                    "result": 1,
                                    "result_timestamp": "2021-11-11T11:11:11Z",
                                }
                            ),
                            "status": 200,
                        }
                    ),
                    HttpResponse207DataElement(
                        **{
                            "message": "some message fail",
                            "resource": RuleTestExecutionResult(
                                **{
                                    "execution_id": "cf136325-3333-42b0-a49c-333333333333",
                                    "result": 1,
                                    "result_timestamp": "2021-11-11T11:11:11Z",
                                }
                            ),
                            "status": 400,
                        }
                    ),
                ],
                "metadata": HttpResponse207Metadata(**{"failure": 1, "success": 1, "total": 2}),
            }
        ),
        new_callable=AsyncMock,
    )
    @patch.object(logging.getLogger("ucl_test_engine.services.test_engine"), "warning")
    @pytest.mark.asyncio
    async def test__return_results_of_one_url_succeed(
        self, mock_logger, mock__send_result_of_one_url
    ):
        """
        test __return_results_of_one_url.
        Correct string is logged.
        Correct data is deleted from db.
        """
        await sync_to_async(self.init_test_execution_2_with_result)()
        await sync_to_async(self.init_test_execution_1_without_result)()
        await sync_to_async(self.init_test_execution_3_with_result)()

        self.assertEqual(await TestEngineTestExecution.objects.all().acount(), 3)

        session = ClientSession()
        result: None = await TestEngineService._TestEngineService__return_results_of_one_url(
            [self.test_execution_2_with_result, self.test_execution_3_with_result],
            self.test_execution_2_with_result.response_url,
            session,
        )
        await session.close()

        self.assertIsNone(result)
        self.assertEqual(await TestEngineTestExecution.objects.all().acount(), 1)

        mock__send_result_of_one_url.assert_called_once()
        mock__send_result_of_one_url.assert_called_with(
            [self.test_execution_2_with_result, self.test_execution_3_with_result],
            self.test_execution_2_with_result.response_url,
            session,
        )
        mock_logger.assert_called_once()
        mock_logger.assert_called_with(
            "Send and update result for execution with id: '%s' failed.",
            "cf136325-3333-42b0-a49c-333333333333",
        )

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__return_results_of_one_url",
        return_value=None,
        new_callable=AsyncMock,
    )
    @pytest.mark.asyncio
    async def test__return_results_succeed(self, mock__return_results_of_one_url):
        """
        test __return_results.
        Data is grouped correctly by response_url.
        Method '__return_results_of_one_url' is called for each unique url.
        """
        await sync_to_async(self.init_test_execution_2_with_result)()
        await sync_to_async(self.init_test_execution_3_with_result)()
        await sync_to_async(self.init_test_execution_4_with_result_other_url)()

        session = ClientSession()
        result: None = await TestEngineService._TestEngineService__return_results(session)

        await session.close()
        self.assertIsNone(result)

        self.assertEqual(mock__return_results_of_one_url.call_count, 2)
        calls = [
            call(
                [self.test_execution_2_with_result, self.test_execution_3_with_result],
                self.test_execution_2_with_result.response_url,
                session,
            ),
            call(
                [self.test_execution_4_with_result_and_other_url],
                self.test_execution_4_with_result_and_other_url.response_url,
                session,
            ),
        ]

        mock__return_results_of_one_url.assert_has_calls(calls, any_order=True)

    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "get_rule_test_result",
        return_value=1,
    )
    @freeze_time("11-11-2021 11:11:11")
    @pytest.mark.asyncio
    async def test__get_results_and_save_in_db_succeed_with_result(
        self,
        mock_get_rule_test_result,
    ):
        """
        test __get_results_and_save_in_db.
        Check Execution result is updated.
        """
        await sync_to_async(self.init_test_execution_1_without_result)()

        session = ClientSession()
        result: None = await TestEngineService._TestEngineService__get_results_and_save_in_db(
            self.test_execution_1_without_result, session
        )

        await session.close()
        self.assertIsNone(result)

        mock_get_rule_test_result.assert_called_once()
        mock_get_rule_test_result.assert_called_with(
            self.test_execution_1_without_result.execution_id, session
        )
        execution: TestEngineTestExecution = await TestEngineTestExecution.objects.aget(
            id=self.test_execution_1_without_result.id
        )

        self.assertEqual(execution.result, 1)
        self.assertEqual(
            execution.updated.isoformat(sep="T").replace("+00:00", "Z"), "2021-11-11T11:11:11Z"
        )

    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "get_rule_test_result",
        return_value=None,
    )
    @pytest.mark.asyncio
    async def test__get_results_and_save_in_db_succeed_without_result(
        self,
        mock_get_rule_test_result,
    ):
        """
        test __get_results_and_save_in_db.
        Check Execution result is not updated.
        """
        await sync_to_async(self.init_test_execution_1_without_result)()

        session = ClientSession()
        result: None = await TestEngineService._TestEngineService__get_results_and_save_in_db(
            self.test_execution_1_without_result, session
        )
        await session.close()

        self.assertIsNone(result)

        mock_get_rule_test_result.assert_called_once()
        mock_get_rule_test_result.assert_called_with(
            self.test_execution_1_without_result.execution_id, session
        )
        execution: TestEngineTestExecution = await TestEngineTestExecution.objects.aget(
            id=self.test_execution_1_without_result.id
        )
        self.assertIsNone(execution.result)
        self.assertIsNone(execution.updated)

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__get_results_and_save_in_db",
        return_value=None,
        new_callable=AsyncMock,
    )
    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__return_results",
        return_value=None,
    )
    @patch("ucl_test_engine.services.test_engine.ClientSession")
    @freeze_time("11-11-2021 11:11:11")
    @pytest.mark.asyncio
    async def test_get_new_results_and_return_results_succeed_only_in_time_executions(
        self,
        mock__client_session,
        mock__return_results,
        mock__get_results_and_save_in_db,
    ):
        """
        test get_new_results_and_return_results.
        '__get_results_and_save_in_db' method is called the correct times.
        All executions are still in the allowed waiting time.
        """
        session = ClientSession()
        mock__client_session.return_value = session

        await sync_to_async(self.init_test_execution_1_without_result)()
        self.test_execution_1_without_result.created = "2021-11-11T05:11:11Z"
        await self.test_execution_1_without_result.asave()
        await sync_to_async(self.init_test_execution_2_with_result)()
        self.test_execution_2_with_result.created = "2021-11-11T05:11:11Z"
        await self.test_execution_2_with_result.asave()
        await sync_to_async(self.init_test_execution_5_without_result)()
        self.test_execution_5_without_result.created = "2021-11-11T05:11:11Z"
        await self.test_execution_5_without_result.asave()

        result: None = await TestEngineService.get_new_results_and_return_results()

        self.assertIsNone(result)

        self.assertEqual(mock__get_results_and_save_in_db.call_count, 2)

        calls = [
            call(self.test_execution_1_without_result, session),
            call(self.test_execution_5_without_result, session),
        ]

        mock__get_results_and_save_in_db.assert_has_calls(calls, any_order=True)
        mock__return_results.assert_called_once()
        await session.close()

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__get_results_and_save_in_db",
        return_value=None,
        new_callable=AsyncMock,
    )
    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__return_results",
        return_value=None,
    )
    @patch("ucl_test_engine.services.test_engine.ClientSession")
    @freeze_time("11-11-2021 11:11:11")
    @pytest.mark.asyncio
    async def test_get_new_results_and_return_results_succeed_one_outdated_execution(
        self,
        mock__client_session,
        mock__return_results,
        mock__get_results_and_save_in_db,
    ):
        """
        test get_new_results_and_return_results.
        '__get_results_and_save_in_db' method is called the correct times.
        One execution reached the allowed waiting time and will be aborted.
        """
        session = ClientSession()
        mock__client_session.return_value = session

        await sync_to_async(self.init_test_execution_1_without_result)()
        self.test_execution_1_without_result.created = "2021-11-11T05:11:11Z"
        await self.test_execution_1_without_result.asave()
        await sync_to_async(self.init_test_execution_2_with_result)()
        self.test_execution_2_with_result.created = "2021-11-11T05:11:11Z"
        await self.test_execution_2_with_result.asave()
        await sync_to_async(self.init_test_execution_5_without_result)()
        self.test_execution_5_without_result.created = "2021-11-10T10:11:11Z"
        await self.test_execution_5_without_result.asave()

        self.assertEqual(await TestEngineTestExecution.objects.filter(result=-1).acount(), 0)

        result: None = await TestEngineService.get_new_results_and_return_results()

        self.assertIsNone(result)
        self.assertEqual(mock__get_results_and_save_in_db.call_count, 1)

        calls = [
            call(self.test_execution_1_without_result, session),
        ]

        mock__get_results_and_save_in_db.assert_has_calls(calls, any_order=True)
        mock__return_results.assert_called_once()

        self.assertEqual(await TestEngineTestExecution.objects.filter(result=-1).acount(), 1)
        execution_5 = await TestEngineTestExecution.objects.aget(
            id=self.test_execution_5_without_result.id
        )
        self.assertEqual(execution_5.result, -1)
        await session.close()

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__get_results_and_save_in_db",
        return_value=None,
        new_callable=AsyncMock,
    )
    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__return_results",
        return_value=None,
    )
    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__abort_test_execution",
        return_value=None,
    )
    @freeze_time("11-11-2021 11:11:11")
    @pytest.mark.asyncio
    async def test_get_new_results_and_return_results_succeed_no_results_to_get(
        self, mock__abort_test_execution, mock__return_results, mock__get_results_and_save_in_db
    ):
        """
        test get_new_results_and_return_results.
        '__get_results_and_save_in_db' method is called the correct times.
        There is no execution waiting for a result to be fetched.
        """
        await sync_to_async(self.init_test_execution_2_with_result)()
        self.test_execution_2_with_result.created = "2021-11-11T05:11:11Z"
        await self.test_execution_2_with_result.asave()

        result: None = await TestEngineService.get_new_results_and_return_results()

        self.assertIsNone(result)
        self.assertFalse(mock__get_results_and_save_in_db.called)
        self.assertFalse(mock__abort_test_execution.called)

        mock__return_results.assert_called_once()

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__get_results_and_save_in_db",
        return_value=None,
        new_callable=AsyncMock,
    )
    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__return_results",
        return_value=None,
    )
    @freeze_time("11-11-2021 11:11:11")
    def test_get_new_results_and_return_results_sync_succeed_only_in_time_executions(
        self,
        mock__return_results,
        mock__get_results_and_save_in_db,
    ):
        """
        test get_new_results_and_return_results_sync.
        '__get_results_and_save_in_db' method is called the correct times.
        All executions are still in the allowed waiting time.
        """
        self.init_test_execution_1_without_result()
        self.test_execution_1_without_result.created = "2021-11-11T05:11:11Z"
        self.test_execution_1_without_result.save()
        self.init_test_execution_2_with_result()
        self.test_execution_2_with_result.created = "2021-11-11T05:11:11Z"
        self.test_execution_2_with_result.save()
        self.init_test_execution_5_without_result()
        self.test_execution_5_without_result.created = "2021-11-11T05:11:11Z"
        self.test_execution_5_without_result.save()

        result: None = TestEngineService.get_new_results_and_return_results_sync()

        self.assertIsNone(result)

        self.assertEqual(mock__get_results_and_save_in_db.call_count, 2)

        mock__return_results.assert_called_once()

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__get_results_and_save_in_db",
        return_value=None,
        new_callable=AsyncMock,
    )
    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__return_results",
        return_value=None,
    )
    @freeze_time("11-11-2021 11:11:11")
    def test_get_new_results_and_return_results_sync_succeed_one_outdated_execution(
        self,
        mock__return_results,
        mock__get_results_and_save_in_db,
    ):
        """
        test get_new_results_and_return_results_sync.
        '__get_results_and_save_in_db' method is called the correct times.
        One execution reached the allowed waiting time and will be aborted.
        """
        self.init_test_execution_1_without_result()
        self.test_execution_1_without_result.created = "2021-11-11T05:11:11Z"
        self.test_execution_1_without_result.save()
        self.init_test_execution_2_with_result()
        self.test_execution_2_with_result.created = "2021-11-11T05:11:11Z"
        self.test_execution_2_with_result.save()
        self.init_test_execution_5_without_result()
        self.test_execution_5_without_result.created = "2021-11-10T10:11:11Z"
        self.test_execution_5_without_result.save()

        self.assertEqual(TestEngineTestExecution.objects.filter(result=-1).count(), 0)

        result: None = TestEngineService.get_new_results_and_return_results_sync()

        self.assertIsNone(result)

        self.assertEqual(mock__get_results_and_save_in_db.call_count, 1)

        mock__return_results.assert_called_once()

        self.assertEqual(TestEngineTestExecution.objects.filter(result=-1).count(), 1)
        self.assertEqual(
            TestEngineTestExecution.objects.filter(result=-1)[0].id,
            self.test_execution_5_without_result.id,
        )

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__get_results_and_save_in_db",
        return_value=None,
        new_callable=AsyncMock,
    )
    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__return_results",
        return_value=None,
    )
    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__abort_test_execution",
        return_value=None,
    )
    @freeze_time("11-11-2021 11:11:11")
    def test_get_new_results_and_return_results_sync_succeed_no_results_to_get(
        self, mock__abort_test_execution, mock__return_results, mock__get_results_and_save_in_db
    ):
        """
        test get_new_results_and_return_results_sync.
        '__get_results_and_save_in_db' method is called the correct times.
        There is no execution waiting for a result to be fetched.
        """
        self.init_test_execution_2_with_result()
        self.test_execution_2_with_result.created = "2021-11-11T05:11:11Z"
        self.test_execution_2_with_result.save()

        result: None = TestEngineService.get_new_results_and_return_results_sync()

        self.assertIsNone(result)

        self.assertFalse(mock__get_results_and_save_in_db.called)
        self.assertFalse(mock__abort_test_execution.called)

        mock__return_results.assert_called_once()

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__return_results_of_one_url",
        return_value=None,
        new_callable=AsyncMock,
        # TODO fix RuntimeWarning: coroutine 'AsyncMockMixin._execute_mock_call' was never awaited
    )
    @aioresponses()
    @pytest.mark.asyncio
    @freeze_time("11-11-2021 11:11:11")
    async def test_get_new_results_and_return_results_async_succeed_no_results_to_get_no_mocks(  # pylint: disable=R0904:too-many-locals
        self, mock__return_results_of_one_url, mock_responses
    ):
        """
        test get_new_results_and_return_results_sync.
        '__get_results_and_save_in_db' method is called the correct times.
        There is no execution waiting for a result to be fetched.
        """

        open_serach_pagination_limit = (
            ZeusConnectionService._ZeusConnectionService__get_open_serach_pagination_limit()
        )

        (
            response_data_event_1,
            request_body_data_event_1,
            response_data_sre_1,
            request_body_data_sre_1,
        ) = await initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_1(  # pylint:disable=C0301:line-too-long
            self, open_serach_pagination_limit
        )

        (
            response_data_event_5,
            request_body_data_event_5,
            response_data_sre_5,
            request_body_data_sre_5,
        ) = await initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_5(  # pylint:disable=C0301:line-too-long
            self, open_serach_pagination_limit
        )

        (
            test_execution_6_without_result,
            response_data_event_6,
            request_body_data_event_6,
            response_data_sre_6,
            request_body_data_sre_6,
        ) = await initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_6(  # pylint:disable=C0301:line-too-long
            open_serach_pagination_limit
        )

        (
            test_execution_7_without_result,
            response_data_event_7,
            request_body_data_event_7,
            response_data_sre_7,
            request_body_data_sre_7,
        ) = await initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_7(  # pylint:disable=C0301:line-too-long
            open_serach_pagination_limit
        )

        (
            test_execution_8_without_result,
            response_data_event_8,
            request_body_data_event_8,
            response_data_sre_8,
            request_body_data_sre_8,
        ) = await initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_8(  # pylint:disable=C0301:line-too-long
            open_serach_pagination_limit
        )

        def match_body_event(url, **kwargs):  # pylint:disable=W0613:unused-argument
            if kwargs["json"] == request_body_data_event_1:
                return CallbackResult(status=200, payload=response_data_event_1)
            if kwargs["json"] == request_body_data_event_5:
                return CallbackResult(status=200, payload=response_data_event_5)
            if kwargs["json"] == request_body_data_event_6:
                return CallbackResult(status=200, payload=response_data_event_6)
            if kwargs["json"] == request_body_data_event_7:
                return CallbackResult(status=200, payload=response_data_event_7)
            if kwargs["json"] == request_body_data_event_8:
                return CallbackResult(status=200, payload=response_data_event_8)

            raise Exception(  # pylint:disable=W0719:broad-exception-raised
                "Unable to match the request"
            )

        def match_body_sre(url, **kwargs):  # pylint:disable=W0613:unused-argument
            if kwargs["json"] == request_body_data_sre_1:
                return CallbackResult(status=200, payload=response_data_sre_1)
            if kwargs["json"] == request_body_data_sre_5:
                return CallbackResult(status=200, payload=response_data_sre_5)
            if kwargs["json"] == request_body_data_sre_6:
                return CallbackResult(status=200, payload=response_data_sre_6)
            if kwargs["json"] == request_body_data_sre_7:
                return CallbackResult(status=200, payload=response_data_sre_7)
            if kwargs["json"] == request_body_data_sre_8:
                return CallbackResult(status=200, payload=response_data_sre_8)

            raise Exception(  # pylint:disable=W0719:broad-exception-raised
                "Unable to match the request"
            )

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/event/_search",
            status=200,
            repeat=True,
            callback=match_body_event,
        )

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/sre/_search",
            status=200,
            repeat=True,
            callback=match_body_sre,
        )

        result: None = await TestEngineService.get_new_results_and_return_results()

        self.assertIsNone(result)

        mock__return_results_of_one_url.assert_called_once()
        # TODO fix RuntimeWarning: coroutine 'AsyncMockMixin._execute_mock_call' was never awaited

        mock__return_results_of_one_url.assert_called_once_with(
            [
                self.test_execution_1_without_result,
                self.test_execution_5_without_result,
                test_execution_6_without_result,
                test_execution_7_without_result,
                test_execution_8_without_result,
            ],
            self.test_execution_1_without_result.response_url,
            ANY,  # Ignores aiohttp.client.ClientSession object
        )
        # TODO fix RuntimeWarning: coroutine 'AsyncMockMixin._execute_mock_call' was never awaited

        self.assertEqual(
            (
                await TestEngineTestExecution.objects.aget(
                    id=self.test_execution_1_without_result.id
                )
            ).result,
            2,
        )

        self.assertEqual(
            (
                await TestEngineTestExecution.objects.aget(
                    id=self.test_execution_5_without_result.id
                )
            ).result,
            2,
        )

        self.assertEqual(
            (
                await TestEngineTestExecution.objects.aget(id=test_execution_6_without_result.id)
            ).result,
            1,
        )

        self.assertEqual(
            (
                await TestEngineTestExecution.objects.aget(id=test_execution_7_without_result.id)
            ).result,
            1,
        )

        self.assertEqual(
            (
                await TestEngineTestExecution.objects.aget(id=test_execution_8_without_result.id)
            ).result,
            1,
        )

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService."
        + "_TestEngineService__return_results_of_one_url",
        return_value=None,
        new_callable=AsyncMock,
        # TODO fix RuntimeWarning: coroutine 'AsyncMockMixin._execute_mock_call' was never awaited
    )
    @aioresponses()
    @freeze_time("11-11-2021 11:11:11")
    def test_get_new_results_and_return_results_sync_succeed_no_results_to_get_no_mocks(  # pylint: disable=R0904:too-many-locals
        self, mock__return_results_of_one_url, mock_responses
    ):
        """
        test get_new_results_and_return_results_sync.
        '__get_results_and_save_in_db' method is called the correct times.
        There is no execution waiting for a result to be fetched.
        """

        open_serach_pagination_limit = (
            ZeusConnectionService._ZeusConnectionService__get_open_serach_pagination_limit()
        )

        (
            response_data_event_1,
            request_body_data_event_1,
            response_data_sre_1,
            request_body_data_sre_1,
        ) = async_to_sync(
            initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_1  # pylint:disable=C0301:line-too-long
        )(
            self, open_serach_pagination_limit
        )

        (
            response_data_event_5,
            request_body_data_event_5,
            response_data_sre_5,
            request_body_data_sre_5,
        ) = async_to_sync(
            initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_5  # pylint:disable=C0301:line-too-long
        )(
            self, open_serach_pagination_limit
        )

        (
            test_execution_6_without_result,
            response_data_event_6,
            request_body_data_event_6,
            response_data_sre_6,
            request_body_data_sre_6,
        ) = async_to_sync(
            initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_6  # pylint:disable=C0301:line-too-long
        )(
            open_serach_pagination_limit
        )

        (
            test_execution_7_without_result,
            response_data_event_7,
            request_body_data_event_7,
            response_data_sre_7,
            request_body_data_sre_7,
        ) = async_to_sync(
            initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_7  # pylint:disable=C0301:line-too-long
        )(
            open_serach_pagination_limit
        )

        (
            test_execution_8_without_result,
            response_data_event_8,
            request_body_data_event_8,
            response_data_sre_8,
            request_body_data_sre_8,
        ) = async_to_sync(
            initialize_and_get_test_data_for_get_new_results_and_return_results__test_execution_8  # pylint:disable=C0301:line-too-long
        )(
            open_serach_pagination_limit
        )

        def match_body_event(url, **kwargs):  # pylint:disable=W0613:unused-argument
            if kwargs["json"] == request_body_data_event_1:
                return CallbackResult(status=200, payload=response_data_event_1)
            if kwargs["json"] == request_body_data_event_5:
                return CallbackResult(status=200, payload=response_data_event_5)
            if kwargs["json"] == request_body_data_event_6:
                return CallbackResult(status=200, payload=response_data_event_6)
            if kwargs["json"] == request_body_data_event_7:
                return CallbackResult(status=200, payload=response_data_event_7)
            if kwargs["json"] == request_body_data_event_8:
                return CallbackResult(status=200, payload=response_data_event_8)

            raise Exception(  # pylint:disable=W0719:broad-exception-raised
                "Unable to match the request"
            )

        def match_body_sre(url, **kwargs):  # pylint:disable=W0613:unused-argument
            if kwargs["json"] == request_body_data_sre_1:
                return CallbackResult(status=200, payload=response_data_sre_1)
            if kwargs["json"] == request_body_data_sre_5:
                return CallbackResult(status=200, payload=response_data_sre_5)
            if kwargs["json"] == request_body_data_sre_6:
                return CallbackResult(status=200, payload=response_data_sre_6)
            if kwargs["json"] == request_body_data_sre_7:
                return CallbackResult(status=200, payload=response_data_sre_7)
            if kwargs["json"] == request_body_data_sre_8:
                return CallbackResult(status=200, payload=response_data_sre_8)

            raise Exception(  # pylint:disable=W0719:broad-exception-raised
                "Unable to match the request"
            )

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/event/_search",
            status=200,
            repeat=True,
            callback=match_body_event,
        )

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/sre/_search",
            status=200,
            repeat=True,
            callback=match_body_sre,
        )

        result: None = TestEngineService.get_new_results_and_return_results_sync()

        self.assertIsNone(result)

        # TODO fix RuntimeWarning: coroutine 'AsyncMockMixin._execute_mock_call' was never awaited
        mock__return_results_of_one_url.assert_called_once_with(
            [
                self.test_execution_1_without_result,
                self.test_execution_5_without_result,
                test_execution_6_without_result,
                test_execution_7_without_result,
                test_execution_8_without_result,
            ],
            self.test_execution_1_without_result.response_url,
            ANY,  # Ignores aiohttp.client.ClientSession object
        )

        self.assertEqual(
            TestEngineTestExecution.objects.get(id=self.test_execution_1_without_result.id).result,
            2,
        )

        self.assertEqual(
            TestEngineTestExecution.objects.get(id=self.test_execution_5_without_result.id).result,
            2,
        )

        self.assertEqual(
            TestEngineTestExecution.objects.get(id=test_execution_6_without_result.id).result,
            1,
        )

        self.assertEqual(
            TestEngineTestExecution.objects.get(id=test_execution_7_without_result.id).result,
            1,
        )

        self.assertEqual(
            TestEngineTestExecution.objects.get(id=test_execution_8_without_result.id).result,
            1,
        )
