""" Test opensearch dataclasses"""

# pylint: disable=R0801:duplicate-code
from collections import OrderedDict
import pytest

from django.test.testcases import TestCase

from ucl_test_engine.tests.set_up import CreateUclTestEngineData

from ucl_test_engine.services.zeus_mock.rule_test_execution_serializer import (
    OpenSearchGetIndexSearchResponseSerializer,
)
from ucl_test_engine.services.zeus_mock.test_execution_dataclass import (
    OpenSearchGetIndexSearchResponse,
    OpenSearchGetIndexSearchResponseHits,
    OpenSearchGetIndexSearchResponseHitsHitsArrayDocument,
    OpenSearchGetIndexSearchResponseHitsHitsArrayDocumentSource,
    OpenSearchGetIndexSearchResponseHitsTotal,
    OpenSearchGetIndexSearchResponseShards,
)


class TestOpenSearchGetIndexSearchResponse(CreateUclTestEngineData, TestCase):
    """Test Service OpenSearchGetIndexSearchResponse"""

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def setUp(self) -> None:
        """clear force_authenticate before each test"""
        self.init_user()

    def test_OpenSearchGetIndexSearchResponseSerializer_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """
        is_valid shall succeed.
        Readonly and unknown fields are not in validated_data
        """

        data = {
            "took": 51,
            "timed_out": False,
            "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
            "hits": {
                "total": {"value": 2, "relation": "eq"},
                "max_score": 1.0,
                "hits": [
                    {
                        "_index": "event",
                        "_id": "ILlX_YkB9SJJhbxijBqL",
                        "_score": 1.0,
                        "_source": {
                            "test_key1": "test_value1",
                            "execution_id": "b0a0cbd8-48dd-43fb-b5d0-111111111111",
                            "pre_detection_id": "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                        },
                    },
                    {
                        "_index": "event",
                        "_id": "I7lX_YkB9SJJhbxijRpR",
                        "_score": 1.0,
                        "_source": {
                            "test_key2": "test_value2",
                            "execution_id": "b0a0cbd8-48dd-43fb-b5d0-222222222222",
                            "pre_detection_id": "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                        },
                    },
                ],
            },
            "something": 1,
        }

        serializer = OpenSearchGetIndexSearchResponseSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        self.assertEqual(
            serializer.validated_data,
            OpenSearchGetIndexSearchResponse(
                took=51,
                timed_out=False,
                _shards=OpenSearchGetIndexSearchResponseShards(
                    total=1, successful=1, skipped=0, failed=0
                ),
                hits=OpenSearchGetIndexSearchResponseHits(
                    total=OpenSearchGetIndexSearchResponseHitsTotal(value=2, relation="eq"),
                    max_score=1.0,
                    hits=[
                        OpenSearchGetIndexSearchResponseHitsHitsArrayDocument(
                            _index="event",
                            _id="ILlX_YkB9SJJhbxijBqL",
                            _score=1.0,
                            _source=OpenSearchGetIndexSearchResponseHitsHitsArrayDocumentSource(
                                execution_id="b0a0cbd8-48dd-43fb-b5d0-111111111111",
                                pre_detection_id="b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                            ),
                        ),
                        OpenSearchGetIndexSearchResponseHitsHitsArrayDocument(
                            _index="event",
                            _id="I7lX_YkB9SJJhbxijRpR",
                            _score=1.0,
                            _source=OpenSearchGetIndexSearchResponseHitsHitsArrayDocumentSource(
                                execution_id="b0a0cbd8-48dd-43fb-b5d0-222222222222",
                                pre_detection_id="33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                            ),
                        ),
                    ],
                ),
            ),
        )

    def test_OpenSearchGetIndexSearchResponseSerializer_fail_missing_fields(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall fail, missing 'took', 'timed_out', '_shards' and 'hits'"""

        data = {}

        serializer = OpenSearchGetIndexSearchResponseSerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(set(serializer.errors), {"took", "timed_out", "_shards", "hits"})

    def test_OpenSearchGetIndexSearchResponseSerializer_fail_missing_fields_in_nested_data_1(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall fail.
        missing fields in '_shards' and 'hits' attribute"""

        data = {
            "took": 51,
            "timed_out": False,
            "_shards": {},
            "hits": {},
        }

        serializer = OpenSearchGetIndexSearchResponseSerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(
            set(serializer.errors["_shards"]), {"total", "successful", "skipped", "failed"}
        )

        self.assertEqual(set(serializer.errors["hits"]), {"total", "max_score", "hits"})

    def test_OpenSearchGetIndexSearchResponseSerializer_fail_missing_fields_in_nested_data_2(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall fail.
        missing fields in '_shards' and 'hits' attribute"""

        data = {
            "took": 51,
            "timed_out": False,
            "_shards": {},
            "hits": {"max_score": None},
        }

        serializer = OpenSearchGetIndexSearchResponseSerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(
            set(serializer.errors["_shards"]), {"total", "successful", "skipped", "failed"}
        )

        self.assertEqual(set(serializer.errors["hits"]), {"total", "hits"})

    def test_OpenSearchGetIndexSearchResponseSerializer_fail_missing_fields_in_nested_data_3(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall fail, missing fields in 'hits/hits/0' attribute"""

        data = {
            "took": 51,
            "timed_out": False,
            "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
            "hits": {
                "total": {"value": 2, "relation": "eq"},
                "max_score": 1.0,
                "hits": [{}],
            },
        }

        serializer = OpenSearchGetIndexSearchResponseSerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(
            set(serializer.errors["hits"]["hits"][0]), {"_index", "_id", "_score", "_source"}
        )

    def test_OpenSearchGetIndexSearchResponseSerializer_fail_missing_fields_in_nested_data_4(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """is_valid shall fail, missing fields in 'hits/hits/0/_source' attribute"""

        data = {
            "took": 51,
            "timed_out": False,
            "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
            "hits": {
                "total": {"value": 2, "relation": "eq"},
                "max_score": 1.0,
                "hits": [
                    {
                        "_index": "event",
                        "_id": "ILlX_YkB9SJJhbxijBqL",
                        "_score": 1.0,
                        "_source": {},
                    }
                ],
            },
        }

        serializer = OpenSearchGetIndexSearchResponseSerializer(data=data)
        self.assertFalse(serializer.is_valid())

        self.assertEqual(
            set(serializer.errors["hits"]["hits"][0]["_source"]),
            {"pre_detection_id"},
        )

    def test_OpenSearchGetIndexSearchResponseSerializer_to_representation_succeed(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """to_representation shall succeed and return correct data"""
        serializer = OpenSearchGetIndexSearchResponseSerializer(
            OpenSearchGetIndexSearchResponse(
                took=51,
                timed_out=False,
                _shards=OpenSearchGetIndexSearchResponseShards(
                    total=1, successful=1, skipped=0, failed=0
                ),
                hits=OpenSearchGetIndexSearchResponseHits(
                    total=OpenSearchGetIndexSearchResponseHitsTotal(value=2, relation="eq"),
                    max_score=1.0,
                    hits=[
                        OpenSearchGetIndexSearchResponseHitsHitsArrayDocument(
                            _index="event",
                            _id="ILlX_YkB9SJJhbxijBqL",
                            _score=1.0,
                            _source=OpenSearchGetIndexSearchResponseHitsHitsArrayDocumentSource(
                                execution_id="b0a0cbd8-48dd-43fb-b5d0-111111111111",
                                pre_detection_id="b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                            ),
                        ),
                        OpenSearchGetIndexSearchResponseHitsHitsArrayDocument(
                            _index="event",
                            _id="I7lX_YkB9SJJhbxijRpR",
                            _score=1.0,
                            _source=OpenSearchGetIndexSearchResponseHitsHitsArrayDocumentSource(
                                execution_id="b0a0cbd8-48dd-43fb-b5d0-222222222222",
                                pre_detection_id="33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                            ),
                        ),
                    ],
                ),
            )
        )

        self.assertEqual(
            serializer.data,
            {
                "took": 51,
                "timed_out": False,
                "_shards": OrderedDict(
                    [("total", 1), ("successful", 1), ("skipped", 0), ("failed", 0)]
                ),
                "hits": OrderedDict(
                    [
                        ("total", OrderedDict([("value", 2), ("relation", "eq")])),
                        ("max_score", 1.0),
                        (
                            "hits",
                            [
                                OrderedDict(
                                    [
                                        ("_index", "event"),
                                        ("_id", "ILlX_YkB9SJJhbxijBqL"),
                                        ("_score", 1.0),
                                        (
                                            "_source",
                                            OrderedDict(
                                                [
                                                    (
                                                        "pre_detection_id",
                                                        "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                                                    ),
                                                    (
                                                        "execution_id",
                                                        "b0a0cbd8-48dd-43fb-b5d0-111111111111",
                                                    ),
                                                ]
                                            ),
                                        ),
                                    ]
                                ),
                                OrderedDict(
                                    [
                                        ("_index", "event"),
                                        ("_id", "I7lX_YkB9SJJhbxijRpR"),
                                        ("_score", 1.0),
                                        (
                                            "_source",
                                            OrderedDict(
                                                [
                                                    (
                                                        "pre_detection_id",
                                                        "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                                                    ),
                                                    (
                                                        "execution_id",
                                                        "b0a0cbd8-48dd-43fb-b5d0-222222222222",
                                                    ),
                                                ]
                                            ),
                                        ),
                                    ]
                                ),
                            ],
                        ),
                    ]
                ),
            },
        )

    def test_OpenSearchGetIndexSearchResponseSerializer_create(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """test OpenSearchGetIndexSearchResponseSerializer create not implemented"""

        with pytest.raises(NotImplementedError):
            OpenSearchGetIndexSearchResponseSerializer().create(validated_data={})

    def test_OpenSearchGetIndexSearchResponseSerializer_update(
        self,
    ):  # pylint: disable=C0103:invalid-name
        """test OpenSearchGetIndexSearchResponseSerializer update not implemented"""

        with pytest.raises(NotImplementedError):
            OpenSearchGetIndexSearchResponseSerializer().update(instance={}, validated_data={})
