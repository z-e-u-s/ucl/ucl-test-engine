""" Test ZeusConnectionService service"""

# pylint: disable=W0212:protected-access
# pylint: disable=R0801:duplicate-code
import json
from unittest.mock import AsyncMock, call, patch

import pytest
import requests
import responses
from aiohttp import BasicAuth, ClientSession, ClientTimeout
from aioresponses import aioresponses
from asgiref.sync import sync_to_async
from django.conf import settings
from django.test import override_settings
from django.test.testcases import TestCase
from rest_framework import status
from rest_framework.exceptions import AuthenticationFailed, NotFound

from ucl_test_engine.exceptions.external_api import ServiceUnavailable
from ucl_test_engine.serializers.rule_test_execution import (
    RuleTestExecutionForSiemExecutionSerializer,
)
from ucl_test_engine.services.zeus_mock.zeus_connection_service import (
    ZeusConnectionService,
)
from ucl_test_engine.tests.set_up import CreateUclTestEngineData


class TestZeusConnectionService(CreateUclTestEngineData, TestCase):
    """Test Service ZeusConnectionService"""

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def setUp(self) -> None:
        """clear force_authenticate before each test"""
        self.init_user()

    @responses.activate
    def test_execute_tests_succeed(self):
        """test sending rule test execution object to zeus mock correctly"""
        self.init_test_execution_1_without_result()

        responses.add(
            responses.POST,
            f"{settings.SIEM_BASE_URL}",
            status=200,
        )

        res: None = ZeusConnectionService.execute_tests(
            [self.test_execution_1_without_result],
        )

        self.assertIsNone(res)

        self.assertEqual(len(responses.calls), 1)
        self.assertEqual(responses.calls[0].request.url, f"{settings.SIEM_BASE_URL}/")

        serializer = RuleTestExecutionForSiemExecutionSerializer(
            [self.test_execution_1_without_result], many=True
        )
        self.assertEqual(
            json.loads(responses.calls[0].request.body.decode("utf-8")), serializer.data
        )

    @responses.activate
    def test_execute_tests_fails_on_400(self):
        """test failure external api returns 400"""

        response_data = {}

        responses.add(
            responses.POST,
            f"{settings.SIEM_BASE_URL}",
            json=response_data,
            status=400,
        )

        with pytest.raises(ServiceUnavailable) as exc:
            ZeusConnectionService.execute_tests(
                [self.test_execution_1_without_result],
            )

        self.assertEqual(
            exc.value.detail,
            "Zeus mock service temporarily unavailable try again later.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "zeus_mock_service_unavailable",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)

    @responses.activate
    def test_execute_tests_fails_on_connection_error(self):
        """test failure on connection error"""

        rsp = responses.Response(
            responses.POST,
            f"{settings.SIEM_BASE_URL}",
            status=400,
        )

        my_error = requests.ConnectionError("custom error")
        my_error.response = rsp

        with pytest.raises(ServiceUnavailable) as exc:
            ZeusConnectionService.execute_tests(
                [self.test_execution_1_without_result],
            )

        self.assertEqual(
            exc.value.detail,
            "Zeus mock service temporarily unavailable try again later.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "zeus_mock_service_unavailable",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)

    @responses.activate
    def test_execute_tests_fails_on_404(self):
        """test failure external api returns 404"""

        response_data = {}

        responses.add(
            responses.POST,
            f"{settings.SIEM_BASE_URL}",
            json=response_data,
            status=404,
        )

        with pytest.raises(NotFound) as exc:
            ZeusConnectionService.execute_tests(
                [self.test_execution_1_without_result],
            )

        self.assertEqual(
            exc.value.detail,
            "Zeus mock service not found.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "zeus_mock_service_not_found",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_404_NOT_FOUND)

    @responses.activate
    def test_execute_tests_fails_on_401(self):
        """test failure external api returns 401"""

        response_data = {}

        responses.add(
            responses.POST,
            f"{settings.SIEM_BASE_URL}",
            json=response_data,
            status=401,
        )

        with pytest.raises(AuthenticationFailed) as exc:
            ZeusConnectionService.execute_tests(
                [self.test_execution_1_without_result],
            )

        self.assertEqual(
            exc.value.detail,
            "Zeus mock service denied access.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "zeus_mock_service_authentication_failed",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_401_UNAUTHORIZED)

    @pytest.mark.asyncio
    @aioresponses()
    @override_settings(
        OPENSEARCH_VERIFY_SSL_CERTIFICATE=True,
        OPENSEARCH_USER_NAME="admin",
        OPENSEARCH_USER_PASSWORD="admin",
    )
    async def test__get_predetection_ids_of_execution_from_events_index_succeed(
        self, mock_responses
    ):
        """test get pre_detection_ids from opensearch by execution_id correctly"""
        await sync_to_async(self.init_test_execution_1_without_result)()

        response_data = {
            "took": 51,
            "timed_out": False,
            "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
            "hits": {
                "total": {"value": 2, "relation": "eq"},
                "max_score": 1.0,
                "hits": [
                    {
                        "_index": "event",
                        "_id": "ILlX_YkB9SJJhbxijBqL",
                        "_score": 1.0,
                        "_source": {
                            "test_key1": "test_value1",
                            "execution_id": str(self.test_execution_1_without_result.id),
                            "pre_detection_id": "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                        },
                    },
                    {
                        "_index": "event",
                        "_id": "I7lX_YkB9SJJhbxijRpR",
                        "_score": 1.0,
                        "_source": {
                            "test_key2": "test_value2",
                            "execution_id": str(self.test_execution_1_without_result.id),
                            "pre_detection_id": "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                        },
                    },
                ],
            },
        }

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/event/_search", status=200, payload=response_data
        )

        session = ClientSession()
        res: list[str] = (
            await ZeusConnectionService._ZeusConnectionService__get_predetection_ids_of_execution_from_events_index(  # pylint: disable=C0301:line-too-long
                self.test_execution_1_without_result.execution_id, session
            )
        )
        await session.close()

        self.assertIsInstance(res, list)
        mock_responses.assert_called_once()

        mock_responses.assert_called_once_with(
            f"{settings.OPENSEARCH_BASE_URL}/event/_search",
            method="GET",
            json={
                "query": {
                    "term": {
                        "execution_id": str(self.test_execution_1_without_result.execution_id),
                    }
                },
                "from": 0,
                "size": 10000,
            },
            timeout=ClientTimeout(total=10, connect=None, sock_read=None, sock_connect=None),
            auth=BasicAuth(login="admin", password="admin", encoding="latin1"),
            ssl=True,
        )

        self.assertEqual(
            res, ["b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4", "33236e07-2ef1-4f3b-9497-7ece4d7ff587"]
        )

    @aioresponses()
    @pytest.mark.asyncio
    async def test__get_predetection_ids_of_execution_from_events_index_fails_on_400(
        self, mock_responses
    ):
        """test failure external api returns 400"""

        response_data = {}

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/event/_search", status=400, payload=response_data
        )

        session = ClientSession()
        with pytest.raises(ServiceUnavailable) as exc:
            await ZeusConnectionService._ZeusConnectionService__get_predetection_ids_of_execution_from_events_index(  # pylint: disable=C0301:line-too-long
                [self.test_execution_1_without_result], session
            )

        await session.close()
        self.assertEqual(
            exc.value.detail,
            "Opensearch service temporarily unavailable try again later.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "opensearch_service_unavailable",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)

    @aioresponses()
    @pytest.mark.asyncio
    async def test__get_predetection_ids_of_execution_from_events_index_fails_on_connection_error(
        self, mock_responses
    ):
        """test failure on connection error"""

        mock_responses.get(f"{settings.OPENSEARCH_BASE_URL}/event/_search", status=400)

        my_error = requests.ConnectionError("custom error")
        my_error.response = mock_responses

        session = ClientSession()
        with pytest.raises(ServiceUnavailable) as exc:
            await ZeusConnectionService._ZeusConnectionService__get_predetection_ids_of_execution_from_events_index(  # pylint: disable=C0301:line-too-long
                [self.test_execution_1_without_result], session
            )

        await session.close()
        self.assertEqual(
            exc.value.detail,
            "Opensearch service temporarily unavailable try again later.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "opensearch_service_unavailable",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)

    @aioresponses()
    @pytest.mark.asyncio
    async def test__get_predetection_ids_of_execution_from_events_index_fails_on_404(
        self, mock_responses
    ):
        """test failure external api returns 404"""

        response_data = {}

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/event/_search", status=404, payload=response_data
        )

        session = ClientSession()
        with pytest.raises(NotFound) as exc:
            await ZeusConnectionService._ZeusConnectionService__get_predetection_ids_of_execution_from_events_index(  # pylint: disable=C0301:line-too-long
                [self.test_execution_1_without_result], session
            )
        await session.close()

        self.assertEqual(
            exc.value.detail,
            "Opensearch service not found.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "opensearch_service_not_found",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_404_NOT_FOUND)

    @aioresponses()
    @pytest.mark.asyncio
    async def test__get_predetection_ids_of_execution_from_events_index_fails_on_401(
        self, mock_responses
    ):
        """test failure external api returns 401"""

        response_data = {}

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/event/_search", status=401, payload=response_data
        )

        session = ClientSession()
        with pytest.raises(AuthenticationFailed) as exc:
            await ZeusConnectionService._ZeusConnectionService__get_predetection_ids_of_execution_from_events_index(  # pylint: disable=C0301:line-too-long
                [self.test_execution_1_without_result], session
            )

        await session.close()
        self.assertEqual(
            exc.value.detail,
            "Opensearch service denied access.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "opensearch_service_authentication_failed",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_401_UNAUTHORIZED)

    @aioresponses()
    @pytest.mark.asyncio
    @override_settings(
        OPENSEARCH_VERIFY_SSL_CERTIFICATE=True,
        OPENSEARCH_USER_NAME="admin",
        OPENSEARCH_USER_PASSWORD="admin",
    )
    async def test__get_records_count_by_predetection_ids_from_sre_index_succeed(
        self, mock_responses
    ):
        """test get security related events from opensearch by pre_detection_id correctly"""
        await sync_to_async(self.init_test_execution_1_without_result)()

        response_data = {
            "took": 51,
            "timed_out": False,
            "_shards": {"total": 1, "successful": 1, "skipped": 0, "failed": 0},
            "hits": {
                "total": {"value": 2, "relation": "eq"},
                "max_score": 1.0,
                "hits": [
                    {
                        "_index": "sre",
                        "_id": "ILlX_YkB9SJJhbxijBqL",
                        "_score": 1.0,
                        "_source": {
                            "test_key1": "test_value1",
                            "execution_id": str(self.test_execution_1_without_result.id),
                            "pre_detection_id": "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                        },
                    },
                    {
                        "_index": "sre",
                        "_id": "I7lX_YkB9SJJhbxijRpR",
                        "_score": 1.0,
                        "_source": {
                            "test_key2": "test_value2",
                            "execution_id": str(self.test_execution_1_without_result.id),
                            "pre_detection_id": "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                        },
                    },
                ],
            },
        }

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/sre/_search", status=200, payload=response_data
        )

        pre_detection_ids: list[str] = [
            "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
            "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
        ]
        session = ClientSession()
        res: int = (
            await ZeusConnectionService._ZeusConnectionService__get_records_count_by_predetection_ids_from_sre_index(  # pylint: disable=C0301:line-too-long
                pre_detection_ids, session
            )
        )

        await session.close()

        self.assertIsInstance(res, int)
        mock_responses.assert_called_once()

        mock_responses.assert_called_once_with(
            f"{settings.OPENSEARCH_BASE_URL}/sre/_search",
            method="GET",
            json={
                "query": {
                    "terms": {
                        "pre_detection_id": pre_detection_ids,
                    }
                }
            },
            timeout=ClientTimeout(total=10, connect=None, sock_read=None, sock_connect=None),
            auth=BasicAuth(login="admin", password="admin", encoding="latin1"),
            ssl=True,
        )

        self.assertEqual(res, 2)

    @aioresponses()
    @pytest.mark.asyncio
    async def test__get_records_count_by_predetection_ids_from_sre_index_fails_on_400(
        self, mock_responses
    ):
        """test failure external api returns 400"""

        response_data = {}

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/sre/_search", status=400, payload=response_data
        )

        session = ClientSession()

        with pytest.raises(ServiceUnavailable) as exc:
            await ZeusConnectionService._ZeusConnectionService__get_records_count_by_predetection_ids_from_sre_index(  # pylint: disable=C0301:line-too-long
                [self.test_execution_1_without_result], session
            )

        await session.close()
        self.assertEqual(
            exc.value.detail,
            "Opensearch service temporarily unavailable try again later.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "opensearch_service_unavailable",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)

    @aioresponses()
    @pytest.mark.asyncio
    async def test__get_records_count_by_predetection_ids_from_sre_index_fails_on_connection_error(
        self, mock_responses
    ):
        """test failure on connection error"""

        mock_responses.get(f"{settings.OPENSEARCH_BASE_URL}/sre/_search", status=400)

        my_error = requests.ConnectionError("custom error")
        my_error.response = mock_responses

        session = ClientSession()
        with pytest.raises(ServiceUnavailable) as exc:
            await ZeusConnectionService._ZeusConnectionService__get_records_count_by_predetection_ids_from_sre_index(  # pylint: disable=C0301:line-too-long
                [self.test_execution_1_without_result], session
            )

        await session.close()
        self.assertEqual(
            exc.value.detail,
            "Opensearch service temporarily unavailable try again later.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "opensearch_service_unavailable",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)

    @aioresponses()
    @pytest.mark.asyncio
    async def test__get_records_count_by_predetection_ids_from_sre_index_fails_on_404(
        self, mock_responses
    ):
        """test failure external api returns 404"""

        response_data = {}

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/sre/_search", status=404, payload=response_data
        )

        session = ClientSession()
        with pytest.raises(NotFound) as exc:
            await ZeusConnectionService._ZeusConnectionService__get_records_count_by_predetection_ids_from_sre_index(  # pylint: disable=C0301:line-too-long
                [self.test_execution_1_without_result], session
            )

        await session.close()
        self.assertEqual(
            exc.value.detail,
            "Opensearch service not found.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "opensearch_service_not_found",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_404_NOT_FOUND)

    @aioresponses()
    @pytest.mark.asyncio
    async def test__get_records_count_by_predetection_ids_from_sre_index_fails_on_401(
        self, mock_responses
    ):
        """test failure external api returns 401"""

        response_data = {}

        mock_responses.get(
            f"{settings.OPENSEARCH_BASE_URL}/sre/_search", status=401, payload=response_data
        )

        session = ClientSession()
        with pytest.raises(AuthenticationFailed) as exc:
            await ZeusConnectionService._ZeusConnectionService__get_records_count_by_predetection_ids_from_sre_index(  # pylint: disable=C0301:line-too-long
                [self.test_execution_1_without_result], session
            )

        await session.close()
        self.assertEqual(
            exc.value.detail,
            "Opensearch service denied access.",
        )
        self.assertEqual(
            exc.value.detail.code,
            "opensearch_service_authentication_failed",
        )
        self.assertEqual(exc.value.status_code, status.HTTP_401_UNAUTHORIZED)

    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "_ZeusConnectionService__get_predetection_ids_of_execution_from_events_index",
        return_value=[
            "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
            "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
        ],
        new_callable=AsyncMock,
    )
    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "_ZeusConnectionService__get_records_count_by_predetection_ids_from_sre_index",
        return_value=1,
        new_callable=AsyncMock,
    )
    @pytest.mark.asyncio
    async def test_get_rule_test_result_succeed(
        self,
        mock__get_records_count_by_predetection_ids_from_sre_index,
        mock__get_predetection_ids_of_execution_from_events_index,
    ):
        """
        test pre_detection_ids are correctly fetched and
          sres are correctly fetched by pre_detection_ids is returned from external
        """
        await sync_to_async(self.init_test_execution_1_without_result)()

        session = ClientSession()

        result: int | None = await ZeusConnectionService.get_rule_test_result(
            self.test_execution_1_without_result.id, session
        )

        await session.close()

        self.assertEqual(result, 1)

        mock__get_predetection_ids_of_execution_from_events_index.assert_called_once()
        mock__get_predetection_ids_of_execution_from_events_index.assert_called_with(
            self.test_execution_1_without_result.id, session, 0
        )
        mock__get_records_count_by_predetection_ids_from_sre_index.assert_called_once()
        mock__get_records_count_by_predetection_ids_from_sre_index.assert_called_with(
            [
                "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
            ],
            session,
        )

    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "_ZeusConnectionService__get_predetection_ids_of_execution_from_events_index",
        return_value=[],
        new_callable=AsyncMock,
    )
    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "_ZeusConnectionService__get_records_count_by_predetection_ids_from_sre_index",
        return_value=None,
        new_callable=AsyncMock,
    )
    @pytest.mark.asyncio
    async def test_get_rule_test_result_succeed_events_not_yet_processed(
        self,
        mock__get_records_count_by_predetection_ids_from_sre_index,
        mock__get_predetection_ids_of_execution_from_events_index,
    ):
        """
        test events not yet processed.
        therefore no pre_detection_ids existing.
        do not check for sres.
        """
        await sync_to_async(self.init_test_execution_1_without_result)()

        session = ClientSession()

        result: int | None = await ZeusConnectionService.get_rule_test_result(
            self.test_execution_1_without_result.id, session
        )

        await session.close()

        self.assertIsNone(result)
        mock__get_predetection_ids_of_execution_from_events_index.assert_called_once()
        mock__get_predetection_ids_of_execution_from_events_index.assert_called_with(
            self.test_execution_1_without_result.id, session, 0
        )
        self.assertEqual(mock__get_records_count_by_predetection_ids_from_sre_index.call_count, 0)

    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "_ZeusConnectionService__get_open_serach_pagination_limit",
        return_value=2,
    )
    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "_ZeusConnectionService__get_predetection_ids_of_execution_from_events_index",
        side_effect=[
            ["b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4", "33236e07-2ef1-4f3b-9497-7ece4d7ff587"],
            ["33236e07-2ef1-4f3b-9497-333333333333"],
        ],
        new_callable=AsyncMock,
    )
    @patch(
        "ucl_test_engine.services.zeus_mock.zeus_connection_service.ZeusConnectionService."
        + "_ZeusConnectionService__get_records_count_by_predetection_ids_from_sre_index",
        return_value=2,
        new_callable=AsyncMock,
    )
    @pytest.mark.asyncio
    async def test_get_rule_test_result_succeed_multiple_requests(
        self,
        mock__get_records_count_by_predetection_ids_from_sre_index,
        mock__get_predetection_ids_of_execution_from_events_index,
        mock__get_open_serach_pagination_limit,
    ):
        """
        test pre_detection_ids are correctly fetched.
        Two requests are needed because pagination limit is exceeded.
        sres are correctly fetched by pre_detection_ids is returned from external
        """
        await sync_to_async(self.init_test_execution_1_without_result)()

        session = ClientSession()

        result: int | None = await ZeusConnectionService.get_rule_test_result(
            self.test_execution_1_without_result.id, session
        )
        await session.close()

        self.assertEqual(result, 2)

        self.assertEqual(mock__get_predetection_ids_of_execution_from_events_index.call_count, 2)
        calls = [
            call(self.test_execution_1_without_result.id, session, 0),
            call(self.test_execution_1_without_result.id, session, 2),
        ]
        mock__get_predetection_ids_of_execution_from_events_index.assert_has_calls(
            calls, any_order=True
        )

        mock__get_records_count_by_predetection_ids_from_sre_index.assert_called_once()
        mock__get_records_count_by_predetection_ids_from_sre_index.assert_called_with(
            [
                "b0a0cbd8-48dd-43fb-b5d0-6968e8c756b4",
                "33236e07-2ef1-4f3b-9497-7ece4d7ff587",
                "33236e07-2ef1-4f3b-9497-333333333333",
            ],
            session,
        )

        mock__get_open_serach_pagination_limit.assert_called()
