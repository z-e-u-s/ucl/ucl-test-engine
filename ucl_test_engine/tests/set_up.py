"""class to setup data required for testing"""

import copy

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractBaseUser
from freezegun import freeze_time

from ucl_test_engine.models.test_engine_test_execution import TestEngineTestExecution
from ucl_test_engine.zeus_roles import ZeusRoles

User = get_user_model()


# pylint: disable=too-many-instance-attributes, too-many-public-methods, too-many-lines
@freeze_time("11-11-2021 11:11:11")
class CreateUclTestEngineData:
    """Create UCL Test Engine data for testing"""

    USERNAME: str = "test@test.de"
    test_user: AbstractBaseUser | None = None
    system_user: AbstractBaseUser | None = None
    token_ucl_administrator: dict | None = None
    token_ucl_security_analyst: dict | None = None
    token_ucl_test_engine: dict | None = None
    token_ucl_use_case_requester: dict | None = None
    token_ucl_backend: dict | None = None
    token_fda_administrator: dict | None = None
    token_fda_ecs_maintainer: dict | None = None
    token_fda_onboarder: dict | None = None
    token_fda_reader: dict | None = None
    test_execution_1_without_result: TestEngineTestExecution | None = None
    test_execution_2_with_result: TestEngineTestExecution | None = None
    test_execution_3_with_result: TestEngineTestExecution | None = None
    test_execution_4_with_result_and_other_url: TestEngineTestExecution | None = None
    test_execution_5_without_result: TestEngineTestExecution | None = None

    def init_test_user(self) -> None:
        """
        create initial test user data for tests
        """
        if self.test_user is not None:
            return

        self.test_user = User.objects.create_user(
            username=self.USERNAME,
            email=self.USERNAME,
        )

    def init_system_user(self) -> None:
        """
        create initial system user data for tests
        """
        if self.system_user is not None:
            return

        self.system_user = User.objects.get(username="system")

    def init_user(self) -> None:
        """
        create initial user data for tests
        """
        self.init_test_user()
        self.init_system_user()

    def init_tokens(self) -> None:
        """
        create initial token data for tests
        """
        if self.token_ucl_security_analyst is not None:
            # only one check for all tokens
            return
        if self.test_user is None:
            self.init_user()

        client_id_for_authz_roles = settings.KEYCLOAK_CONFIG["CLIENT_ID_FOR_AUTHZ_ROLES"]

        # Tokens
        base_token = {"resource_access": {client_id_for_authz_roles: {"roles": []}}}
        self.token_ucl_administrator = copy.deepcopy(base_token)
        self.token_ucl_administrator["resource_access"][client_id_for_authz_roles]["roles"].append(
            ZeusRoles.UCL_ADMINISTRATOR.value
        )
        self.token_ucl_security_analyst = copy.deepcopy(base_token)
        self.token_ucl_security_analyst["resource_access"][client_id_for_authz_roles][
            "roles"
        ].append(ZeusRoles.UCL_SECURITY_ANALYST.value)
        self.token_ucl_test_engine = copy.deepcopy(base_token)
        self.token_ucl_test_engine["resource_access"][client_id_for_authz_roles]["roles"].append(
            ZeusRoles.UCL_TEST_ENGINE.value
        )
        self.token_ucl_use_case_requester = copy.deepcopy(base_token)
        self.token_ucl_use_case_requester["resource_access"][client_id_for_authz_roles][
            "roles"
        ].append(ZeusRoles.UCL_USE_CASE_REQUESTER.value)
        self.token_ucl_backend = copy.deepcopy(base_token)
        self.token_ucl_backend["resource_access"][client_id_for_authz_roles]["roles"].append(
            ZeusRoles.UCL_BACKEND.value
        )

        self.token_fda_administrator = copy.deepcopy(base_token)
        self.token_fda_administrator["resource_access"][client_id_for_authz_roles]["roles"].append(
            ZeusRoles.FDA_ADMINISTRATOR.value
        )
        self.token_fda_ecs_maintainer = copy.deepcopy(base_token)
        self.token_fda_ecs_maintainer["resource_access"][client_id_for_authz_roles]["roles"].append(
            ZeusRoles.FDA_ECS_MAINTAINER.value
        )
        self.token_fda_onboarder = copy.deepcopy(base_token)
        self.token_fda_onboarder["resource_access"][client_id_for_authz_roles]["roles"].append(
            ZeusRoles.FDA_ONBOARDER.value
        )
        self.token_fda_reader = copy.deepcopy(base_token)
        self.token_fda_reader["resource_access"][client_id_for_authz_roles]["roles"].append(
            ZeusRoles.FDA_READER.value
        )

    def init_test_execution_1_without_result(self) -> None:
        """
        create initial test execution 1 without result data for tests
        """
        if self.test_execution_1_without_result is not None:
            return
        if self.test_user is None:
            self.init_user()

        test_execution_raw_data = {
            "execution_id": "cf136325-3333-42b0-a49c-111111111111",
            "rule_id": "1113ead1-f329-42b0-a49c-111111111111",
            "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
            "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
        }
        self.test_execution_1_without_result = TestEngineTestExecution.objects.create(
            **test_execution_raw_data
        )

    def init_test_execution_2_with_result(self) -> None:
        """
        create initial test execution 2 with result data for tests
        """
        if self.test_execution_2_with_result is not None:
            return
        if self.test_user is None:
            self.init_user()

        test_execution_raw_data = {
            "execution_id": "cf136325-3333-42b0-a49c-222222222222",
            "rule_id": "1113ead1-f329-42b0-a49c-222222222222",
            "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
            "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            "result": 1,
            "updated": "2023-11-11T11:11:11Z",
        }
        self.test_execution_2_with_result = TestEngineTestExecution.objects.create(
            **test_execution_raw_data
        )

    def init_test_execution_3_with_result(self) -> None:
        """
        create initial test execution 3 with result data for tests
        """
        if self.test_execution_3_with_result is not None:
            return
        if self.test_user is None:
            self.init_user()

        test_execution_raw_data = {
            "execution_id": "cf136325-3333-42b0-a49c-333333333333",
            "rule_id": "1113ead1-f329-42b0-a49c-333333333333",
            "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
            "test_events": [{"firstKey": "firstValue", "thirdKey": 3}],
            "result": 2,
            "updated": "2023-11-11T11:11:11Z",
        }
        self.test_execution_3_with_result = TestEngineTestExecution.objects.create(
            **test_execution_raw_data
        )

    def init_test_execution_4_with_result_other_url(self) -> None:
        """
        create initial test execution 4 with result and other url data for tests
        """
        if self.test_execution_4_with_result_and_other_url is not None:
            return
        if self.test_user is None:
            self.init_user()

        test_execution_raw_data = {
            "execution_id": "cf136325-3333-42b0-a49c-444444444444",
            "rule_id": "1113ead1-f329-42b0-a49c-444444444444",
            "response_url": "http://localhost:8000/api/v1/other-url/bulk-update",
            "test_events": [{"firstKey": "firstValue", "fourthKey": 4}],
            "result": 4,
            "updated": "2023-11-11T11:11:11Z",
        }
        self.test_execution_4_with_result_and_other_url = TestEngineTestExecution.objects.create(
            **test_execution_raw_data
        )

    def init_test_execution_5_without_result(self) -> None:
        """
        create initial test execution 5 without result data for tests
        """
        if self.test_execution_5_without_result is not None:
            return
        if self.test_user is None:
            self.init_user()

        test_execution_raw_data = {
            "execution_id": "cf136325-3333-42b0-a49c-555555555555",
            "rule_id": "1113ead1-f329-42b0-a49c-555555555555",
            "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
            "test_events": [{"firstKey": "firstValue", "fifthKey": 5}],
        }
        self.test_execution_5_without_result = TestEngineTestExecution.objects.create(
            **test_execution_raw_data
        )

    def init_all(self) -> None:
        """
        create all data for tests
        """
        self.init_user()
        self.init_tokens()
        self.init_test_execution_1_without_result()
        self.init_test_execution_2_with_result()
        self.init_test_execution_3_with_result()
        self.init_test_execution_4_with_result_other_url()
        self.init_test_execution_5_without_result()
