""" Test JSON Response Renderer """

import json
import uuid
import datetime

from freezegun import freeze_time
from django.utils import timezone
from django.test import TestCase
from rest_framework.test import APIRequestFactory
from rest_framework import status
from rest_framework.response import Response
from ucl_test_engine.renderers.json_response_renderer import JSONResponseRenderer


class JSONResponseRendererTest(TestCase):
    """Test JSON Response Renderer"""

    def setUp(self):
        self.factory = APIRequestFactory()
        self.renderer = JSONResponseRenderer()

    def test_render_data_response(self):
        data = {"field1": "value1", "field2": "value2"}
        response = Response(data, status=status.HTTP_200_OK)
        response.accepted_renderer = self.renderer
        response.accepted_media_type = "application/json"
        response.renderer_context = {"response": response}
        json_content = response.render().content
        json_data = json.loads(json_content)
        self.assertEqual(json_data, {"data": data, "message": ""})

    def test_render_error_response(self):
        data = {"detail": "Error message"}
        response = Response(data, status=status.HTTP_400_BAD_REQUEST)
        response.accepted_renderer = self.renderer
        response.accepted_media_type = "application/json"
        response.renderer_context = {"response": response}
        json_content = response.render().content
        json_data = json.loads(json_content)
        self.assertEqual(json_data, {"data": [], "message": "Error message"})

    def test_render_custom_response(self):
        data = {"message": "custom message", "data": {"field1": "value1", "field2": "value2"}}
        response = Response(data, status=status.HTTP_200_OK)
        response.accepted_renderer = self.renderer
        response.accepted_media_type = "application/json"
        response.renderer_context = {"response": response}
        json_content = response.render().content
        json_data = json.loads(json_content)
        self.assertEqual(
            json_data,
            {"data": {"field1": "value1", "field2": "value2"}, "message": "custom message"},
        )

    def test_error_message_rendering(self):
        data = {"foo": "Error message"}
        response = Response(data, status=status.HTTP_400_BAD_REQUEST)
        response.accepted_renderer = self.renderer
        response.accepted_media_type = "application/json"
        response.renderer_context = {"response": response}
        json_content = response.render().content
        json_data = json.loads(json_content)
        self.assertEqual(json_data, {"data": [], "message": str(data)})

    def test_uuid_encoding(self):
        data = {"uuid_field": uuid.uuid4()}
        response = Response(data, status=status.HTTP_200_OK)
        response.accepted_renderer = self.renderer
        response.accepted_media_type = "application/json"
        response.renderer_context = {"response": response}
        json_content = response.render().content
        json_data = json.loads(json_content)

        self.assertEqual(
            json_data,
            {
                "data": {"uuidField": str(data["uuid_field"])},
                "message": "",
            },
        )

    def test_date_encoding_1(self):
        """
        check datetime now with timezone utc is rendered as ISOString with 'Z' as last letter.
        """
        date = datetime.datetime.now(datetime.UTC)
        data = {"date_field": date}
        response = Response(data, status=status.HTTP_200_OK)
        response.accepted_renderer = self.renderer
        response.accepted_media_type = "application/json"
        response.renderer_context = {"response": response}
        json_content = response.render().content
        json_data = json.loads(json_content)

        self.assertEqual(
            json_data,
            {
                "data": {"dateField": date.isoformat().replace("+00:00", "Z")},
                "message": "",
            },
        )

    @freeze_time("2023-08-24T11:22:33.666666")
    def test_date_encoding_2(self):
        """
        check timezone now with default timezone (utc),
         is rendered as ISOString with 'Z' as last letter.
        """
        date = timezone.now()
        data = {"date_field": date}
        response = Response(data, status=status.HTTP_200_OK)
        response.accepted_renderer = self.renderer
        response.accepted_media_type = "application/json"
        response.renderer_context = {"response": response}
        json_content = response.render().content
        json_data = json.loads(json_content)

        self.assertEqual(
            json_data,
            {
                "data": {"dateField": date.isoformat().replace("+00:00", "Z")},
                "message": "",
            },
        )
        self.assertEqual(
            json_data,
            {
                "data": {"dateField": "2023-08-24T11:22:33.666666Z"},
                "message": "",
            },
        )
