""" Test ucl_test_engine api routes """

from unittest import mock

from rest_framework.test import APIRequestFactory
from django.test.testcases import TestCase


from ucl_test_engine.views.base import BaseViewSet

from .set_up import User


class BaseViewSetTest(TestCase):
    """Test Base ViewSet functions"""

    def setUp(self):
        self.view = BaseViewSet()
        self.user = User.objects.create_user(username="testuser", password="testpassword")
        self.factory = APIRequestFactory()
        self.request = self.factory.post("/", {}, format="json")
        self.request.user = self.user

    def test_perform_create(self):
        serializer = mock.Mock()
        self.view.request = self.request
        self.view.perform_create(serializer)
        serializer.save.assert_called_once_with(updated=None)

    def test_perform_update(self):
        serializer = mock.Mock()
        self.view.request = self.request
        self.view.perform_update(serializer)
        serializer.save.assert_called_once()
        args, kwargs = serializer.save.call_args
        self.assertIsNotNone(kwargs["updated"])
