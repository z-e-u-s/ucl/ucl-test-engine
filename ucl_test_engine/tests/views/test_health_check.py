"""API Tests for health and readiness probe endpoints"""

from django.db import connection
from django.test import override_settings
from django.db.utils import OperationalError
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from ucl_test_engine.views.health_check import HealthCheckAPIView, ReadinessCheckAPIView


class QueryTimeoutWrapper:  # pylint: disable=R0903:too-few-public-methods
    """DB wrapper to mock a connection timeout."""

    def __call__(self, execute, *args, **kwargs):
        raise OperationalError("Connection timed out")


class HealthCheckAPITestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = APIClient()
        HealthCheckAPIView.throttle_classes = []  # disable throttling for testing
        cls.url = "/api/v1/healthcheck"

    def test_health_check(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(response.data)


class ReadinessCheckAPITestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = APIClient()
        ReadinessCheckAPIView.throttle_classes = []  # disable throttling for testing
        cls.url = "/api/v1/readinesscheck"

    def test_readinesscheck(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(response.data)

    def test_readinesscheck_db_down(self):
        with connection.execute_wrapper(QueryTimeoutWrapper()):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
            self.assertEqual(
                response.data, {"ready": False, "details": "Database connection failed!"}
            )

    def test_readinesscheck_auth_disabled(self):
        with override_settings(DISABLE_AUTHENTICATION=True):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
            self.assertEqual(
                response.data, {"ready": False, "details": "Authentication is disabled!"}
            )
