""" Test ucl_test_engine api routes """

from unittest.mock import patch

from rest_framework import status as api_status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient, APIRequestFactory, APITestCase

from ucl_test_engine.models.test_engine_test_execution import TestEngineTestExecution
from ucl_test_engine.tests.set_up import CreateUclTestEngineData


class UclApiTestsTestExecution(
    CreateUclTestEngineData, APITestCase
):  # pylint:disable=R0904:too-many-public-methods
    """Test TestExecution API"""

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.client = APIClient()

    def setUp(self) -> None:
        """clear force_authenticate before each test"""
        self.factory = APIRequestFactory()
        self.client.force_authenticate()
        self.init_tokens()
        self.init_user()

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService.test_rule_test_executions",
        return_value=None,
    )
    def test_create_test_executions_succeed_one_test_execution(
        self, mock_test_rule_test_executions
    ):
        self.init_test_execution_1_without_result()
        self.assertEqual(TestEngineTestExecution.objects.all().count(), 1)
        self.client.force_authenticate(self.test_user, self.token_ucl_backend)
        data = [
            {
                "execution_id": "cf136325-3333-42b0-a49c-b650e5014785",
                "rule_id": "1113ead1-f329-42b0-a49c-741961528514",
                "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
                "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            }
        ]
        response = self.client.post(reverse("test-engine-execution-list"), data=data, format="json")
        self.assertEqual(api_status.HTTP_202_ACCEPTED, response.status_code)
        mock_test_rule_test_executions.assert_called_once()

        self.assertEqual(TestEngineTestExecution.objects.all().count(), 2)
        test_exec: TestEngineTestExecution = TestEngineTestExecution.objects.all().get(
            execution_id=data[0]["execution_id"]
        )

        mock_test_rule_test_executions.assert_called_once_with(test_executions=[test_exec])

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService.test_rule_test_executions",
        return_value=None,
    )
    def test_create_test_executions_succeed_two_test_execution(
        self, mock_test_rule_test_executions
    ):
        self.assertEqual(TestEngineTestExecution.objects.all().count(), 0)
        self.client.force_authenticate(self.test_user, self.token_ucl_backend)
        data = [
            {
                "execution_id": "cf136325-3333-42b0-a49c-000000000000",
                "rule_id": "1113ead1-f329-42b0-a49c-741961528514",
                "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
                "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            },
            {
                "execution_id": "cf136325-3333-42b0-a49c-222222222222",
                "rule_id": "1113ead1-f329-42b0-a49c-222222222222",
                "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
                "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            },
        ]
        response = self.client.post(reverse("test-engine-execution-list"), data=data, format="json")
        self.assertEqual(api_status.HTTP_202_ACCEPTED, response.status_code)
        mock_test_rule_test_executions.assert_called_once()

        test_exec: list[TestEngineTestExecution] = list(TestEngineTestExecution.objects.all())

        mock_test_rule_test_executions.assert_called_once_with(test_executions=test_exec)
        self.assertEqual(TestEngineTestExecution.objects.all().count(), 2)

    @patch(
        "ucl_test_engine.services.test_engine.TestEngineService.test_rule_test_executions",
        return_value=None,
    )
    def test_test_executions_api_post_check_permissions(
        self, mock_test_rule_test_executions
    ):  # pylint:disable=W0613:unused-argument
        data = [
            {
                "execution_id": "cf136325-3333-42b0-a49c-b650e5014785",
                "rule_id": "1113ead1-f329-42b0-a49c-741961528514",
                "response_url": "http://localhost:8000/api/v1/rule-test-executions/bulk-update",
                "test_events": [{"firstKey": "firstValue", "secondKey": 2}],
            }
        ]
        permissions = [
            (self.token_ucl_administrator, api_status.HTTP_403_FORBIDDEN),
            (self.token_ucl_security_analyst, api_status.HTTP_403_FORBIDDEN),
            (self.token_ucl_test_engine, api_status.HTTP_403_FORBIDDEN),
            (self.token_ucl_use_case_requester, api_status.HTTP_403_FORBIDDEN),
            (self.token_ucl_backend, api_status.HTTP_202_ACCEPTED),
            (self.token_fda_administrator, api_status.HTTP_403_FORBIDDEN),
            (self.token_fda_ecs_maintainer, api_status.HTTP_403_FORBIDDEN),
            (self.token_fda_onboarder, api_status.HTTP_403_FORBIDDEN),
            (self.token_fda_reader, api_status.HTTP_403_FORBIDDEN),
        ]
        for token, expected_status_code in permissions:
            self.client.force_authenticate(self.test_user, token)
            response = self.client.post(
                reverse("test-engine-execution-list"), data=data, format="json"
            )
            self.assertEqual(expected_status_code, response.status_code)

    def test_executions_api_create_check_permissions_no_token(self):
        data = {}
        self.client.force_authenticate()
        response = self.client.post(reverse("test-engine-execution-list"), data=data, format="json")
        self.assertEqual(api_status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_executions_api_partial_update_check_permissions(self):
        data = {}
        permissions = [
            (self.token_ucl_administrator, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_security_analyst, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_use_case_requester, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_backend, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_administrator, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_ecs_maintainer, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_onboarder, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_reader, api_status.HTTP_404_NOT_FOUND),
        ]
        for token, expected_status_code in permissions:
            self.client.force_authenticate(self.test_user, token)
            response = self.client.patch(
                "/api/v1/executions/cf136325-3333-42b0-a49c-b650e5014785",
                data,
                format="json",
            )
            self.assertEqual(expected_status_code, response.status_code)

    def test_executions_api_delete_check_permissions(self):
        permissions = [
            (self.token_ucl_administrator, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_security_analyst, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_use_case_requester, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_backend, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_administrator, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_ecs_maintainer, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_onboarder, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_reader, api_status.HTTP_404_NOT_FOUND),
        ]
        for token, expected_status_code in permissions:
            self.client.force_authenticate(self.test_user, token)
            response = self.client.delete(
                "/api/v1/executions/cf136325-3333-42b0-a49c-b650e5014785",
                format="json",
            )
            self.assertEqual(expected_status_code, response.status_code)

    def test_executions_api_retrieve_check_permissions(self):
        permissions = [
            (self.token_ucl_administrator, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_security_analyst, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_use_case_requester, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_backend, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_administrator, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_ecs_maintainer, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_onboarder, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_reader, api_status.HTTP_404_NOT_FOUND),
        ]
        for token, expected_status_code in permissions:
            self.client.force_authenticate(self.test_user, token)
            response = self.client.get(
                "/api/v1/executions/cf136325-3333-42b0-a49c-b650e5014785",
                format="json",
            )
            self.assertEqual(expected_status_code, response.status_code)

    def test_executions_api_list_check_permissions(self):
        permissions = [
            (self.token_ucl_administrator, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_security_analyst, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_use_case_requester, api_status.HTTP_404_NOT_FOUND),
            (self.token_ucl_backend, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_administrator, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_ecs_maintainer, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_onboarder, api_status.HTTP_404_NOT_FOUND),
            (self.token_fda_reader, api_status.HTTP_404_NOT_FOUND),
        ]
        for token, expected_status_code in permissions:
            self.client.force_authenticate(self.test_user, token)
            response = self.client.get(
                "/api/v1/executions/cf136325-3333-42b0-a49c-b650e5014785",
                format="json",
            )
            self.assertEqual(expected_status_code, response.status_code)
