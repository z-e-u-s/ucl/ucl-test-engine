"""module for HTTP response 207 body typing"""

from dataclasses import dataclass, field

from ucl_test_engine.typing.test_execution_dataclass import RuleTestExecutionResult


@dataclass
class HttpResponse207DataElement:
    """
    Element for one request of many containing resource, message and status
    """

    message: str
    resource: RuleTestExecutionResult
    status: int


@dataclass
class HttpResponse207Metadata:
    """Metadata overview over all request results"""

    failure: int
    success: int
    total: int


@dataclass
class HttpResponse207BodyData:
    """Body data containing the response elements and overview"""

    data: list[HttpResponse207DataElement]
    metadata: HttpResponse207Metadata


@dataclass
class UclResponseWrapperBody:
    """Body containing the response wrapper with data and message field"""

    data: HttpResponse207BodyData
    message: str = field(metadata={"serializer_kwargs": {"allow_blank": True}})
