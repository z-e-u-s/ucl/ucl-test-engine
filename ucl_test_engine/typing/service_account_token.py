"""module for service account token dataclass"""

import datetime
from dataclasses import dataclass


@dataclass
class ServiceAccountToken:
    """Result from fetching service account token"""

    access_token: str
    expires_in: int
    refresh_expires_in: int
    token_type: str
    # not-before-policy: int
    scope: str
    expires_at: datetime.datetime | None = None
