"""module for logClass as dict typing"""

import uuid
from dataclasses import dataclass, field


@dataclass
class RuleTestExecutionResult:
    """Result object from the testEngine for a ruleTestExecution"""

    __test__ = False

    execution_id: uuid.UUID
    result: int = field(metadata={"serializer_kwargs": {"min_value": -1}})
    result_timestamp: str
