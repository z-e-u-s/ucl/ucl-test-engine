""" urls for api """

from rest_framework import routers
from ucl_test_engine.views.test_engine import TestEngineViewSet

router = routers.DefaultRouter(trailing_slash=False)

router.register(r"executions", viewset=TestEngineViewSet, basename="test-engine-execution")

urlpatterns = router.urls
