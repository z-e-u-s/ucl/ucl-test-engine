"""BaseViewSet"""

from django.utils import timezone
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from ucl_test_engine.renderers import JSONResponseRenderer


class BaseViewSet(viewsets.GenericViewSet):
    """BaseViewSet

    Provides common functionality to subclasses. Defines the authentication and permission classes
    as well as the renderer_class for responses.
    """

    permission_classes = [IsAuthenticated]
    renderer_classes = [JSONResponseRenderer]

    def perform_create(self, serializer):
        """On object creation,
        set updated to None"""
        if self.request.user:
            serializer.save(updated=None)

    def perform_update(self, serializer):
        """On object modification,
        set the timestamp of the object modification(updated)"""
        if self.request.user:
            serializer.save(
                updated=timezone.now().isoformat(sep="T").replace("+00:00", "Z"),
            )
