"""Health- and Readinesscheck View."""

# pylint: disable=W0246:useless-parent-delegation
from drf_spectacular.utils import extend_schema
from rest_framework.response import Response
from rest_framework import status, permissions
from rest_framework.views import APIView


class HealthCheckAPIView(APIView):
    # pylint: disable=C0301:line-too-long
    """View the current system status.

    This view is only here for API documentation issues.
    Work is done here: "ucl_test_engine.middleware.health_check.HealthCheckMiddleware" because otherwise we cannot check the database connection because keycloak middleware would have been failed before we reach this code.
    It returns a status code 200 and a json with the {"status": "up"} if the system is
    running correctly. Checks if the database connection can be established. If not
    a 503 status code will be returned.
    """

    throttle_classes = []
    permission_classes = [permissions.AllowAny]

    @extend_schema(
        responses={
            200: {
                "type": "object",
                "properties": {
                    "status": {
                        "type": "string",
                        "description": "Health status of the system",
                    },
                },
            },
        },
        operation_id="status",
        tags=["Health Status"],
    )
    def get(self, request, *args, **kwargs):
        """Get the current system status.

        Args:
            request (Request): the request object
            *args: arguments
            **kwargs: keyword arguments

        Returns:
            Response (200): a json with the {"status": "up"} if the system is
        running correctly
        """

        return Response(status=status.HTTP_200_OK)


class ReadinessCheckAPIView(APIView):
    # pylint: disable=C0301:line-too-long
    """Check if the system is ready to receive requests.

    This view is only here for API documentation issues.
    Work is done here: "ucl_test_engine.middleware.health_check.HealthCheckMiddleware" because otherwise we cannot check the database connection because keycloak middleware would have been failed before we reach this code.
    If the application is ready to receive requests a 200 status code will be returned.
    If the local mode is enabled (DISABLE_AUTHENTICATION = True) the readiness check
    returns a 503 status code. If the Database connection can not be established a 503
    status code will be returned.
    """

    throttle_classes = []
    permission_classes = [permissions.AllowAny]

    @extend_schema(
        responses={
            200: {
                "type": "object",
                "properties": {
                    "ready": {
                        "type": "boolean",
                        "description": "Readiness of the system",
                    },
                },
            },
            503: {
                "type": "object",
                "properties": {
                    "ready": {
                        "type": "boolean",
                        "description": "Readiness of the system",
                    },
                    "details": {
                        "type": "string",
                        "description": "Cause of the error.",
                    },
                },
            },
        },
        operation_id="ready",
        tags=["Readiness Status"],
    )
    def get(self, request, *args, **kwargs):
        """Get the current readiness state.

        Args:
            request (Request): the request object
            *args: arguments
            **kwargs: keyword arguments

        Returns:
            Response (200): if the database connection can be established
            Response (503): if the database connection can not be established or authentication is disabled
        """

        return Response(status=status.HTTP_200_OK)
