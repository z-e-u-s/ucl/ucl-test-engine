"""Test Engine ViewSet"""

# pylint: disable=R0801:duplicate-code

from rest_framework import status
from rest_framework.response import Response

from ucl_test_engine.auth.permissions.ucl_backend import UclBackendPermission
from ucl_test_engine.models.test_engine_test_execution import TestEngineTestExecution
from ucl_test_engine.serializers.rule_test_execution import (
    RuleTestExecutionCreateValidationListSerializer,
    RuleTestExecutionCreateValidationSerializer,
)
from ucl_test_engine.services.test_engine import TestEngineService
from ucl_test_engine.views.base import BaseViewSet


class TestEngineViewSet(BaseViewSet):
    __test__ = False
    """test engine API"""

    permission_classes = [UclBackendPermission]

    serializer_class = RuleTestExecutionCreateValidationSerializer

    def create(self, request, *args, **kwargs):
        """persist and then start test_executions"""
        serializer = RuleTestExecutionCreateValidationListSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        test_executions: list[TestEngineTestExecution] = serializer.save()

        TestEngineService.test_rule_test_executions(test_executions=test_executions)

        return Response(status=status.HTTP_202_ACCEPTED)
