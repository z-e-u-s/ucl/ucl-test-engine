"""ZEUS Roles Enum"""

from enum import Enum


class ZeusRoles(Enum):
    """Enum for all security (authorization) roles used in ZEUS Project"""

    UCL_BACKEND = "ucl-backend"
    UCL_ADMINISTRATOR = "ucl-admin"
    UCL_SECURITY_ANALYST = "ucl-security-analyst"
    UCL_TEST_ENGINE = "ucl-test-engine"
    UCL_USE_CASE_REQUESTER = "ucl-use-case-requester"
    FDA_ADMINISTRATOR = "fda-admin"
    FDA_ECS_MAINTAINER = "fda-ecs-maintainer"
    FDA_ONBOARDER = "fda-onboarder"
    FDA_READER = "fda-reader"
