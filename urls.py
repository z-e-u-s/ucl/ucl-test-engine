"""UCL Test Engine URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from drf_spectacular.views import SpectacularSwaggerView, SpectacularAPIView, SpectacularRedocView

urlpatterns = [
    path(f"{settings.API_ROOT}/", include("ucl_test_engine.urls")),
]

if settings.ENABLE_API_DOCU:
    urlpatterns += [
        path(f"{settings.API_ROOT}/schema", SpectacularAPIView.as_view(), name="schema"),
        path(
            f"{settings.API_ROOT}/schema/redoc",
            SpectacularRedocView.as_view(url_name="schema"),
            name="redoc",
        ),
        path(
            f"{settings.API_ROOT}/schema/swagger-ui",
            SpectacularSwaggerView.as_view(url_name="schema"),
            name="swagger-ui",
        ),
    ]

if settings.ENABLE_DJANGO_AUTH:
    urlpatterns += [
        path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    ]

if settings.ENABLE_ADMIN_SITE:
    urlpatterns += [
        path("admin/", admin.site.urls),
    ]
